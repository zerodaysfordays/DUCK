__package__ = "com.example.testresources3"

from java.lang import String
from android.app import Activity
import android.os
from android.widget import TextView

import app_resources

class TestResourcesActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        
        res = self.getResources()
        
        t = res.getBoolean(app_resources.R.bool.true_value)
        f = res.getBoolean(app_resources.R.bool.false_value)
        
        expected = str(True) + " " + str(False)
        actual = str(t) + " " + str(f)
        
        if t and not f:
            view.setText("Test passed!")
        else:
            view.setText("Test failed:\nExpected:\n" + expected + "\nObtained:\n" + actual)
            view.setBackgroundColor(0xff400000)
        
        self.setContentView(view)
