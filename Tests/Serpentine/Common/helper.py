from java.lang import String
from android.app import Activity
from android.os import Bundle
from android.widget import ScrollView, TextView

class TestActivity(Activity):

    def __init__(self):
        Activity.__init__(self)
    
    @args(void, [String, String, bool])
    def showResult(self, expected, actual, verbose):
    
        scrollView = ScrollView(self)
        view = TextView(self)
        scrollView.addView(view)
        
        if actual == expected:
            text = "Test passed!"
            if verbose:
                text += "\n\nActual text:\n" + actual
            
            view.setText(text)
        else:
            view.setText("Test failed:\nExpected:\n" + expected + "\nObtained:\n" + actual)
            view.setBackgroundColor(0xff400000)
        
        self.setContentView(scrollView)
    
    @args(void, [String, String])
    def showResult(self, expected, actual):
    
        self.showResult(expected, actual, False)
