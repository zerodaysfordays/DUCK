__package__ = "com.example.arrays21"

from java.util import String
from serpentine.arrays import Arrays
from Common.helper import TestActivity

class ArraysActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        b = array(int, 3, 4)
        
        for i in range(3):
            for j in range(4):
                b[i][j] = i + j
        
        expected = "0 1 2 3 \n1 2 3 4 \n2 3 4 5 \n"
        s = ""
        
        for i in range(3):
            for j in range(4):
                s += str(b[i][j]) + " "
            s += "\n"
        
        self.showResult(expected, s, True)
