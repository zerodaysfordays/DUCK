__package__ = "com.example.comparisons12"

import android.os
from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    __interfaces__ = []
    
    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        t = [# Float and double
             [(float(1) == 1.0), (1.0 == float(1)), (float(1) != 1.0), (1.0 != float(1)),
              (float(1) < 1.0), (1.0 < float(1)), (float(1) > 1.0), (1.0 > float(1)),
              (float(1) <= 1.0), (1.0 <= float(1)), (float(1) >= 1.0), (1.0 >= float(1)),
              (float(1) == 2.0), (2.0 == float(1)), (float(1) != 2.0), (2.0 != float(1)),
              (float(1) < 2.0), (2.0 < float(1)), (float(1) > 2.0), (2.0 > float(1)),
              (float(1) <= 2.0), (2.0 <= float(1)), (float(1) >= 2.0), (2.0 >= float(1)),
              (1.0 == float(2)), (float(2) == 1.0), (1.0 != float(2)), (float(2) != 1.0),
              (1.0 < float(2)), (float(2) < 1.0), (1.0 > float(2)), (float(2) > 1.0),
              (1.0 <= float(2)), (float(2) <= 1.0), (1.0 >= float(2)), (float(2) >= 1.0)],
             # Long and double
             [(long(1) == 1.0), (1.0 == long(1)), (long(1) != 1.0), (1.0 != long(1)),
              (long(1) < 1.0), (1.0 < long(1)), (long(1) > 1.0), (1.0 > long(1)),
              (long(1) <= 1.0), (1.0 <= long(1)), (long(1) >= 1.0), (1.0 >= long(1)),
              (long(1) == 2.0), (2.0 == long(1)), (long(1) != 2.0), (2.0 != long(1)),
              (long(1) < 2.0), (2.0 < long(1)), (long(1) > 2.0), (2.0 > long(1)),
              (long(1) <= 2.0), (2.0 <= long(1)), (long(1) >= 2.0), (2.0 >= long(1)),
              (1.0 == long(2)), (long(2) == 1.0), (1.0 != long(2)), (long(2) != 1.0),
              (1.0 < long(2)), (long(2) < 1.0), (1.0 > long(2)), (long(2) > 1.0),
              (1.0 <= long(2)), (long(2) <= 1.0), (1.0 >= long(2)), (long(2) >= 1.0)],
             # Float and long
             [(float(1) == long(1)), (long(1) == float(1)), (float(1) != long(1)), (long(1) != float(1)),
              (float(1) < long(1)), (long(1) < float(1)), (float(1) > long(1)), (long(1) > float(1)),
              (float(1) <= long(1)), (long(1) <= float(1)), (float(1) >= long(1)), (long(1) >= float(1)),
              (float(1) == long(2)), (long(2) == float(1)), (float(1) != long(2)), (long(2) != float(1)),
              (float(1) < long(2)), (long(2) < float(1)), (float(1) > long(2)), (long(2) > float(1)),
              (float(1) <= long(2)), (long(2) <= float(1)), (float(1) >= long(2)), (long(2) >= float(1)),
              (long(1) == float(2)), (float(2) == long(1)), (long(1) != float(2)), (float(2) != long(1)),
              (long(1) < float(2)), (float(2) < long(1)), (long(1) > float(2)), (float(2) > long(1)),
              (long(1) <= float(2)), (float(2) <= long(1)), (long(1) >= float(2)), (float(2) >= long(1))]]
        
        results = (
            True, True, False, False,
            False, False, False, False,
            True, True, True, True,
            False, False, True, True,
            True, False, False, True,
            True, False, False, True,
            False, False, True, True,
            True, False, False, True,
            True, False, False, True,
            )
        
        s = ""
        for tests in t:
            i = 0
            for result in tests:
                if result != results[i]:
                    s += "Not OK\n"
                    break
                i += 1
            else:
                s += "OK\n"
        
        expected = "OK\nOK\nOK\n"
        
        self.showResult(expected, s)
