__package__ = "com.example.arrays19"

from java.lang import Integer, String
from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.util import Log
from android.widget import TextView

from Common.helper import TestActivity

class ArraysActivity(TestActivity):

    def __init__(self):
    
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        i = 48
        l = []
        
        while i < 58:
            l.add(byte(i))
            i += 1
        
        a = l.toArray()    # an array of bytes (actually Bytes)
        
        s = String(a, "ASCII")
        self.showResult("0123456789", s)
