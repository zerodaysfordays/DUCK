__package__ = "com.example.operators1"

from java.lang import Integer, String
from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class OperatorsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        s = "Hello "
        t = "world!"
        text = s + t
        
        view = TextView(self)
        view.setText(text)
        
        self.setContentView(view)
