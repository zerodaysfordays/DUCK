__package__ = "com.example.lists10"

from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class ListsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        l = [1,2,3]
        m = [4,5,6]
        
        view = TextView(self)
        
        if l != m:
            view.setText("Test passed!")
        else:
            view.setText("Test failed")
        
        self.setContentView(view)
