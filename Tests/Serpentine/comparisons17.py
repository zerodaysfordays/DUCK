__package__ = "com.example.comparisons17"

import android.os

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        expected = ""
        s = ""
        
        f = False
        t = True
        
        s += str(f) + " "
        s += str(t) + "\n"
        
        expected += "false true\n"
        
        s += str(f and f) + " "
        s += str(f and t) + " "
        s += str(t and f) + " "
        s += str(t and t) + "\n"
        s += str(f and f and f) + " "
        s += str(f and f and t) + " "
        s += str(f and t and f) + " "
        s += str(f and t and t) + " "
        s += str(t and f and f) + " "
        s += str(t and f and t) + " "
        s += str(t and t and f) + " "
        s += str(t and t and t) + "\n"
        
        expected += ("false false false true\n"
                     "false false false false false false false true\n")
        
        s += str(f or f) + " "
        s += str(f or t) + " "
        s += str(t or f) + " "
        s += str(t or t) + "\n"
        s += str(f or f or f) + " "
        s += str(f or f or t) + " "
        s += str(f or t or f) + " "
        s += str(f or t or t) + " "
        s += str(t or f or f) + " "
        s += str(t or f or t) + " "
        s += str(t or t or f) + " "
        s += str(t or t or t) + "\n"
        
        expected += ("false true true true\n"
                     "false true true true true true true true\n")
        
        actual = s
        self.showResult(expected, actual)
