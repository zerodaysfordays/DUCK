__package__ = "com.example.methods1"

from java.lang import String
from android.os import Bundle

from Common.helper import TestActivity

# Application classes

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = self.fn(123)
        
        self.showResult("123", s)

    @args(String, [int])
    def fn(self, i):
    
        return str(i)
