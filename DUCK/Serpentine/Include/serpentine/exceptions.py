__package__ = "serpentine.exceptions"

from java.lang import Exception, String

class IndexError(Exception):

    def __init__(self):
        Exception.__init__(self)
    
    @args(void, [String])
    def __init__(self, message):
        Exception.__init__(self, message)

class IOError(Exception):

    def __init__(self):
        Exception.__init__(self)
    
    @args(void, [String])
    def __init__(self, message):
        Exception.__init__(self, message)

class KeyError(Exception):

    def __init__(self):
        Exception.__init__(self)
    
    @args(void, [String])
    def __init__(self, message):
        Exception.__init__(self, message)

class TypeError(Exception):

    def __init__(self):
        Exception.__init__(self)
    
    @args(void, [String])
    def __init__(self, message):
        Exception.__init__(self, message)

class ValueError(Exception):

    def __init__(self):
        Exception.__init__(self)
    
    @args(void, [String])
    def __init__(self, message):
        Exception.__init__(self, message)

# Use the following exception to report errors at compilation time.

class SyntaxError(Exception):

    def __init__(self):
        Exception.__init__(self)
    
    @args(void, [String])
    def __init__(self, message):
        Exception.__init__(self, message)
