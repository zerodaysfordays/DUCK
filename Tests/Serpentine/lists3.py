__package__ = "com.example.lists3"

from java.lang import Integer, String
from java.util import Collection, HashMap, HashSet
from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class ListsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        a = ["Hello"]
        a[0] = "World"
        
        view = TextView(self)
        if a[0] == "World":
            view.setText("Test passed!")
        else:
            view.setText("Test failed: " + str(a[1]))
        
        self.setContentView(view)
