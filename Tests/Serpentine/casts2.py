# Tests generation of bytecode for constants of specific types.
# Use the DUCK_LISTING environment variable when building to create a listing
# for inspection.

__package__ = "com.example.casts2"

from java.nio import ByteBuffer

from Common.helper import TestActivity

# Application classes

class CastsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        buf = ByteBuffer.allocate(8)
        buf.putLong(7)
        
        a = buf.array()
        s = ""
        
        for i in a:
            s += str(i) + " "
        
        expected = "0 0 0 0 0 0 0 7 "
        
        self.showResult(expected, s)
