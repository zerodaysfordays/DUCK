__package__ = "serpentine.strings"

from java.lang import CharSequence
from java.util import Object, String

class Strings(Object):

    __replace__ = [Q]
    
    @static
    @args(String, [Object])
    def str(value):
    
        if value == None:
            return "None"
        
        return value.toString()
    
    @static
    @args(String, [CharSequence])
    def str(value):
    
        return value.toString()
    
    @static
    @args(String, [Q])
    def str(value):
    
        return String.valueOf(value)
