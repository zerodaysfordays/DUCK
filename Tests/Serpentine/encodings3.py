# -*- coding: iso-8859-15 -*-

__package__ = "com.example.encodings3"

import android.os

from Common.helper import TestActivity

class EncodingsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = u"Hello world ���!"
        self.showResult(u"Hello world ���!", a, True)
