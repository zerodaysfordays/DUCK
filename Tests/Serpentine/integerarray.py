__package__ = "com.example.integerarray"

from java.lang import Integer
from java.util import ArrayList
from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class IntegerList(ArrayList):

    __item_types__ = [Integer]
    
    def __init__(self):
        ArrayList.__init__(self)


class IntegerListActivity(Activity):

    __interfaces__ = []
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        numbers = IntegerList()
        numbers.add(Integer(68))
        numbers.add(Integer(85))
        numbers.add(Integer(67))
        numbers.add(Integer(75))
        
        number_array = numbers.toArray()
        
        view = TextView(self)
        s = ""
        i = 0
        while i < len(number_array):
            v = CAST(number_array[i], Integer)
            s = s.concat(" ").concat(s.valueOf(v.intValue()))
            i = i + 1
        
        view.setText(s)
        self.setContentView(view)
