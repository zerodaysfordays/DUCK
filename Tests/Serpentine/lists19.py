__package__ = "com.example.lists19"

from Common.helper import TestActivity
from serpentine.collections import Collections

class ListsActivity(TestActivity):

    def __init__(self):
    
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l = Collections.range(10)
        
        s = ""
        for i in Collections.slice(l, 0, 5):
            s += str(i) + " "
        
        for i in Collections.slice(l, 5, 10):
            s += str(i) + " "
        
        expected = "0 1 2 3 4 5 6 7 8 9 "
        self.showResult(expected, s)
