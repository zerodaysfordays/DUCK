__package__ = "com.example.methods8"

from java.lang import Object, String
from android.os import Bundle

from Common.helper import TestActivity

# Application classes

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        o = Sub()
        s = o.fn(123)
        
        self.showResult("123123", s)


class Base(Object):

    def __init__(self):
        Object.__init__(self)
    
    @static
    @args(String, [int])
    def fn(i):
        return str(i)


class Middle(Base):

    def __init__(self):
        Base.__init__(self)
    
    @args(String, [int])
    def fn(self, j):
        return str(j)


class Sub(Middle):

    def __init__(self):
        Middle.__init__(self)
    
    @args(String, [int])
    def fn(self, k):
    
        s = Base.fn(k)
        s += Middle.fn(k)   # This should fail because Middle.fn is not static.
        s += Middle.fn(self, k)
        return s
