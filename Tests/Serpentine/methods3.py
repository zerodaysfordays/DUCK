__package__ = "com.example.methods3"

from java.lang import Object, String
from android.os import Bundle

from Common.helper import TestActivity

# Application classes

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = Other.fn(123)
        t = str(Other.simple())
        
        self.showResult("123456", s + t)


class Other(Object):

    @static
    @args(String, [int])
    def fn(i):
        return str(i)
    
    @static
    @args(int, [])
    def simple():
        return 456
