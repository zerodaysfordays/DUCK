__package__ = "com.example.json2"

from java.util import Collections
from org.json import JSONArray, JSONObject, JSONTokener

from Common.helper import TestActivity

class JSONActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        j = \
"""[
    {
     "Isacord Polyester": 20,
     "Janome": 2,
     "Madeira Rayon": 1000,
     "Robison-Anton Polyester": 5596,
     "Sulky Rayon": 1005
    },
    {
     "Isacord Polyester": 15,
     "Janome": 1,
     "Madeira Rayon": 1001,
     "Sulky Rayon": 1001
    }
]"""
        
        obj = JSONTokener(j).nextValue()
        json_array = CAST(obj, JSONArray)
        
        s = "[\n"
        i = 0
        while i < len(json_array):
        
            s += "    {\n"
            json_obj = json_array.getJSONObject(i)
            it = json_obj.keys()
            
            # Sort the keys so that the generated entries should appear in the
            # same order as above.
            keys = []
            
            while it.hasNext():
                key = it.next()
                keys.add(key)
            
            Collections.sort(keys)
            last = keys.getLast()
            
            for key in keys:
            
                s += '     "' + key + '": ' + str(json_obj.getInt(key))
                if key != last:
                    s += ",\n"
                else:
                    s += "\n"
            
            i += 1
            if i < len(json_array):
                s += "    },\n"
            else:
                s += "    }\n"
        
        s += "]"
        
        self.showResult(j, s, True)
