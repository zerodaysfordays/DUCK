__package__ = "com.example.json1"

from org.json import JSONObject, JSONTokener

from Common.helper import TestActivity

class JSONActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # Example JSON structure from the JSONTokener documentation:
        j = """
        {
            "query": "Pizza",
            "locations": [ 94043, 90210 ]
        }
        """
        
        obj = JSONTokener(j).nextValue()
        json_obj = CAST(obj, JSONObject)
        
        s = json_obj.getString("query")
        locations = json_obj.getJSONArray("locations")
        s += ": "
        for i in range(len(locations)):
            s += str(locations.getInt(i)) + " "
        
        self.showResult("Pizza: 94043 90210 ", s, True)
