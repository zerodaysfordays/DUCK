__package__ = "com.example.while10"

from Common.helper import TestActivity

class WhileActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        i = 0
        while False:
            x = i
        else:
            x = 123
        
        s = str(x)
        
        expected = "123"
        
        self.showResult(expected, s)
