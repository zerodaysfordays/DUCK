# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.content.pm"

from java.lang import Object, String
from android.os import Parcel, Parcelable, PatternMatcher
from android.util import Printer

class ActivityInfo(ComponentInfo):

    CONFIG_DENSITY              = 0x00001000
    CONFIG_FONT_SCALE           = 0x40000000
    CONFIG_KEYBOARD             = 0x00000010
    CONFIG_KEYBOARD_HIDDEN      = 0x00000020
    CONFIG_LAYOUT_DIRECTION     = 0x00002000
    CONFIG_LOCALE               = 0x00000004
    CONFIG_MCC                  = 0x00000001
    CONFIG_MNC                  = 0x00000002
    CONFIG_NAVIGATION           = 0x00000040
    CONFIG_ORIENTATION          = 0x00000080
    CONFIG_SCREEN_LAYOUT        = 0x00000100
    CONFIG_SCREEN_SIZE          = 0x00000400
    CONFIG_SMALLEST_SCREEN_SIZE = 0x00000800
    CONFIG_TOUCHSCREEN          = 0x00000008
    CONFIG_UI_MODE              = 0x00000200
    ### ...


class ComponentInfo(PackageItemInfo):

    def __init__(self):
        pass
    
    @args(void, [ComponentInfo])
    def __init__(self, original):
        pass
    
    ### ...


class PackageItemInfo(Object):

    def __init__(self):
        pass
    
    @args(void, [PackageItemInfo])
    def __init__(self, original):
        pass


class PackageManager(Object):

    def __init__(self):
        pass


class PathPermission(PatternMatcher):

    __static_fields__ = {
        "CREATOR": Parcelable.Creator(PathPermission)
        }
    
    @args(void, [String, int, String, String])
    def __init__(self, pattern, type, readPermission, writePermission):
        pass
    
    @args(void, [Parcel])
    def __init__(self, src):
        pass
    
    @args(String, [])
    def getReadPermission(self):
        pass
    
    @args(String, [])
    def getWritePermission(self):
        pass


class ProviderInfo(ComponentInfo):

    __interfaces__ = [Parcelable]
    __fields__ = {
        "CREATOR": Parcelable.Creator(ProviderInfo),
        "authority": String,
        "flags": int,
        "grantUriPermissions": bool,
        "initOrder": int,
        "isSyncable": bool,
        "multiprocess": bool,
        "pathPermissions": [PathPermission],
        "readPermission": String,
        "urlPermissionPatterns": [PatternMatcher],
        "writePermission": String
        }
    
    FLAG_SINGLE_USER = 0x40000000
    
    def __init__(self):
        pass
    
    @args(void, [ProviderInfo])
    def __init__(self):
        pass
    
    @args(void, [Printer, String])
    def dump(self, pw, prefix):
        pass
