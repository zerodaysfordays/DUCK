__package__ = "com.example.maps9"

from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class MapsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        a = {}
        b = [1, 2, 3]
        c = [4, 5, 6]
        a["ABC"] = b
        a["DEF"] = c
        
        keys = a.keySet()
        i = keys.iterator()
        s = ""
        
        while i.hasNext():
            key = i.next()
            j = a[key].iterator()
            s += key + "\n"
            
            while j.hasNext():
                value = j.next()
                s += str(value) + " "
            
            s += "\n"
        
        view = TextView(self)
        view.setText(s)
        self.setContentView(view)
