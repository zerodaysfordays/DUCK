#!/usr/bin/env python

from distutils.core import setup
import os

import DUCK

def find_files(prefix, directory, suffix):

    l = []
    for name in os.listdir(os.path.join(prefix, directory)):
    
        file_name = os.path.join(directory, name)
        full_name = os.path.join(prefix, directory, name)
        
        if os.path.isfile(full_name) and full_name.endswith(suffix):
            l.append(file_name)
        elif os.path.isdir(full_name):
            l += find_files(prefix, file_name, ".py")
    
    return l


include_files = []
for subdir in "Include", "Meta":
    include_files += find_files("DUCK/Serpentine", subdir, ".py")

setup(
    name         = "DUCK",
    description  = "Tools and modules for reading and writing Dalvik-related files.",
    author       = "David Boddie",
    author_email = "david@boddie.org.uk",
    url          = "http://www.boddie.org.uk/david/Projects/Python/DUCK",
    version      = DUCK.__version__,
    packages     = ["Dalvik",
                    "DUCK",
                    "DUCK/Serpentine",
                    "DUCK/Tools"],
    package_data = {"DUCK/Serpentine": include_files}
    )
