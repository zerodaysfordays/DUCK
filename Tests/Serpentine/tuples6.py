__package__ = "com.example.tuples6"

from Common.helper import TestActivity

class TuplesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a, self.b = 1, 2
        s = str(a) + " " + str(self.b) + " "
        
        a, self.b = self.b, a
        s += str(a) + " " + str(self.b)
        
        expected = "1 2 2 1"
        self.showResult(expected, s, True)
