Add support for bitwise operations on primitive types
=====================================================

There are only bytecodes for bitwise operations on int and long types.
Biwise operations on other primitive types require values to be cast to those
types before they can be performed.
