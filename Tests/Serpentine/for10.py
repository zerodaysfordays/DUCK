__package__ = "com.example.for10"

from java.lang import Integer
import android.os
from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        
        for i in array([1, 2, 3, 4, 5]):
            s += str(i) + " "
        
        for i in array([1, 4, 9, 16, 25]):
            s += str(i) + " "
        
        expected = "1 2 3 4 5 1 4 9 16 25 "
        self.showResult(expected, s)
