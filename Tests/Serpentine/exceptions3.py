__package__ = "com.example.exceptions3"

from java.lang import ArithmeticException, Exception
from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class ExceptionsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        try:
            a = self.fn(0)
        except Exception, e:
            view.setText("Caught " + str(e))
        
        self.setContentView(view)
    
    @args(int, [int])
    def fn(self, value):
    
        return 1/value
