__package__ = "com.example.zip7"

from Common.helper import TestActivity

class ZipActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l1 = ["A", "C", "E"]
        l2 = ["B", "D", "F"]
        s = ""
        
        for i, j in zip(l1, l2):
            s += i + j
        
        expected = "ABCDEF"
        self.showResult(expected, s, True)
