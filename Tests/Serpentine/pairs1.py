__package__ = "com.example.pairs1"

from serpentine.collections import Pair
from Common.helper import TestActivity

class PairsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        p = Pair(1, "Hello")
        s = str(p.first()) + " " + p.second()
        
        expected = "1 Hello"
        self.showResult(expected, s, True)
