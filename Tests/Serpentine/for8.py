__package__ = "com.example.for8"

from java.lang import Integer
import android.os
from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = array([1, 4, 9, 16, 25])
        s = ""
        
        for i in a:
            s += str(i) + " "
        
        expected = "1 4 9 16 25 "
        self.showResult(expected, s)
