__package__ = "com.example.lists5"

from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class ListsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        a = [123, 456]
        
        view = TextView(self)
        if a[1] == 456:
            view.setText("Test passed!")
        else:
            view.setText("Test failed: " + str(a[1]))
        
        self.setContentView(view)
