__package__ = "com.example.integerlist"

from java.lang import Integer
from java.util import ArrayList, List
from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class IntegerList(ArrayList):

    __item_types__ = [Integer]
    
    def __init__(self):
        ArrayList.__init__(self)


class IntegerListActivity(Activity):

    __interfaces__ = []
    __fields__ = {
        "numbers": List(Integer)
        }
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        self.numbers = IntegerList()
        self.numbers.add(Integer(68))
        self.numbers.add(Integer(85))
        self.numbers.add(Integer(67))
        self.numbers.add(Integer(75))
        
        view = TextView(self)
        s = ""
        i = 0
        while i < len(self.numbers):
            s = s.concat(" ").concat(s.valueOf(self.numbers[i].intValue()))
            i = i + 1
        
        view.setText(s)
        self.setContentView(view)
