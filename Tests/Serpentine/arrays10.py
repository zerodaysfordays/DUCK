__package__ = "com.example.arrays10"

from Common.helper import TestActivity

class ArraysActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # Create an array of integers.
        a = array([1, 3, 5, 7, 9])
        
        s = ""
        i = 0
        while i <= 10:
            s += str(i in a) + " "
            i += 1
        
        expected = "false true false true false true false true false true false "
        
        self.showResult(expected, s)
