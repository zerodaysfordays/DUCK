__package__ = "com.example.strings6"

from java.lang import String

from Common.helper import TestActivity

class StringsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = "Hello world!"
        b = a[2] + a[4] + a[6]
        
        expected = "low"
        
        self.showResult(expected, b)
