__package__ = "com.example.lists18"

from Common.helper import TestActivity
from serpentine.collections import Collections

class ListsActivity(TestActivity):

    def __init__(self):
    
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l = Collections.range(10)
        m = Collections.range(0, 5)
        n = Collections.range(0, 10, 2)
        
        s = ""
        for i in l:
            s += str(i) + " "
        for i in m:
            s += str(i) + " "
        for i in n:
            s += str(i) + " "
        
        expected = "0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 0 2 4 6 8 "
        
        self.showResult(expected, s)
