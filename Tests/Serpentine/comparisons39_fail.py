__package__ = "com.example.comparisons39"

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        i = 2
        
        if i == 1:
            x = 123
        else:
            a = 1 + x
        
        s = str(x)
        
        self.showResult("123", s)
