__package__ = "com.example.arrays12"

from Common.helper import TestActivity

class ArraysActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # Create an array of integers.
        a = array([1, 3, 5, 7, 9])
        
        a = a[0:3]
        s = ""
        
        for i in a:
            s += str(i) + " "
        
        expected = "1 3 5 "
        
        self.showResult(expected, s)
