"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from struct import calcsize

# See https://source.android.com/devices/tech/dalvik/dex-format.html for
# information about Dex format files.

NO_INDEX = 0xffffffff

# Value formats
VALUE_BYTE = 0x00
VALUE_SHORT = 0x02
VALUE_CHAR = 0x03
VALUE_INT = 0x04
VALUE_LONG = 0x06
VALUE_FLOAT = 0x10
VALUE_DOUBLE = 0x11
VALUE_STRING = 0x17
VALUE_TYPE = 0x18
VALUE_FIELD = 0x19
VALUE_METHOD = 0x1a
VALUE_ENUM = 0x1b
VALUE_ARRAY = 0x1c
VALUE_ANNOTATION = 0x1d
VALUE_NULL = 0x1e
VALUE_BOOLEAN = 0x1f

# Map type codes
TYPE_HEADER_ITEM = 0x0000
TYPE_STRING_ID_ITEM = 0x0001
TYPE_TYPE_ID_ITEM = 0x0002
TYPE_PROTO_ID_ITEM = 0x0003
TYPE_FIELD_ID_ITEM = 0x0004
TYPE_METHOD_ID_ITEM = 0x0005
TYPE_CLASS_DEF_ITEM = 0x0006
TYPE_MAP_LIST = 0x1000
TYPE_TYPE_LIST = 0x1001
TYPE_ANNOTATION_SET_REF_LIST = 0x1002
TYPE_ANNOTATION_SET_ITEM = 0x1003
TYPE_CLASS_DATA_ITEM = 0x2000
TYPE_CODE_ITEM = 0x2001
TYPE_STRING_DATA_ITEM = 0x2002
TYPE_DEBUG_INFO_ITEM = 0x2003
TYPE_ANNOTATION_ITEM = 0x2004
TYPE_ENCODED_ARRAY_ITEM = 0x2005
TYPE_ANNOTATIONS_DIRECTORY_ITEM = 0x2006

# Visibility values
VISIBILITY_BUILD = 0
VISIBILITY_RUNTIME = 1
VISIBILITY_SYSTEM = 2

# Access flags
ACC_PUBLIC                  = 0x00001
ACC_PRIVATE                 = 0x00002
ACC_PROTECTED               = 0x00004
ACC_STATIC                  = 0x00008
ACC_FINAL                   = 0x00010
ACC_SYNCHRONIZED            = 0x00020
ACC_VOLATILE                = 0x00040
ACC_BRIDGE                  = 0x00040
ACC_TRANSIENT               = 0x00080
ACC_VARARGS                 = 0x00080
ACC_NATIVE                  = 0x00100
ACC_INTERFACE               = 0x00200
ACC_ABSTRACT                = 0x00400
ACC_STRICT                  = 0x00800
ACC_SYNTHETIC               = 0x01000
ACC_ANNOTATION              = 0x02000
ACC_ENUM                    = 0x04000
ACC_CONSTRUCTOR             = 0x10000
ACC_DECLARED_SYNCHRONIZED   = 0x20000

class DecodeError(Exception):
    pass

class EncodeError(Exception):
    pass

# Value types

class TypeDescriptor:

    def __init__(self, value = None):
        self.value = value
    
    def __repr__(self):
        if self.value is None:
            return "%s()" % self.__class__.__name__
        else:
            return "%s(%s)" % (self.__class__.__name__, repr(self.value))
    
    def __str__(self):
        return self.name
    
    def __hash__(self):
        return hash(self.desc)
    
    def __cmp__(self, other):
        if not isinstance(other, TypeDescriptor):
            return -1
        
        return cmp(self.desc, other.desc)
    
    def size(self):
        """Returns the number of registers or words required to represent
        values of this type."""
        bytes = calcsize(self.typecode)
        size = bytes/4
        if bytes % 4 != 0:
            size += 1
        return size
    
    def unique_name(self):
        return str(self)

class Void(TypeDescriptor):
    name = "void"
    desc = u"V"
    type = None
    typecode = None

class Boolean(TypeDescriptor):
    name = "boolean"
    desc = u"Z"
    type = VALUE_BOOLEAN
    typecode = "b"

class Byte(TypeDescriptor):
    name = "byte"
    desc = u"B"
    type = VALUE_BYTE
    typecode = "b"

class Short(TypeDescriptor):
    name = "short"
    desc = u"S"
    type = VALUE_SHORT
    typecode = "h"

class Char(TypeDescriptor):
    name = "char"
    desc = u"C"
    type = VALUE_CHAR
    typecode = "H"

class Int(TypeDescriptor):
    name = "int"
    desc = u"I"
    type = VALUE_INT
    typecode = "i"

class Long(TypeDescriptor):
    name = "long"
    desc = u"J"
    type = VALUE_LONG
    typecode = "q"

class Float(TypeDescriptor):
    name = "float"
    desc = u"F"
    type = VALUE_FLOAT
    typecode = "f"

class Double(TypeDescriptor):
    name = "double"
    desc = u"D"
    type = VALUE_DOUBLE
    typecode = "d"

class Type(TypeDescriptor):
    name = "type"
    desc = None
    type = VALUE_TYPE
    typecode = "I"

class ReferenceType(TypeDescriptor):
    name = "short name"
    desc = u"L"
    type = None
    typecode = "I"

# Create default values for these types.
dVoid = Void()
dBoolean = Boolean()
dByte = Byte()
dShort = Short()
dChar = Char()
dInt = Int()
dLong = Long()
dFloat = Float()
dDouble = Double()
dType = Type()
dReferenceType = ReferenceType()

class Name(TypeDescriptor):

    name = "name"
    type = VALUE_TYPE
    typecode = "I"
    
    def __init__(self, s):
    
        if s[-1] != ";":
            raise DecodeError("Invalid fully qualified name '%s'" % s)
        
        self.value = s[1:-1]
        self.desc = s
    
    def __repr__(self):
        return "Name(%s)" % repr(self.desc)
    
    def __str__(self):
        return self.desc

class Array(TypeDescriptor):

    name = "array"
    desc = u"["
    type = VALUE_ARRAY
    typecode = "I"
    
    def __init__(self, desc):
        TypeDescriptor.__init__(self, desc)
        self.desc = desc
    
    def item_type(self):
    
        s = self.desc.lstrip(u"[")
        
        try:
            # Obtain either a prepared instance representing a simple type or a
            # class to be used to create an instance representing a complex one.
            desc = TypeDescriptors[s[0]]
        except KeyError:
            raise DecodeError("Unknown type descriptor '%s'" % s)
        
        if desc == Name or desc == MemberName:
            # Create an instance of the Name class used to represent the type.
            desc = desc(s)
        
        return desc
    
    def __repr__(self):
        return "Array(%s)" % repr(self.desc)
    
    def __str__(self):
        return self.desc

class MemberName(TypeDescriptor):

    name = "member name"
    type = VALUE_TYPE
    typecode = "I"
    
    def __init__(self, s):
    
        if s[0] == "<" and s[-1] != ">":
            raise DecodeError("Invalid member name '%s'" % s)
        
        self.desc = s
    
    def __repr__(self):
        return "MemberName(%s)" % repr(self.desc)
    
    def __str__(self):
        return self.desc

TypeDescriptors = {"V": dVoid,   "Z": dBoolean, "B": dByte,
                   "S": dShort,  "C": dChar,    "I": dInt,
                   "J": dLong,   "F": dFloat,   "D": dDouble,
                   "L": Name,   "[": Array,   "<": MemberName}

ShortDescriptors = {"V": dVoid,  "Z": dBoolean, "B": dByte,
                    "S": dShort, "C": dChar,    "I": dInt,
                    "J": dLong,  "F": dFloat,   "D": dDouble,
                    "L": dReferenceType}

# Resolved structures

class Prototype:

    def __init__(self, shorty, return_type, parameters):
    
        self.shorty = shorty
        self.return_type = return_type
        self.parameters = parameters
        
        self.cmp_tuple = tuple([self.return_type] + self.parameters)
    
    def __repr__(self):
    
        return "Prototype(%s, %s, %s)" % (repr(self.shorty),
            repr(self.return_type), repr(self.parameters))
    
    def __hash__(self):
    
        return hash(self.cmp_tuple)
    
    def __cmp__(self, other):
    
        if self.cmp_tuple == other.cmp_tuple:
            return 0
        else:
            return 1

class Field:

    type = VALUE_FIELD
    typecode = "I"
    
    def __init__(self, class_type, type_, name, arg_types = None):
    
        self.class_type = class_type
        self.type_ = type_
        self.name = name
        self.arg_types = arg_types
        
        self.cmp_tuple = (self.class_type, self.type_, self.name)
    
    def __repr__(self):
    
        return "Field(%s, %s, %s)" % (repr(self.class_type), repr(self.type_),
                                      repr(self.name))
    def __hash__(self):
    
        return hash(self.cmp_tuple)
    
    def __cmp__(self, other):
    
        if self.cmp_tuple == other.cmp_tuple:
            return 0
        else:
            return 1

class Method:

    """Represents a method, describing its name, its prototype and the class
    containing it for use in creation of method_id_item structures, as well as
    its access flags and code for use in encoded_method structures.
    
    The __hash__ and __cmp__ methods only take the name, prototype and
    containing class into account. This makes it possible to replace or update
    Method objects while continuing to use the original equivalent objects in
    dictionaries and sets. In other words, updating the code or access flags
    should not change the properties of the object as a key."""
    
    type = VALUE_METHOD
    typecode = "I"
    
    def __init__(self, class_type, proto, name, access_flags = None, code = None):
    
        self.class_type = class_type
        self.proto = proto
        self.name = name
        self.access_flags = access_flags
        self.code = code
        
        self.cmp_tuple = (class_type, proto, name.desc)
    
    def __repr__(self):
    
        return "Method(%s, %s, %s, %s, %s)" % (repr(self.class_type),
            repr(self.proto), repr(self.name), repr(self.access_flags),
            repr(self.code))
    
    def package(self):
        return ".".join(self.class_type.value.split("/")[:-1])
    
    def __hash__(self):
    
        # Don't include the code in the hash calculation - the other attributes
        # should be enough to uniquely identify the method. Also discard the
        # access flags for the time being.
        return hash(self.cmp_tuple)
    
    def __cmp__(self, other):
    
        # Don't include the code in the comparison - the other attributes
        # should be enough to uniquely identify the method. Also discard the
        # access flags for the time being.
        
        if self.cmp_tuple == other.cmp_tuple:
            return 0
        else:
            return 1

class Enum(Field):

    type = VALUE_ENUM
    typecode = "I"
    
    def __repr__(self):
        return "<Enum %s %s>" % (self.type_, self.name)

# Number decoding and encoding functions

def _read_uleb128(data, offset):

    value = 0
    bits = 0
    
    while True:
    
        byte = ord(data[offset])
        value = value | ((byte & 0x7f) << bits)
        bits += 7
        offset += 1
        
        if byte & 0x80 == 0:
            break
    
    return value, bits, offset

def read_sleb128(data, offset):

    value, bits, offset = _read_uleb128(data, offset)
    
    mask = 1 << (bits - 1)
    top = value & mask
    
    while top != 0 and bits % 8 != 0:
    
        # Extend the top bit to fill the remaining bits at the top of the
        # value.
        
        top = top << 1
        value = value | top
        bits += 1
    
    if top != 0:
        value = value - (1 << bits)
    
    return value, offset

def read_uleb128(data, offset):

    value, bits, offset = _read_uleb128(data, offset)
    return value, offset

def read_signed(data, offset, size, little_endian = True):

    """Returns the value of a given size (in bytes) from the data buffer,
    starting at the given offset, interpreting it as either a little endian or
    big endian signed value. The value is treated as negative if its top bit is
    set."""
    
    # Initially interpret the data as an unsigned integer to obtain a value in
    # two's complement form.
    value = read_unsigned(data, offset, size, little_endian)
    
    # Test the top bit.
    signed = 0x80 << ((size - 1) * 8)
    
    if value & signed != 0:
        # Convert the two's complement form of the signed integer to a proper
        # signed integer.
        value = value - (signed << 1)
    
    return value

def read_unsigned(data, offset, size, little_endian = True):

    """Returns the value of a given size (in bytes) from the data buffer,
    starting at the given offset, interpreting it as either a little endian or
    big endian unsigned value."""
    
    value = 0
    i = 0
    
    if little_endian:
        while i < size:
            value = value | (ord(data[offset + i]) << (i * 8))
            i += 1
    else:
        while i < size:
            value = (value << 8) | ord(data[offset + i])
            i += 1
    
    return value

def write_sleb128(number):

    if number < 0:
        # Obtain the value containing the bit-inverted form of the two's
        # complement representation the negative number should have so that we
        # can process it like a positive number.
        #number = (0x100000000 + number) ^ 0xffffffff
        number = -number - 1
        invert = 0x7f
    else:
        invert = 0
    
    s = ""
    while len(s) < 5:
    
        byte = number & 0x7f
        number = number >> 7
        
        if byte < 0x40 and number == 0:
            # The top bit in the payload can be extended to fill the sign bits.
            s += chr(byte ^ invert)
            break
        else:
            s += chr((byte ^ invert) | 0x80)
    
    return s

def write_uleb128(number):

    s = ""
    while len(s) < 5:
    
        byte = number & 0x7f
        number = number >> 7
        
        if number == 0:
            s += chr(byte)
            break
        else:
            s += chr(byte | 0x80)
    
    return s

# http://stackoverflow.com/questions/7921016/what-does-it-mean-to-say-java-modified-utf-8-encoding
# http://docs.oracle.com/javase/8/docs/api/java/io/DataInput.html

def read_mutf8(data, offset, length):

    s = u""
    i = 0
    p = offset
    
    while i < length:
    
        c = ord(data[p])
        
        if c & 0x80 == 0 and c != 0:
            s += unichr(c)
        
        elif c & 0xe0 == 0xc0:
            p += 1
            d = ord(data[p])
            if d & 0xc0 != 0x80:
                raise ValueError("Not a MUTF-8 string at offset %i." % offset)
            
            # 110ccccc 10dddddd
            s += unichr((d & 0x3f) | ((c & 0x1f) << 6))
        
        elif c & 0xf0 == 0xe0:
            p += 1
            d = ord(data[p])
            if d & 0xc0 != 0x80:
                raise ValueError("Not a MUTF-8 string at offset %i." % offset)
            p += 1
            e = ord(data[p])
            if e & 0xc0 != 0x80:
                raise ValueError("Not a MUTF-8 string at offset %i." % offset)
            
            # 110ccccc 10dddddd 10eeeeee
            s += unichr((e & 0x3f) | ((d & 0x3f) << 6) | ((c & 0x0f) << 12))
        else:
            raise ValueError("Not a MUTF-8 string at offset %i." % offset)
        
        p += 1
        i += 1
    
    return s

def write_mutf8(text):

    d = ""
    for char in text:
    
        c = ord(char)
        if 1 <= c <= 0x7f:
            d += chr(c)
        
        elif c == 0 or c <= 0x7ff:
            d += chr(0xc0 | ((c >> 6) & 0x1f))
            d += chr(0x80 | (c & 0x3f))
        
        else:
            d += chr(0xe0 | ((c >> 12) & 0x0f))
            d += chr(0x80 | ((c >> 6) & 0x3f))
            d += chr(0x80 | (c & 0x3f))
    
    return d

# The following function sorts a list containing strings and Unicode strings by
# converting

def utf8_sort(s1, s2):

    return cmp(write_mutf8(s1), write_mutf8(s2))
