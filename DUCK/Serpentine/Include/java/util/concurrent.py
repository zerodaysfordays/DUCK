# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.util.concurrent"

from java.io import Serializable
from java.lang import Enum, Object, Runnable, Thread
from java.util import AbstractQueue, Collection, Queue

class AbstractExecutorService(Object):

    __parameters__ = [T]
    
    def __init__(self):
        pass
    
    @args(List(Future(T)), [Collection(Callable(T))])
    def invokeAll(self, tasks):
        pass
    
    @args(List(Future(T)), [Collection(Callable(T)), long, TimeUnit])
    def invokeAll(self, tasks, timeout, unit):
        pass
    
    @args(T, [Collection(Callable(T)), long, TimeUnit])
    def invokeAny(self, tasks, timeout, unit):
        pass
    
    @args(T, [Collection(Callable(T))])
    def invokeAny(self, tasks):
        pass
    
    @args(Future(T), [Runnable, T])
    def submit(self, task, result):
        pass
    
    @args(Future(T), [Callable(T)])
    def submit(self, task):
        pass
    
    @args(Future, [Runnable])
    def submit(self, task):
        pass
    
    @protected
    @args(RunnableFuture(T), [Callable(T)])
    def newTaskFor(self, callable):
        pass
    
    @protected
    @args(RunnableFuture(T), [Runnable, T])
    def newTaskFor(self, runnable, value):
        pass


class BlockingQueue:

    __interfaces__ = [Queue] # Queue(E)
    __parameters__ = [E]
    
    @args(bool, [E])
    def add(self, e):
        pass
    
    @args(bool, [Object])
    def contains(self, o):
        pass
    
    @args(int, [Collection(E)])
    def drainTo(self, c):
        pass
    
    @args(int, [Collection(E), int])
    def drainTo(self, c, maxElements):
        pass
    
    @args(bool, [E, long, TimeUnit])
    def offer(self, e, timeout, unit):
        pass
    
    @args(bool, [E])
    def offer(self, e):
        pass
    
    @args(E, [long, TimeUnit])
    def poll(self, timeout, unit):
        pass
    
    @args(void, [E])
    def put(self, e):
        pass
    
    @args(int, [])
    def remainingCapacity(self):
        pass
    
    @args(bool, [Object])
    def remove(self, o):
        pass
    
    @args(E, [])
    def take(self):
        pass


class Executor:

    @args(void, [Runnable])
    def execute(self, command):
        pass


class Future:

    __parameters__ = [V]
    
    @args(boolean, [bool])
    def cancel(self, mayInterruptIfRunning):
        pass
    
    @args(V, [])
    def get(self):
        pass
    
    @args(V, [long, TimeUnit])
    def get(self, timeout, unit):
        pass
    
    @args(bool, [])
    def isCancelled(self):
        pass
    
    @args(bool, [])
    def isDone(self):
        pass


class LinkedBlockingQueue(AbstractQueue):

    __interfaces__ = [BlockingQueue, Serializable]
    
    def __init__(self):
        pass
    
    @args(void, [int])
    def __init__(self, capacity):
        pass
    
    @args(void, [Collection(E)])
    def __init__(self, c):
        pass
    
    @args(void, [])
    def clear(self):
        pass
    
    @args(bool, [Object])
    def contains(self, o):
        pass
    
    @args(int, [Collection(E), int])
    def drainTo(self, c, maxElements):
        pass
    
    @args(int, [Collection(E)])
    def drainTo(self, c):
        pass
    
    @args(Iterator(E), [])
    def iterator(self):
        pass
    
    @args(bool, [E])
    def offer(self, e):
        pass
    
    @args(bool, [E, long, TimeUnit])
    def offer(self, e, timeout, unit):
        pass
    
    @args(E, [])
    def peek(self):
        pass
    
    @args(E, [])
    def poll(self):
        pass
    
    @args(E, [long, TimeUnit])
    def poll(self, timeout, unit):
        pass
    
    @args(void, [E])
    def put(self, e):
        pass
    
    @args(int, [])
    def remainingCapacity(self):
        pass
    
    @args(bool, [Object])
    def remove(self, o):
        pass
    
    @args(int, [])
    def size(self):
        pass
    
    @args(E, [])
    def take(self):
        pass
    
    @args([E], [[E]])
    def toArray(self, a):
        pass
    
    @args([Object], [])
    def toArray(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass


class RejectedExecutionHandler:

    @args(void, [Runnable, ThreadPoolExecutor])
    def rejectedExecution(self, r, executor):
        pass


class RunnableFuture:

    def run(self):
        pass


class ThreadPoolExecutor(AbstractExecutorService):

    class AbortPolicy(Object):
    
        __interfaces__ = [RejectedExecutionHandler]
        
        def __init__(self):
            pass
        
        @args(void, [Runnable, ThreadPoolExecutor])
        def rejectedExecution(self, r, e):
            pass
    
    class CallerRunsPolicy(Object):
    
        __interfaces__ = [RejectedExecutionHandler]
        
        def __init__(self):
            pass
        
        @args(void, [Runnable, ThreadPoolExecutor])
        def rejectedExecution(self, r, e):
            pass
    
    class DiscardOldestPolicy(Object):
    
        __interfaces__ = [RejectedExecutionHandler]
        
        def __init__(self):
            pass
        
        @args(void, [Runnable, ThreadPoolExecutor])
        def rejectedExecution(self, r, e):
            pass
    
    class DiscardPolicy(Object):
    
        __interfaces__ = [RejectedExecutionHandler]
        
        def __init__(self):
            pass
        
        @args(void, [Runnable, ThreadPoolExecutor])
        def rejectedExecution(self, r, e):
            pass
    
    @args(void, [int, int, long, TimeUnit, BlockingQueue(Runnable)])
    def __init__(self, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue):
        pass
    
    @args(void, [int, int, long, TimeUnit, BlockingQueue(Runnable), ThreadFactory])
    def __init__(self, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory):
        pass
    
    @args(void, [int, int, long, TimeUnit, BlockingQueue(Runnable), RejectedExecutionHandler])
    def __init__(self, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler):
        pass
    
    @args(void, [int, int, long, TimeUnit, BlockingQueue(Runnable), ThreadFactory, RejectedExecutionHandler])
    def __init__(self, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler):
        pass
    
    @args(void, [bool])
    def allowCoreThreadTimeOut(self, value):
        pass
    
    @args(bool, [])
    def allowsCoreThreadTimeOut(self):
        pass
    
    @args(bool, [long, TimeUnit])
    def awaitTermination(self, timeout, unit):
        pass
    
    @args(void, [Runnable])
    def execute(self, command):
        pass
    
    @args(int, [])
    def getActiveCount(self):
        pass
    
    @args(long, [])
    def getCompletedTaskCount(self):
        pass
    
    @args(int, [])
    def getCorePoolSize(self):
        pass
    
    @args(long, [TimeUnit])
    def getKeepAliveTime(self, unit):
        pass
    
    @args(int, [])
    def getLargestPoolSize(self):
        pass
    
    @args(int, [])
    def getMaximumPoolSize(self):
        pass
    
    @args(int, [])
    def getPoolSize(self):
        pass
    
    @args(BlockingQueue(Runnable), [])
    def getQueue(self):
        pass
    
    @args(RejectedExecutionHandler, [])
    def getRejectedExecutionHandler(self):
        pass
    
    @args(long, [])
    def getTaskCount(self):
        pass
    
    @args(ThreadFactory, [])
    def getThreadFactory(self):
        pass
    
    @args(bool, [])
    def isShutdown(self):
        pass
    
    @args(bool, [])
    def isTerminated(self):
        pass
    
    @args(bool, [])
    def isTerminating(self):
        pass
    
    @args(int, [])
    def prestartAllCoreThreads(self):
        pass
    
    @args(bool, [])
    def prestartCoreThread(self):
        pass
    
    def purge(self):
        pass
    
    @args(bool, [Runnable])
    def remove(self, task):
        pass
    
    @args(void, [int])
    def setCorePoolSize(self, corePoolSize):
        pass
    
    @args(void, [long, TimeUnit])
    def setKeepAliveTime(self, time, unit):
        pass
    
    @args(void, [int])
    def setMaximumPoolSize(self, maximumPoolSize):
        pass
    
    @args(void, [RejectedExecutionHandler])
    def setRejectedExecutionHandler(self, handler):
        pass
    
    @args(void, [ThreadFactory])
    def setThreadFactory(self, threadFactory):
        pass
    
    def shutdown(self):
        pass
    
    @args(List(Runnable), [])
    def shutdownNow(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @protected
    @args(void, [Runnable, Throwable])
    def afterExecute(self, r, t):
        pass
    
    @protected
    @args(void, [Thread, Runnable])
    def beforeExecute(self, t, r):
        pass
    
    @protected
    def finalize(self):
        pass
    
    @protected
    def terminated(self):
        pass


class TimeUnit(Enum):

    __static_fields__ = {
        "DAYS": TimeUnit,
        "HOURS": TimeUnit,
        "MICROSECONDS": TimeUnit,
        "MILLISECONDS": TimeUnit,
        "MINUTES": TimeUnit,
        "NANOSECONDS": TimeUnit,
        "SECONDS": TimeUnit
        }
    
    @args(long, [long, TimeUnit])
    def convert(self, sourceDuration, sourceUnit):
        pass
    
    @args(void, [long])
    def sleep(self, timeout):
        pass
    
    @args(void, [Thread, long])
    def timedJoin(self, thread, timeout):
        pass
    
    @args(void, [Object, long])
    def timedWait(self, object, timeout):
        pass
    
    @args(long, [long])
    def toDays(self, duration):
        pass
    
    @args(long, [long])
    def toHours(self, duration):
        pass
    
    @args(long, [long])
    def toMicros(self, duration):
        pass
    
    @args(long, [long])
    def toMillis(self, duration):
        pass
    
    @args(long, [long])
    def toMinutes(self, duration):
        pass
    
    @args(long, [long])
    def toNanos(self, duration):
        pass
    
    @args(long, [long])
    def toSeconds(self, duration):
        pass
    
    @static
    @args(TimeUnit, [String])
    def valueOf(name):
        pass
    
    @final
    @args([TimeUnit], [])
    def values(self):
        pass
