__package__ = "com.example.if2"

from java.lang import String

from Common.helper import TestActivity

class IfActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        a = 1
        if a == 1:
            b = 1
        else:
            c = b
        
        self.showResult("", s)
