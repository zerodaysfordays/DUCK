__package__ = "com.example.assignments3"

from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class AssignmentsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        s = ""
        
        a = 1
        b = float(2)
        c = double(3)
        d = long(4)
        
        # These assignments should work because the sizes of the values are the
        # same.
        x = a
        y = c
        
        a = b
        b = x
        c = d
        d = y
        s1 = str(a) + " " + str(b) + " " + str(c) + " " + str(d)
        
        s += s1 + "\n"
        if s1 != "2 1.0 4.0 3":
            s += "Test failed"
        else:
            s += "Test passed!"
        
        view.setText(s)
        self.setContentView(view)
