# -*- coding: utf8 -*-

__package__ = "com.example.unicode2"

from Common.helper import TestActivity

class UnicodeActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        text = u"東京都\n北京市\nMосква\n서울시"
        
        # The following string is included to give the compiler a byte string
        # to handle in addition to other kinds of strings.
        s = "\xff"
        
        self.showResult(u"\u6771\u4eac\u90fd\n\u5317\u4eac\u5e02\n"
                        u"M\u043e\u0441\u043a\u0432\u0430\n"
                        u"\uc11c\uc6b8\uc2dc", text, True)
