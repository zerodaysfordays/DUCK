
Python syntax for list and dictionary literals
----------------------------------------------

Add support for [] and {} syntax, deferring resolution of parameter types until
values are retrieved or stored in them.
