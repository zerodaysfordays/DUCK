__package__ = "com.example.assignments1"

from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class AssignmentsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        a = 1
        b = float(2)
        c = double(3)
        d = long(4)
        
        view.setText("Test passed!")
        self.setContentView(view)
