#!/usr/bin/env python

"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, sys

from DUCK.Tools import buildhelper

levels = [
    ("tyX  Xyu"
     "g X  X j"
     "g *  * j"
     "X*XX*X*X"
     "C      j"
     "XC   ZCj"
     "gXX#XX j"
     "g    X Z"
     "g X  X A"
     "g XXX  j"
     "C      j"
     "XC     j"
     "g D C  j"
     "g X  C j"
     "g  O ACj"
     "b nnnnnm")
    ]

app_name = "Fluid"
package_name = "com.example.fluid"
res_files = {
    "drawable": {"ic_launcher": "icon.svg"},
    "raw": {"wall1":        "images/wall1.png",
            "wall2":        "images/wall2.png",
            "wall3":        "images/wall3.png",
            "iceblock":     "images/iceblock.png",
            "grate":        "images/grate.png",
            "block":        "images/block.png",
            "left":         "images/left.png",
            "right":        "images/right.png",
            "top":          "images/top.png",
            "bottom":       "images/bottom.png",
            "topleft":      "images/topleft.png",
            "topright":     "images/topright.png",
            "bottomleft":   "images/bottomleft.png",
            "bottomright":  "images/bottomright.png",
            "doorleft":     "images/doorleft.png",
            "doorright":    "images/doorright.png",
            },
    "string": {"level": levels[0]}
    }
code_file = "fluid.py"
include_paths = []
layout = None
features = []
permissions = []
docs_dir = os.getenv("DOCS_DIR")

if __name__ == "__main__":

    args = sys.argv[:]
    
    result = buildhelper.main(__file__, app_name, package_name, res_files,
        layout, code_file, include_paths, features, permissions, args,
        docs_dir = docs_dir)
    
    sys.exit(result)
