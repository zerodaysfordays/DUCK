# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.util"

from java.lang import CharSequence, Exception, Object, String, Throwable
from android.content.res import Resources

class AndroidException(Exception):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, name):
        pass
    
    @args(void, [String, Throwable])
    def __init__(self, name, cause):
        pass
    
    @args(void, [Exception])
    def __init__(self, cause):
        pass


class AttributeSet:

    @args(bool, [String, String, bool])
    def getAttributeBooleanValue(self, namespace, attribute, defaultValue):
        pass
    
    @args(bool, [int, bool])
    def getAttributeBooleanValue(self, index, defaultValue):
        pass
    
    @args(int, [])
    def getAttributeCount(self):
        pass
    
    @args(float, [int, float])
    def getAttributeFloatValue(self, index, defaultValue):
        pass
    
    @args(float, [String, String, float])
    def getAttributeFloatValue(self, namespace, attribute, defaultValue):
        pass
    
    @args(int, [String, String, int])
    def getAttributeIntValue(self, namespace, attribute, defaultValue):
        pass
    
    @args(int, [int, int])
    def getAttributeIntValue(self, index, defaultValue):
        pass
    
    @args(int, [String, String, [String], int])
    def getAttributeListValue(self, namespace, attribute, options, defaultValue):
        pass
    
    @args(int, [int, [String], int])
    def getAttributeListValue(self, index, options, defaultValue):
        pass
    
    @args(String, [int])
    def getAttributeName(self, index):
        pass
    
    @args(int, [int])
    def getAttributeNameResource(self, index):
        pass
    
    @args(int, [String, String, int])
    def getAttributeResourceValue(self, namespace, attribute, defaultValue):
        pass
    
    @args(int, [int, int])
    def getAttributeResourceValue(self, index, defaultValue):
        pass
    
    @args(int, [int, int])
    def getAttributeUnsignedIntValue(self, index, defaultValue):
        pass
    
    @args(int, [String, String, int])
    def getAttributeUnsignedIntValue(self, namespace, attribute, defaultValue):
        pass
    
    @args(String, [int])
    def getAttributeValue(self, index):
        pass
    
    @args(String, [String, String])
    def getAttributeValue(self, namespace, name):
        pass
    
    @args(String, [])
    def getClassAttribute(self):
        pass
    
    @args(String, [])
    def getIdAttribute(self):
        pass
    
    @args(int, [int])
    def getAttributeResourceValue(self, defaultValue):
        pass
    
    @args(String, [])
    def getPositionDescription(self):
        pass
    
    @args(int, [])
    def getStyleAttribute(self):
        pass


class DisplayMetrics(Object):

    DENSITY_400 = 400
    DENSITY_DEFAULT = 160
    DENSITY_HIGH = 240
    DENSITY_LOW = 120
    DENSITY_MEDIUM = 160
    DENSITY_TV = 213
    DENSITY_XHIGH = 320
    DENSITY_XXHIGH = 480
    DENSITY_XXXHIGH = 640
    
    __fields__ = {
        "density": float,
        "densityDpi": int,
        "heightPixels": int,
        "scaledDensity": float,
        "widthPixels": int,
        "xdpi": int,
        "ydpi": int
        }
    
    def __init__(self):
        pass
    
    @args(bool, [DisplayMetrics])
    def equals(self, other):
        pass
    
    @args(void, [DisplayMetrics])
    def setTo(self, o):
        pass
    
    @args(void, [])
    def setToDefaults(self):
        pass


class Log(Object):

    ASSERT  = 7
    DEBUG   = 3
    ERROR   = 6
    INFO    = 4
    VERBOSE = 2
    WARN    = 5
    
    @static
    @args(int, [String, String])
    def d(tag, msg):
        pass
    
    @static
    @args(int, [String, String, Throwable])
    def d(tag, msg, throwable):
        pass
    
    @static
    @args(int, [String, String])
    def e(tag, msg):
        pass
    
    @static
    @args(int, [String, String, Throwable])
    def e(tag, msg, throwable):
        pass
    
    @static
    @args(String, [Throwable])
    def getStackTraceString(throwable):
        pass
    
    @static
    @args(int, [String, String])
    def i(tag, msg):
        pass
    
    @static
    @args(int, [String, String, Throwable])
    def i(tag, msg, throwable):
        pass
    
    @static
    @args(bool, [String, int])
    def isLoggable(tag, level):
        pass
    
    @static
    @args(int, [int, String, String])
    def i(priority, tag, msg):
        pass
    
    @static
    @args(int, [String, String])
    def v(tag, msg):
        pass
    
    @static
    @args(int, [String, String, Throwable])
    def v(tag, msg, throwable):
        pass
    
    @static
    @args(int, [String, Throwable])
    def w(tag, throwable):
        pass
    
    @static
    @args(int, [String, String, Throwable])
    def w(tag, msg, throwable):
        pass
    
    @static
    @args(int, [String, String])
    def w(tag, msg):
        pass
    
    @static
    @args(int, [String, Throwable])
    def wtf(tag, throwable):
        pass
    
    @static
    @args(int, [String, String, Throwable])
    def wtf(tag, msg, throwable):
        pass
    
    @static
    @args(int, [String, String])
    def wtf(tag, msg):
        pass


class LruCache(Object):

    __parameters__ = [K, V]
    
    @args(void, [int])
    def __init__(self, maxSize):
        pass
    
    @synchronized
    @final
    @args(int, [])
    def createCount(self):
        pass
    
    @final
    @args(void, [])
    def evictAll(self):
        pass
    
    @synchronized
    @final
    @args(int, [])
    def evictionCount(self):
        pass
    
    @final
    @args(V, [K])
    def get(self, key):
        pass
    
    @synchronized
    @final
    @args(int, [])
    def hitCount(self):
        pass
    
    @synchronized
    @final
    @args(int, [])
    def maxSize(self):
        pass
    
    @synchronized
    @final
    @args(int, [])
    def missCount(self):
        pass
    
    @final
    @args(V, [K, V])
    def put(self, key, value):
        pass
    
    @synchronized
    @final
    @args(int, [])
    def putCount(self):
        pass
    
    @final
    @args(V, [K])
    def remove(self, key):
        pass
    
    @args(void, [int])
    def resize(self, maxSize):
        pass
    
    @synchronized
    @final
    @args(int, [])
    def size(self):
        pass
    
    @synchronized
    @final
    @args(Map(K, V), [])
    def snapshot(self):
        pass
    
    @synchronized
    @final
    @args(String, [])
    def toString(self):
        pass
    
    @args(void, [int])
    def trimToSize(self, maxSize):
        pass
    
    @protected
    @args(V, [K])
    def create(self, key):
        pass
    
    @protected
    @args(void, [bool, K, V, V])
    def entryRemoved(self, evicted, key, oldValue, newValue):
        pass
    
    @protected
    @args(int, [K, V])
    def sizeOf(self, key, value):
        pass


class TypedValue(Object):

    COMPLEX_MANTISSA_MASK           = 0x00ffffff
    COMPLEX_MANTISSA_SHIFT          = 8
    COMPLEX_RADIX_0p23              = 3
    COMPLEX_RADIX_16p7              = 1
    COMPLEX_RADIX_23p0              = 0
    COMPLEX_RADIX_8p15              = 2
    COMPLEX_RADIX_MASK              = 3
    COMPLEX_UNIT_DIP                = 1
    COMPLEX_UNIT_FRACTION           = 0
    COMPLEX_UNIT_FRACTION_PARENT    = 1
    COMPLEX_UNIT_IN                 = 4
    COMPLEX_UNIT_MASK               = 0x0000000f
    COMPLEX_UNIT_MM                 = 5
    COMPLEX_UNIT_PT                 = 3
    COMPLEX_UNIT_PX                 = 0
    COMPLEX_UNIT_SHIFT              = 0
    COMPLEX_UNIT_SP                 = 2
    
    DATA_NULL_EMPTY     = 1
    DATA_NULL_UNDEFINED = 0
    
    DENSITY_DEFAULT     = 0
    DENSITY_NONE        = 0x0000ffff
    
    TYPE_ATTRIBUTE          = 2
    TYPE_DIMENSION          = 5
    TYPE_FIRST_COLOR_INT    = 28
    TYPE_FIRST_INT          = 16
    TYPE_FLOAT              = 4
    TYPE_FRACTION           = 6
    TYPE_INT_BOOLEAN        = 18
    TYPE_INT_COLOR_ARGB4    = 30
    TYPE_INT_COLOR_ARGB8    = 28
    TYPE_INT_COLOR_RGB4     = 31
    TYPE_INT_COLOR_RGB8     = 29
    TYPE_INT_DEC            = 16
    TYPE_INT_HEX            = 17
    TYPE_LAST_COLOR_INT     = 31
    TYPE_LAST_INT           = 31
    TYPE_NULL               = 0
    TYPE_REFERENCE          = 1
    TYPE_STRING             = 3
    
    __fields__ = {
        "assetCookie": int,
        "changingConfigurations": int,
        "data": int,
        "density": int,
        "resourceId": int,
        "string": CharSequence,
        "type": int
        }
    
    def __init__(self):
        pass
    
    @static
    @args(float, [int, float, DisplayMetrics])
    def applyDimension(unit, value, metrics):
        pass
    
    @final
    @args(CharSequence, [])
    def coerceToString(self):
        pass
    
    @final
    @static
    @args(String, [int, int])
    def coerceToString(type, data):
        pass
    
    @static
    @args(float, [int, DisplayMetrics])
    def complexToDimension(data, metrics):
        pass
    
    @static
    @args(int, [int, DisplayMetrics])
    def complexToPixelOffset(data, metrics):
        pass
    
    @static
    @args(int, [int, DisplayMetrics])
    def complexToPixelSize(data, metrics):
        pass
    
    @static
    @args(float, [int])
    def complexToFloat(complex):
        pass
    
    @static
    @args(float, [int, float, float])
    def complexToFraction(data, base, pbase):
        pass
    
    @args(int, [])
    def getComplexUnit(self):
        pass
    
    @args(float, [DisplayMetrics])
    def getDimension(self, metrics):
        pass
    
    @final
    @args(float, [])
    def getFloat(self):
        pass
    
    @args(float, [float, float])
    def getFraction(self, base, pbase):
        pass
    
    @args(void, [TypedValue])
    def setTo(self, other):
        pass
