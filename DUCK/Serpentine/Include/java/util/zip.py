# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.util.zip"

import java.io
import java.lang

class CRC32(java.lang.Object):

    def __init__(self):
        pass
    
    @args(long, [])
    def getValue(self):
        pass
    
    def reset(self):
        pass
    
    @args(void, [[byte], int, int])
    def update(self, buf, offset, byteCount):
        pass
    
    @args(void, [int])
    def update(self, value):
        pass
    
    @args(void, [[byte]])
    def update(self, buf):
        pass


class GZIPInputStream(InflaterInputStream):

    GZIP_MAGIC = 0x00008b1f
    
    __fields__ = {
        "crc": CRC32,
        "eos": bool
        }
    
    @args(void, [java.io.InputStream])
    def __init__(self, inputStream):
        pass
    
    @args(void, [java.io.InputStream, int])
    def __init__(self, inputStream, size):
        pass


class Inflater(java.lang.Object):

    def __init__(self):
        pass
    
    @args(void, [bool])
    def __init__(self, noHeader):
        pass
    
    @synchronized
    def end(self):
        pass
    
    @synchronized
    @args(bool, [])
    def finished(self):
        pass
    
    @synchronized
    @args(int, [])
    def getAdler(self):
        pass
    
    @synchronized
    @args(long, [])
    def getBytesRead(self):
        pass
    
    @synchronized
    @args(long, [])
    def getBytesWritten(self):
        pass
    
    @synchronized
    @args(int, [])
    def getRemaining(self):
        pass
    
    @synchronized
    @args(int, [])
    def getTotalIn(self):
        pass
    
    @synchronized
    @args(int, [])
    def getTotalOut(self):
        pass
    
    @synchronized
    @args(int, [[byte], int, int])
    def inflate(self, buf, offset, byteCount):
        pass
    
    @args(int, [[byte]])
    def inflate(self, buf):
        pass
    
    @synchronized
    @args(bool, [])
    def needsDictionary(self):
        pass
    
    @synchronized
    @args(bool, [])
    def needsInput(self):
        pass
    
    @synchronized
    def reset(self):
        pass
    
    @synchronized
    @args(void, [[byte], int, int])
    def setDictionary(self, dictionary, offset, byteCount):
        pass
    
    @synchronized
    @args(void, [[byte]])
    def setDictionary(self, dictionary):
        pass
    
    @synchronized
    @args(void, [[byte], int, int])
    def setInput(self, buf, offset, byteCount):
        pass
    
    @synchronized
    @args(void, [[byte]])
    def setInput(self, buf):
        pass


class InflaterInputStream(java.io.FilterInputStream):

    __fields__ = {
        "buf": [byte],
        "inf": Inflater,
        "len": int
        }
    
    @args(void, [java.io.InputStream])
    def __init__(self, inputStream):
        pass
    
    @args(void, [java.io.InputStream, Inflater])
    def __init__(self, inputStream, inflater):
        pass
    
    @args(void, [java.io.InputStream, Inflater, int])
    def __init__(self, inputStream, inflater, bufferSize):
        pass
