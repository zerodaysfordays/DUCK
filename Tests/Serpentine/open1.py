__package__ = "com.example.open1"

from java.lang import String

from Common.helper import TestActivity

class OpenActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        try:
            f = open("/no_such_file.txt", "rw")
            s = ""
        except IOError, e1:
            s = str(e1)
        except ValueError, e2:
            s = str(e2)
        
        self.showResult("serpentine.exceptions.IOError: Cannot open file '/no_such_file.txt' in mode 'rw'", s)
