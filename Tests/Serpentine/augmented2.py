__package__ = "com.example.augmented2"

from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class AugmentedActivity(Activity):

    __interfaces__ = []
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        self.i = 0
        while self.i < 12:
            self.i += 1
        
        if self.i == 12:
            view.setText("Test passed!")
        else:
            view.setText("Test failed!")
        
        self.setContentView(view)
