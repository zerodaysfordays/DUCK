__package__ = "com.example.logical1"

from android.app import Activity
from android.os import Bundle
from android.widget import TextView

# Application classes

class BitsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        
        a = True
        b = False
        
        s = ""
        s += "not " + str(a) + " -> " + str(not a) + "\n"
        s += "not " + str(b) + " -> " + str(not b) + "\n"
        
        view.setText(s)
        self.setContentView(view)
