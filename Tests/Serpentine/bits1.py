__package__ = "com.example.bits1"

from java.lang import Integer, Object, String
from java.util import ArrayList
from android.app import Activity
from android.os import Bundle
from android.widget import TextView

# Application classes

class ObjectList(ArrayList):

    __item_types__ = [Object]
    
    def __init__(self):
        ArrayList.__init__(self)


class BitsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        
        a = 0x10000000
        b = 0x00001000
        c = 0xffff0000
        d = 0x0000ffff
        
        s = ""
        # Pass explictly constructed lists of Objects to String.format until
        # support for trailing lists of arguments has been implemented.
        formatList = ObjectList()
        formatList.add(Integer(a))
        s += "a = " + String.format("%x", formatList.toArray()) + "\n"
        formatList.clear()
        formatList.add(Integer(b))
        s += "b = " + String.format("%x", formatList.toArray()) + "\n"
        formatList.clear()
        formatList.add(Integer(a | b))
        s += "a | b -> " + String.format("%x", formatList.toArray()) + "\n"
        formatList.clear()
        formatList.add(Integer(a & b))
        s += "a & b -> " + String.format("%x", formatList.toArray()) + "\n"
        formatList.clear()
        formatList.add(Integer(a ^ b))
        s += "a ^ b -> " + String.format("%x", formatList.toArray()) + "\n"
        
        view.setText(s)
        
        self.setContentView(view)
