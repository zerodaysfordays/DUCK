# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.security"

from java.io import Serializable
from java.lang import Object, String
from java.util import Enumeration, Random

class Guard:

    @args(void, [Object])
    def checkGuard(self, object):
        pass


class Permission(Object):

    __interfaces__ = [Guard, Serializable]
    
    @args(void, [String])
    def __init__(self, name):
        pass
    
    @args(void, [Object])
    def checkGuard(self, object):
        pass
    
    @abstract
    @args(String, [])
    def getActions(self):
        pass
    
    @final
    @args(String, [])
    def getName(self):
        pass
    
    @abstract
    @args(bool, [Permission])
    def implies(self, permission):
        pass
    
    @args(PermissionCollection, [])
    def newPermissionCollection(self):
        pass


class PermissionCollection(Object):

    __interfaces__ = [Serializable]
    
    def __init__(self):
        pass
    
    @abstract
    @args(void, [Permission])
    def add(self, permission):
        pass
    
    @abstract
    @args(Enumeration(Permission), [])
    def elements(self):
        pass
    
    @abstract
    @args(bool, [Permission])
    def implies(self, permission):
        pass
    
    @args(bool, [])
    def isReadOnly(self):
        pass
    
    def setReadOnly(self):
        pass


class Principal:

    @args(bool, [Object])
    def equals(self, object):
        pass
    
    @args(String, [])
    def getName(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass


class SecureRandom(Random):

    def __init__(self):
        pass
    
    @args(void, [[byte]])
    def __init__(self, seed):
        pass
    
    @protected
    @args(void, [SecureRandomSpi, Provider])
    def __init__(self, secureRandomSpi, provider):
        pass
    
    @args([byte], [int])
    def generateSeed(self, numBytes):
        pass
    
    @args(String, [])
    def getAlgorithm(self):
        pass
    
    @static
    @args(SecureRandom, [String, String])
    def getInstance(algorithm, provider):
        pass
    
    @static
    @args(SecureRandom, [String, Provider])
    def getInstance(algorithm, provider):
        pass
    
    @static
    @args(SecureRandom, [String])
    def getInstance(algorithm):
        pass
    
    @final
    @args(Provider, [])
    def getProvider(self):
        pass
    
    @static
    @args([byte], [int])
    def getSeed(numBytes):
        pass
    
    @synchronized
    @args(void, [[byte]])
    def nextBytes(self, bytes):
        pass
    
    @synchronized
    @args(void, [[byte]])
    def setSeed(self, seed):
        pass
    
    @args(void, [long])
    def setSeed(self, seed):
        pass

    @protected
    @final
    @args(int, [int])
    def next(self, numBits):
        pass
