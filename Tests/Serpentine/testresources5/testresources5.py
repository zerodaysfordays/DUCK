__package__ = "com.example.testresources5"

from java.lang import String
from android.app import Activity
import android.os
from android.widget import TextView

import app_resources

class TestResourcesActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        res = self.getResources()
        
        things = res.getIntArray(app_resources.R.array.things)
        strings = res.getStringArray(app_resources.R.array.strings)
        colours = res.obtainTypedArray(app_resources.R.array.colours)
        booleans = res.obtainTypedArray(app_resources.R.array.booleans)
        
        actual = str(len(things)) + ": "
        
        for i in things:
            actual += str(i) + " "
        
        actual += "\n"
        
        actual += str(len(strings)) + ": "
        
        for j in strings:
            actual += j + " "
        
        actual += "\n"
        
        l = len(colours)
        actual += str(l) + ": "
        
        k = 0
        while k < l:
            colour = colours.getColor(k, 0)
            actual += str(colour) + " "
            k += 1
        
        actual += "\n"
        
        l = len(booleans)
        actual += str(l) + ": "
        
        k = 0
        while k < l:
            actual += str(booleans.getBoolean(k, False)) + " "
            k += 1
        
        expected = ("3: 123 456 -789 \n5: List of strings to test \n"
                    "4: 3840 32768 32767 1078951727 \n"
                    "2: true false ")
        
        self.showResult(expected, actual)
    
    @args(void, [String, String])
    def showResult(self, expected, actual):
        
        view = TextView(self)
        
        if actual == expected:
            view.setText("Test passed!")
        else:
            view.setText("Test failed:\nExpected:\n" + expected + "\nObtained:\n" + actual)
            view.setBackgroundColor(0xff400000)
        
        self.setContentView(view)
