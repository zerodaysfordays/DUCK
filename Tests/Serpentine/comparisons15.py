__package__ = "com.example.comparisons15"

import android.os

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        
        t = True
        f = False
        
        if t and f:
            s = "True and False -> True"
        else:
            s = "True and False -> False"
        
        s += "\n"
        
        if t or f:
            s += "True and False -> True"
        else:
            s += "True and False -> False"
        
        expected = "True and False -> False\nTrue and False -> True"
        actual = s
        self.showResult(expected, actual)
