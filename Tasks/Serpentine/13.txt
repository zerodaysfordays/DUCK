Apply API level information to the headers
==========================================

Also add support to the compiler and build system to hide methods outside the
desired API range.

Possibly also consider adding support for simple compile-time version tests
in the source code to be able to compile different code for different API
levels.
