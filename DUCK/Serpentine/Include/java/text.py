# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.text"

from java.lang import Object, String
from java.util import Date, Locale, TimeZone

class DateFormat(Format):

    #__fields__ = {
    #    "calendar": Calendar,
    #    "numberFormat": NumberFormat
    #    }
    
    AM_PM_FIELD = 14
    DATE_FIELD = 3
    DAY_OF_WEEK_FIELD = 9
    DAY_OF_WEEK_IN_MONTH_FIELD = 11
    DAY_OF_YEAR_FIELD = 10
    DEFAULT = 2
    ERA_FIELD = 0
    FULL = 0
    HOUR0_FIELD = 16
    HOUR1_FIELD = 15
    HOUR_OF_DAY0_FIELD = 5
    HOUR_OF_DAY1_FIELD = 4
    LONG = 1
    MEDIUM = 2
    MILLISECOND_FIELD = 8
    MINUTE_FIELD = 6
    MONTH_FIELD = 2
    SECOND_FIELD = 7
    SHORT = 3
    TIMEZONE_FIELD = 17
    WEEK_OF_MONTH_FIELD = 13
    WEEK_OF_YEAR_FIELD = 12
    YEAR_FIELD = 1
    
    def __init__(self):
        pass
    
    @args(Object, [])
    def clone(self, ):
        pass
    
    @args(boolean, [Object])
    def equals(self, object):
        pass
    
    #@abstract
    #@args(StringBuffer, [Date, StringBuffer, FieldPosition])
    #def format(self, date, buffer, field):
    #    pass
    
    #@final
    #@args(StringBuffer, [Object, StringBuffer, FieldPosition])
    #def format(self, object, buffer, field):
    #    pass

    @final
    @args(String, [Date])
    def format(self, date):
        pass
    
    @static
    @args([Locale], [])
    def getAvailableLocales():
        pass
    
    @args(Calendar, [])
    def getCalendar(self):
        pass
    
    @final
    @static
    @args(DateFormat, [int])
    def getDateInstance(style):
        pass
    
    @final
    @static
    @args(DateFormat, [int, Locale])
    def getDateInstance(style, locale):
        pass
    
    @final
    @static
    @args(DateFormat, [])
    def getDateInstance():
        pass
    
    @final
    @static
    @args(DateFormat, [int, int])
    def getDateTimeInstance(dateStyle, timeStyle):
        pass
    
    @final
    @static
    @args(DateFormat, [int, int, Locale])
    def getDateTimeInstance(dateStyle, timeStyle, locale):
        pass
    
    @final
    @static
    @args(DateFormat, [])
    def getDateTimeInstance():
        pass
    
    @final
    @static
    @args(DateFormat, [])
    def getInstance():
        pass
    
    #@args(NumberFormat, [])
    #def getNumberFormat(self):
    #    pass
    
    @final
    @static
    @args(DateFormat, [int])
    def getTimeInstance(style):
        pass
    
    @final
    @static
    @args(DateFormat, [int, Locale])
    def getTimeInstance(style, locale):
        pass
    
    @final
    @static
    @args(DateFormat, [])
    def getTimeInstance():
        pass
    
    @args(bool, [])
    def isLenient(self):
        pass
    
    @args(Date, [String])
    def parse(self, string):
        pass
    
    @abstract
    @args(Date, [String, ParsePosition])
    def parse(self, string, position):
        pass
    
    @args(Object, [String, ParsePosition])
    def parseObject(self, string, position):
        pass
    
    @args(void, [Calendar])
    def setCalendar(self, cal):
        pass
    
    @args(void, [bool])
    def setLenient(self, value):
        pass
    
    #@args(void, [NumberFormat])
    #def setNumberFormat(self, format):
    #    pass
    
    @args(void, [TimeZone])
    def setTimeZone(self, timezone):
        pass

    
class Format(Object):

    ### class Field
    
    def __init__(self):
        pass
    
    @final
    @args(String, [Object])
    def format(self, object):
        pass
    
    @args(Object, [String])
    def parseObject(self, string):
        pass


class ParsePosition(Object):

    @args(void, [int])
    def __init__(self, index):
        pass
    
    @args(bool, [Object])
    def equals(self, object):
        pass
    
    @args(int, [])
    def getErrorIndex(self):
        pass
    
    @args(int, [])
    def getIndex(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(void, [int])
    def setErrorIndex(self, index):
        pass
    
    @args(void, [int])
    def setIndex(self, index):
        pass
    
    @args(String, [])
    def toString(self):
        pass


class SimpleDateFormat(DateFormat):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, pattern):
        pass
    
    ### ...
    
    @args(void, [String, Locale])
    def __init__(self, template, locale):
        pass
    
    @args(void, [String])
    def applyLocalizedPattern(self, template):
        pass
    
    @args(void, [String])
    def applyPattern(self, template):
        pass
    
    @args(Object, [])
    def clone(self):
        pass
    
    @args(bool, [Object])
    def equals(self, object):
        pass
    
    #@args(StringBuffer, [Date, StringBuffer, FieldPosition])
    #def format(self, date, buffer, fieldPos):
    #    pass
    
    #@args(AttributedCharacterIterator, [Object])
    #def formatToCharacterIterator(self, object):
    #    pass
    
    @args(Date, [])
    def get2DigitYearStart(self):
        pass
    
    #@args(DateFormatSymbols, [])
    #def getDateFormatSymbols(self):
    #    pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(Date, [String, ParsePosition])
    def parse(self, string, position):
        pass
    
    @args(void, [Date])
    def set2DigitYearStart(self, date):
        pass
    
    #@args(void, [DateFormatSymbols])
    #def setDateFormatSymbols(self, value):
    #    pass
    
    @args(String, [])
    def toLocalizedPattern(self):
        pass
    
    @args(String, [])
    def toPattern(self):
        pass
