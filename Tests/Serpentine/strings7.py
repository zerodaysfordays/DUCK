__package__ = "com.example.strings7"

from java.lang import String

from Common.helper import TestActivity

class StringsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = "Hello world!"
        b = "w" in a
        c = "?" in a
        
        s = str(b) + " " + str(c)
        expected = "true false"
        
        self.showResult(expected, s)
