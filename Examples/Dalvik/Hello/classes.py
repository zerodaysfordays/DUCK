"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os

from Dalvik.dex import *
from Dalvik.bytecode import *

strings = [
    '<init>',
    'BuildConfig.java',
    'CLASS',
    'CONSTRUCTOR',
    'DEBUG',
    'FIELD',
    'HelloActivity.java',
    'I',
    'L',
    'LOCAL_VARIABLE',
    'Landroid/annotation/SuppressLint;',
    'Landroid/annotation/TargetApi;',
    'Landroid/app/Activity;',
    'Landroid/os/Bundle;',
    'Lcom/example/hello/BuildConfig;',
    'Lcom/example/hello/HelloActivity;',
    'Lcom/example/hello/R$attr;',
    'Lcom/example/hello/R$drawable;',
    'Lcom/example/hello/R$layout;',
    'Lcom/example/hello/R$string;',
    'Lcom/example/hello/R;',
    'Ldalvik/annotation/EnclosingClass;',
    'Ldalvik/annotation/InnerClass;',
    'Ldalvik/annotation/MemberClasses;',
    'Ljava/lang/Object;',
    'Ljava/lang/String;',
    'Ljava/lang/annotation/Annotation;',
    'Ljava/lang/annotation/ElementType;',
    'Ljava/lang/annotation/Retention;',
    'Ljava/lang/annotation/RetentionPolicy;',
    'Ljava/lang/annotation/Target;',
    'METHOD',
    'PARAMETER',
    'R.java',
    'SuppressLint.java',
    'TYPE',
    'TargetApi.java',
    'V',
    'VI',
    'VL',
    'Z',
    '[Ljava/lang/String;',
    'accessFlags',
    'app_name',
    'attr',
    'drawable',
    'ic_launcher',
    'layout',
    'main',
    'name',
    'onCreate',
    'setContentView',
    'string',
    'value'
    ]

types = [
    Int(),
    Name('Landroid/annotation/SuppressLint;'),
    Name('Landroid/annotation/TargetApi;'),
    Name('Landroid/app/Activity;'),
    Name('Landroid/os/Bundle;'),
    Name('Lcom/example/hello/BuildConfig;'),
    Name('Lcom/example/hello/HelloActivity;'),
    Name('Lcom/example/hello/R$attr;'),
    Name('Lcom/example/hello/R$drawable;'),
    Name('Lcom/example/hello/R$layout;'),
    Name('Lcom/example/hello/R$string;'),
    Name('Lcom/example/hello/R;'),
    Name('Ldalvik/annotation/EnclosingClass;'),
    Name('Ldalvik/annotation/InnerClass;'),
    Name('Ldalvik/annotation/MemberClasses;'),
    Name('Ljava/lang/Object;'),
    Name('Ljava/lang/String;'),
    Name('Ljava/lang/annotation/Annotation;'),
    Name('Ljava/lang/annotation/ElementType;'),
    Name('Ljava/lang/annotation/Retention;'),
    Name('Ljava/lang/annotation/RetentionPolicy;'),
    Name('Ljava/lang/annotation/Target;'),
    Void(),
    Boolean(),
    Array('[Ljava/lang/String;')
    ]

prototypes = [
    Prototype([Int()], Int(), []),
    Prototype([Void()], Void(), []),
    Prototype([Void(), Int()], Void(), [Int()]),
    Prototype([Void(), ReferenceType()], Void(), [Name('Landroid/os/Bundle;')]),
    Prototype([ReferenceType()], Array('[Ljava/lang/String;'), [])
    ]

fields = [
    Field(Name('Lcom/example/hello/BuildConfig;'), Boolean(), MemberName('DEBUG')),
    Field(Name('Lcom/example/hello/R$drawable;'), Int(), MemberName('ic_launcher')),
    Field(Name('Lcom/example/hello/R$layout;'), Int(), MemberName('main')),
    Field(Name('Lcom/example/hello/R$string;'), Int(), MemberName('app_name')),
    Field(Name('Ljava/lang/annotation/ElementType;'), Name('Ljava/lang/annotation/ElementType;'), MemberName('CONSTRUCTOR')),
    Field(Name('Ljava/lang/annotation/ElementType;'), Name('Ljava/lang/annotation/ElementType;'), MemberName('FIELD')),
    Field(Name('Ljava/lang/annotation/ElementType;'), Name('Ljava/lang/annotation/ElementType;'), MemberName('LOCAL_VARIABLE')),
    Field(Name('Ljava/lang/annotation/ElementType;'), Name('Ljava/lang/annotation/ElementType;'), MemberName('METHOD')),
    Field(Name('Ljava/lang/annotation/ElementType;'), Name('Ljava/lang/annotation/ElementType;'), MemberName('PARAMETER')),
    Field(Name('Ljava/lang/annotation/ElementType;'), Name('Ljava/lang/annotation/ElementType;'), MemberName('TYPE')),
    Field(Name('Ljava/lang/annotation/RetentionPolicy;'), Name('Ljava/lang/annotation/RetentionPolicy;'), MemberName('CLASS'))
    ]

methods = [
    Method(Name('Landroid/annotation/SuppressLint;'),
           Prototype([ReferenceType()], Array('[Ljava/lang/String;'), []),
           MemberName('value'),
           AccessFlags(ACC_ABSTRACT | ACC_PUBLIC),
           None),
    Method(Name('Landroid/annotation/TargetApi;'),
           Prototype([Int()], Int(), []),
           MemberName('value'),
           AccessFlags(ACC_ABSTRACT | ACC_PUBLIC),
           None),
    Method(Name('Landroid/app/Activity;'),
           Prototype([Void()], Void(), []),
           MemberName('<init>'),
           None,
           None),
    Method(Name('Landroid/app/Activity;'),
           Prototype([Void(), ReferenceType()], Void(), [Name('Landroid/os/Bundle;')]),
           MemberName('onCreate'),
           None,
           None),
    Method(Name('Lcom/example/hello/BuildConfig;'),
           Prototype([Void()], Void(), []),
           MemberName('<init>'),
           AccessFlags(ACC_CONSTRUCTOR | ACC_PUBLIC),
           Code(1, 1, 1, [], 0, assemble([
                invoke_direct(Count4(1), Index16(13), Register4(0)), # Object.<init>
                return_void()
                ]))),
    Method(Name('Lcom/example/hello/HelloActivity;'),
           Prototype([Void()], Void(), []),
           MemberName('<init>'),
           AccessFlags(ACC_CONSTRUCTOR | ACC_PUBLIC),
           Code(1, 1, 1, [], 0, assemble([
                invoke_direct(Count4(1), Index16(2), Register4(0)), # Activity.<init>
                return_void()
                ]))),
    Method(Name('Lcom/example/hello/HelloActivity;'),
           Prototype([Void(), ReferenceType()], Void(), [Name('Landroid/os/Bundle;')]),
           MemberName('onCreate'),
           AccessFlags(ACC_PUBLIC),
           Code(3, 2, 2, [], 0, assemble([
                invoke_super(Count4(2), Index16(3), Register4(1), Register4(2)),
                const_high_16(Register4(0), 32515), # 0x7f030000 (R.layout.main)
                invoke_virtual(Count4(2), Index16(7), Register4(1), Register4(0)),
                return_void()
                ]))),
    Method(Name('Lcom/example/hello/HelloActivity;'),
           Prototype([Void(), Int()], Void(), [Int()]),
           MemberName('setContentView'),
           None,
           None),
    Method(Name('Lcom/example/hello/R$attr;'),
           Prototype([Void()], Void(), []),
           MemberName('<init>'),
           AccessFlags(ACC_CONSTRUCTOR | ACC_PUBLIC),
           Code(1, 1, 1, [], 0, assemble([
                invoke_direct(Count4(1), Index16(13), Register4(0)), # Object.<init>
                return_void()
                ]))),
    Method(Name('Lcom/example/hello/R$drawable;'),
           Prototype([Void()], Void(), []),
           MemberName('<init>'),
           AccessFlags(ACC_CONSTRUCTOR | ACC_PUBLIC),
           Code(1, 1, 1, [], 0, assemble([
                invoke_direct(Count4(1), Index16(13), Register4(0)), # Object.<init>
                return_void()
                ]))),
    Method(Name('Lcom/example/hello/R$layout;'),
           Prototype([Void()], Void(), []),
           MemberName('<init>'),
           AccessFlags(ACC_CONSTRUCTOR | ACC_PUBLIC),
           Code(1, 1, 1, [], 0, assemble([
                invoke_direct(Count4(1), Index16(13), Register4(0)), # Object.<init>
                return_void()
                ]))),
    Method(Name('Lcom/example/hello/R$string;'),
           Prototype([Void()], Void(), []),
           MemberName('<init>'),
           AccessFlags(ACC_CONSTRUCTOR | ACC_PUBLIC),
           Code(1, 1, 1, [], 0, assemble([
                invoke_direct(Count4(1), Index16(13), Register4(0)), # Object.<init>
                return_void()
                ]))),
    Method(Name('Lcom/example/hello/R;'),
           Prototype([Void()], Void(), []),
           MemberName('<init>'),
           AccessFlags(ACC_CONSTRUCTOR | ACC_PUBLIC),
           Code(1, 1, 1, [], 0, assemble([
                invoke_direct(Count4(1), Index16(13), Register4(0)), # Object.<init>
                return_void()
                ]))),
    Method(Name('Ljava/lang/Object;'),
           Prototype([Void()], Void(), []),
           MemberName('<init>'),
           None,
           None)
    ]

classes = [
    ClassDef(Name('Landroid/annotation/SuppressLint;'),
             AccessFlags(ACC_ANNOTATION | ACC_ABSTRACT | ACC_INTERFACE | ACC_PUBLIC),
             Name('Ljava/lang/Object;'),
             [Name('Ljava/lang/annotation/Annotation;')],
             'SuppressLint.java',
             Annotations([
                (Visibility(VISIBILITY_RUNTIME),
                 Annotation(Name('Ljava/lang/annotation/Retention;'), [
                    ('value', fields[10]) # RetentionPolicy.CLASS
                    ])),
                (Visibility(VISIBILITY_RUNTIME),
                 Annotation(Name('Ljava/lang/annotation/Target;'), [
                    ('value', [
                        fields[9], # ElementType.TYPE
                        fields[5], # ElementType.FIELD
                        fields[7], # ElementType.METHOD
                        fields[8], # ElementType.PARAMETER
                        fields[4], # ElementType.CONSTRUCTOR
                        fields[6]  # ElementType.LOCAL_VARIABLE
                        ])
                    ]))
                ],
                [], [], []),
             ClassData([],
                       [],
                       [],
                       [methods[0]]), # SuppressLint.value
             []),
    ClassDef(Name('Landroid/annotation/TargetApi;'),
             AccessFlags(ACC_ANNOTATION | ACC_ABSTRACT | ACC_INTERFACE | ACC_PUBLIC),
             Name('Ljava/lang/Object;'),
             [Name('Ljava/lang/annotation/Annotation;')],
             'TargetApi.java',
             Annotations([
                (Visibility(VISIBILITY_RUNTIME),
                 Annotation(Name('Ljava/lang/annotation/Retention;'), [
                    ('value', fields[10]) # RetentionPolicy.CLASS
                    ])),
                (Visibility(VISIBILITY_RUNTIME),
                 Annotation(Name('Ljava/lang/annotation/Target;'), [
                    ('value', [
                        fields[9], # ElementType.TYPE
                        fields[7], # ElementType.METHOD
                        fields[4]  # ElementType.CONSTRUCTOR
                        ])
                    ]))
                ],
                [], [], []),
             ClassData([], [], [],
                       [methods[1]]), # TargetApi.value
             []),
    ClassDef(Name('Lcom/example/hello/BuildConfig;'),
             AccessFlags(ACC_FINAL | ACC_PUBLIC),
             Name('Ljava/lang/Object;'),
             [],
             'BuildConfig.java',
             [],
             ClassData([(fields[0], AccessFlags(ACC_FINAL | ACC_STATIC | ACC_PUBLIC))],
                        # BuildConfig.DEBUG
                       [],
                       [methods[4]], # <init>
                       []),
             [False]), # DEBUG=False (can be omitted)
    ClassDef(Name('Lcom/example/hello/HelloActivity;'),
             AccessFlags(ACC_PUBLIC),
             Name('Landroid/app/Activity;'),
             [],
             'HelloActivity.java',
             [],
             ClassData([], [],
                       [methods[5]],  # <init>
                       [methods[6]]), # onCreate
             []),
    ClassDef(Name('Lcom/example/hello/R$attr;'),
             AccessFlags(ACC_FINAL | ACC_PUBLIC),
             Name('Ljava/lang/Object;'),
             [],
             'R.java',
             Annotations([
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name('Ldalvik/annotation/EnclosingClass;'), [
                    ('value', Type(Name('Lcom/example/hello/R;')))
                    ])),
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name('Ldalvik/annotation/InnerClass;'), [
                    ('accessFlags', Int(ACC_FINAL | ACC_STATIC | ACC_PUBLIC)),
                    ('name', 'attr')
                    ]))
                ],
                [], [], []),
             ClassData([], [],
                       [methods[8]], # R$attr.<init>
                       []),
             []),
    ClassDef(Name('Lcom/example/hello/R$drawable;'),
             AccessFlags(ACC_FINAL | ACC_PUBLIC),
             Name('Ljava/lang/Object;'),
             [],
             'R.java',
             Annotations([
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name('Ldalvik/annotation/EnclosingClass;'), [
                    ('value', Type(Name('Lcom/example/hello/R;')))
                    ])),
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name('Ldalvik/annotation/InnerClass;'), [
                    ('accessFlags', Int(ACC_FINAL | ACC_STATIC | ACC_PUBLIC)),
                    ('name', 'drawable')
                    ]))
                ],
                [], [], []),
             ClassData([(fields[1], AccessFlags(ACC_FINAL | ACC_STATIC | ACC_PUBLIC))],
                        # R$drawable.ic_launcher
                       [],
                       [methods[9]], # R$drawable.<init>
                       []),
             [Int(0x7f020000)]),
             # package=0x7f, type=2, resource=0, value=0 (@drawable/ic_launcher)
    ClassDef(Name('Lcom/example/hello/R$layout;'),
             AccessFlags(ACC_FINAL | ACC_PUBLIC),
             Name('Ljava/lang/Object;'),
             [],
             'R.java',
             Annotations([
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name('Ldalvik/annotation/EnclosingClass;'), [
                    ('value', Type(Name('Lcom/example/hello/R;')))
                    ])),
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name('Ldalvik/annotation/InnerClass;'), [
                    ('accessFlags', Int(ACC_FINAL | ACC_STATIC | ACC_PUBLIC)),
                    ('name', 'layout')
                    ]))
                ],
                [], [], []),
             ClassData([(fields[2], AccessFlags(ACC_FINAL | ACC_STATIC | ACC_PUBLIC))],
                        # R$layout.main
                       [],
                       [methods[10]], # R$layout.<init>
                       []),
             [Int(0x7f030000)]),
             # package=0x7f, type=3, resource=0, value=0 (@layout/main)
             
    ClassDef(Name('Lcom/example/hello/R$string;'),
             AccessFlags(ACC_FINAL | ACC_PUBLIC),
             Name('Ljava/lang/Object;'),
             [],
             'R.java',
             Annotations([
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name('Ldalvik/annotation/EnclosingClass;'), [
                    ('value', Type(Name('Lcom/example/hello/R;')))
                    ])),
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name('Ldalvik/annotation/InnerClass;'), [
                    ('accessFlags', Int(ACC_FINAL | ACC_STATIC | ACC_PUBLIC)),
                    ('name', 'string')
                    ]))
                ],
                [], [], []),
             ClassData([(fields[3], AccessFlags(ACC_FINAL | ACC_STATIC | ACC_PUBLIC))],
                        # R$string.app_name
                       [],
                       [methods[11]], # R$string.<init>
                       []), [Int(0x7f040000)]),
                       # package=0x7f, type=4, resource=0, value=0 (@string/app_name)
    ClassDef(Name('Lcom/example/hello/R;'),
             AccessFlags(ACC_FINAL | ACC_PUBLIC),
             Name('Ljava/lang/Object;'),
             [],
             'R.java',
             Annotations([
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name('Ldalvik/annotation/MemberClasses;'), [
                    ('value', [
                        Type(Name('Lcom/example/hello/R$string;')),
                        Type(Name('Lcom/example/hello/R$layout;')),
                        Type(Name('Lcom/example/hello/R$drawable;')),
                        Type(Name('Lcom/example/hello/R$attr;'))
                        ])
                    ]))
                ],
                [], [], []),
             ClassData([], [],
                       [methods[12]], # R.<init>
                       []),
             [])
    ]

def build(output_dir):

    d = Dex()
    d.create(strings, types, prototypes, fields, methods, classes)
    d.save(os.path.join(output_dir, 'classes.dex'))
