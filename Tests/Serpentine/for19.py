__package__ = "com.example.for19"

from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        l = [0, 1, 2, 3, 4, 5]
        m = [0.0, 0.25, 0.5, 0.75, 1.0]
        
        for i in l:
            s += str(i) + " "
        
        for i in m:
            s += str(i) + " "
        
        expected = "0 1 2 3 4 5 0.0 0.25 0.5 0.75 1.0 "
        self.showResult(expected, s)
