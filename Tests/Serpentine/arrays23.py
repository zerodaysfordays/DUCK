__package__ = "com.example.arrays23"

from java.util import String
from serpentine.arrays import Arrays
from Common.helper import TestActivity

class ArraysActivity(TestActivity):

    __fields__ = {
        "a": [[int]]
        }
    
    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = array(int, 3, 3)
        
        for i in range(3):
            for j in range(3):
                a[i][j] = i + j
        
        expected = "0 1 2 \n1 2 3 \n2 3 4 \n"
        s = ""
        
        for i in range(3):
            for j in range(3):
                s += str(a[i][j]) + " "
            s += "\n"
        
        self.showResult(expected, s, True)
