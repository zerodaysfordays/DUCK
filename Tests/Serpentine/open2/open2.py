__package__ = "com.example.open2"

from java.lang import String
from android.app import Activity
from android.widget import TextView

class OpenActivity(Activity):

    def __init__(self):
        Activity.__init__(self)
    
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        path = "/sdcard/Download/open2.temp.txt"
        
        try:
            f = open(path, "rw")
            s = "Opened " + path + " for reading and writing."
        except IOError, e1:
            s = str(e1)
        except ValueError, e2:
            s = str(e2)
        
        view = TextView(self)
        view.setText(s)
        
        self.setContentView(view)
