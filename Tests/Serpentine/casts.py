# Tests generation of bytecode for constants of specific types.
# Use the DUCK_LISTING environment variable when building to create a listing
# for inspection.

__package__ = "com.example.casts"

from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class CastsActivity(Activity):

    __interfaces__ = []
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        
        a = 1.0
        b = float(1.0)
        c = float(a)
        d = 2
        e = int(2)
        f = int(d)
        g = 3
        h = long(3)
        i = long(g)
        j = 65535
        k = short(32767)
        l = short(j)
        m = 255
        n = byte(127)
        o = byte(m)
        p = 128
        q = char(127)
        r = char(p)
        s = 1.0
        t = int(1.0)
        u = int(s)
        
        view.setText(str(a) + " " + str(b) + " " + str(c) + "\n" + \
                     str(d) + " " + str(e) + " " + str(f) + "\n" + \
                     str(g) + " " + str(h) + " " + str(i) + "\n" + \
                     str(j) + " " + str(k) + " " + str(l) + "\n" + \
                     str(m) + " " + str(n) + " " + str(o) + "\n" + \
                     str(p) + " " + str(q) + " " + str(r) + "\n" + \
                     str(s) + " " + str(t) + " " + str(u))
        self.setContentView(view)
