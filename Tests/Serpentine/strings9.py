__package__ = "com.example.strings9"

from java.lang import String

from Common.helper import TestActivity

class StringsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = "Hello world!"
        b = a[-2] + a[-8] + a[-12] + a[-1]
        
        # Test the bounds of negative indices.
        try:
            b += a[-13]
        except IndexError:
            b += " IndexError"
        
        expected = "doH! IndexError"
        
        self.showResult(expected, b, True)
