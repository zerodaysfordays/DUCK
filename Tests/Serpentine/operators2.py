__package__ = "com.example.operators2"

from java.lang import Integer, String
from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class OperatorsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        # Test that different primitive types can be combined using binary
        # operators.
        s = ""
        
        s += str(1 + float(2)) + "\n"           # int/float
        s += str(1 + 2.0) + "\n"                # int/double
        s += str(1 + long(2)) + "\n"            # int/long
        s += str(float(1) + 2) + "\n"           # float/int
        s += str(float(1) + long(2)) + "\n"     # float/long
        s += str(float(1) + 2.0) + "\n"         # float/double
        s += str(long(1) + 2) + "\n"            # long/int
        s += str(long(1) + float(2)) + "\n"     # long/float
        s += str(long(1) + 2.0) + "\n"          # long/double
        s += str(double(1) + 2) + "\n"          # double/int
        s += str(double(1) + float(2)) + "\n"   # double/float
        s += str(double(1) + long(2)) + "\n"    # double/float
        
        view = TextView(self)
        if s == "3.0\n3.0\n3\n3.0\n3\n3.0\n3\n3\n3.0\n3.0\n3.0\n3.0\n":
            view.setText("Test passed!")
        else:
            view.setText("Test failed:\n" + s)
        self.setContentView(view)
