__package__ = "com.example.zip5"

from Common.helper import TestActivity

class ZipActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l1 = array([1, 2, 3])
        l2 = array(["Aa", "Bb", "Cc"])
        z = zip(l1, l2)
        
        p = z[0]
        q = p.first()
        r = p.second()
        s = str(q) + " " + r
        
        expected = "1 Aa"
        self.showResult(expected, s, True)
