# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.media"

from java.lang import CharSequence, Object, String
from java.net import URI
from java.nio import ByteBuffer
from java.util import Map
from android.content import Context
from android.graphics import Rect
from android.hardware import Camera
from android.os import Handler, Parcelable
from android.view import KeyEvent, SurfaceHolder

class AsyncPlayer(Object):

    @args(void, [String])
    def __init__(self, tag):
        pass
    
    @args(void, [Context, Uri, bool, int])
    def play(self, context, uri, looping, stream):
        pass
    
    @args(void, [Context, Uri, bool, int, AudioAttributes])
    def play(self, context, uri, looping, stream, attributes):
        pass
    
    def stop(self):
        pass


class AudioAttributes(Object):

    __interfaces__ = [Parcelable]
    __fields__ = {
        "CREATOR": Parcelable.Creator(AudioAttributes)
        }
    
    CONTENT_TYPE_MOVIE = 3
    CONTENT_TYPE_MUSIC = 2
    CONTENT_TYPE_SONIFICATION = 4
    CONTENT_TYPE_SPEECH = 1
    CONTENT_TYPE_UNKNOWN = 0
    FLAG_AUDIBILITY_ENFORCED = 1
    FLAG_HW_AV_SYNC = 16
    USAGE_ALARM = 4
    USAGE_ASSISTANCE_ACCESSIBILITY = 11
    USAGE_ASSISTANCE_NAVIGATION_GUIDANCE = 12
    USAGE_ASSISTANCE_SONIFICATION = 13
    USAGE_GAME = 14
    USAGE_MEDIA = 1
    USAGE_NOTIFICATION = 5
    USAGE_NOTIFICATION_COMMUNICATION_DELAYED = 9
    USAGE_NOTIFICATION_COMMUNICATION_INSTANT = 8
    USAGE_NOTIFICATION_COMMUNICATION_REQUEST = 7
    USAGE_NOTIFICATION_EVENT = 10
    USAGE_NOTIFICATION_RINGTONE = 6
    USAGE_UNKNOWN = 0
    USAGE_VOICE_COMMUNICATION = 2
    USAGE_VOICE_COMMUNICATION_SIGNALLING = 3
    
    class Builder(Object):
    
        def __init__(self):
            pass
        
        @args(void, [AudioAttributes])
        def __init__(self, attributes):
            pass
        
        @args(AudioAttributes, [])
        def build(self):
            pass
        
        @args(AudioAttributes.Builder, [int])
        def setContentType(self, contentType):
            pass
        
        @args(AudioAttributes.Builder, [int])
        def setFlags(self, flags):
            pass
        
        @args(AudioAttributes.Builder, [int])
        def setLegacyStreamType(self, streamType):
            pass
        
        @args(AudioAttributes.Builder, [int])
        def setUsage(self, usage):
            pass
    
    @args(int, [])
    def getContentType(self):
        pass
    
    @args(int, [])
    def getFlags(self):
        pass
    
    @args(int, [])
    def getUsage(self):
        pass


### API level 23
class AudioDeviceInfo(Object):

    TYPE_AUX_LINE = 19
    TYPE_BLUETOOTH_A2DP = 8
    TYPE_BLUETOOTH_SCO = 7
    TYPE_BUILTIN_EARPIECE = 1
    TYPE_BUILTIN_MIC = 15
    TYPE_BUILTIN_SPEAKER = 2
    TYPE_DOCK = 13
    TYPE_FM = 14
    TYPE_FM_TUNER = 16
    TYPE_HDMI = 9
    TYPE_HDMI_ARC = 10
    TYPE_IP = 20
    TYPE_LINE_ANALOG = 5
    TYPE_LINE_DIGITAL = 6
    TYPE_TELEPHONY = 18
    TYPE_TV_TUNER = 17
    TYPE_UNKNOWN = 0
    TYPE_USB_ACCESSORY = 12
    TYPE_USB_DEVICE = 11
    TYPE_WIRED_HEADPHONES = 4
    TYPE_WIRED_HEADSET = 3
    
    @args([int], [])
    def getChannelCounts(self):
        pass
    
    @args([int], [])
    def getChannelIndexMasks(self):
        pass
    
    @args([int], [])
    def getChannelMasks(self):
        pass
    
    @args([int], [])
    def getEncodings(self):
        pass
    
    @args(int, [])
    def getId(self):
        pass
    
    @args(CharSequence, [])
    def getProductName(self):
        pass
    
    @args([int], [])
    def getSampleRates(self):
        pass
    
    @args(int, [])
    def getType(self):
        pass
    
    @args(bool, [])
    def isSink(self):
        pass
    
    @args(bool, [])
    def isSource(self):
        pass


class AudioFormat(Object):

    # Deprecated in API level 5
    CHANNEL_CONFIGURATION_DEFAULT = 5
    CHANNEL_CONFIGURATION_INVALID = 0
    CHANNEL_CONFIGURATION_MONO = 2
    CHANNEL_CONFIGURATION_STEREO = 3
    
    CHANNEL_INVALID = 0
    CHANNEL_IN_BACK = 32
    CHANNEL_IN_BACK_PROCESSED = 512
    CHANNEL_IN_DEFAULT = 1
    CHANNEL_IN_FRONT = 16
    CHANNEL_IN_FRONT_PROCESSED = 256
    CHANNEL_IN_LEFT = 4
    CHANNEL_IN_LEFT_PROCESSED = 64
    CHANNEL_IN_MONO = 16
    CHANNEL_IN_PRESSURE = 1024
    CHANNEL_IN_RIGHT = 8
    CHANNEL_IN_RIGHT_PROCESSED = 128
    CHANNEL_IN_STEREO = 12
    CHANNEL_IN_VOICE_DNLINK = 32768
    CHANNEL_IN_VOICE_UPLINK = 16384
    CHANNEL_IN_X_AXIS = 2048
    CHANNEL_IN_Y_AXIS = 4096
    CHANNEL_IN_Z_AXIS = 8192
    CHANNEL_OUT_5POINT1 = 252
    CHANNEL_OUT_7POINT1 = 1020
    CHANNEL_OUT_7POINT1_SURROUND = 6396
    CHANNEL_OUT_BACK_CENTER = 1024
    CHANNEL_OUT_BACK_LEFT = 64
    CHANNEL_OUT_BACK_RIGHT = 128
    CHANNEL_OUT_DEFAULT = 1
    CHANNEL_OUT_FRONT_CENTER = 16
    CHANNEL_OUT_FRONT_LEFT = 4
    CHANNEL_OUT_FRONT_LEFT_OF_CENTER = 256
    CHANNEL_OUT_FRONT_RIGHT = 8
    CHANNEL_OUT_FRONT_RIGHT_OF_CENTER = 512
    CHANNEL_OUT_FREQUENCY = 32
    CHANNEL_OUT_MONO = 4
    CHANNEL_OUT_QUAD = 204
    CHANNEL_OUT_SIDE_LEFT = 2048
    CHANNEL_OUT_SIDE_RIGHT = 4096
    CHANNEL_OUT_STEREO = 12
    CHANNEL_OUT_SURROUND = 1052
    ENCODING_AC3 = 5
    ENCODING_DEFAULT = 1
    ENCODING_DTS = 7
    ENCODING_DTS_HD = 8
    ENCODING_E_AC3 = 6
    ENCODING_INVALID = 0
    ENCODING_PCM_16BIT = 2
    ENCODING_PCM_8BIT = 3
    ENCODING_PCM_FLOAT = 4
    
    class Builder(Object):
    
        def __init__(self):
            pass
        
        @args(void, [AudioFormat])
        def __init__(self, format):
            pass
        
        @args(AudioFormat, [])
        def build(self):
            pass
        
        @args(AudioFormat.Builder, [int])
        def setChannelIndexMask(self, channelIndexMask):
            pass
        
        @args(AudioFormat.Builder, [int])
        def setChannelMask(self, channelMask):
            pass
        
        @args(AudioFormat.Builder, [int])
        def setEncoding(self, encoding):
            pass
        
        @args(AudioFormat.Builder, [int])
        def setSampleRate(self, sampleRate):
            pass
    
    @args(int, [])
    def getChannelCount(self):
        pass
    
    @args(int, [])
    def getChannelIndexMask(self):
        pass
    
    @args(int, [])
    def getChannelMask(self):
        pass
    
    @args(int, [])
    def getEncoding(self):
        pass
    
    @args(int, [])
    def getSampleRate(self):
        pass


class AudioManager(Object):

    ACTION_AUDIO_BECOMING_NOISY = "android.media.AUDIO_BECOMING_NOISY"
    ACTION_HDMI_AUDIO_PLUG = "android.media.action.HDMI_AUDIO_PLUG"
    ACTION_HEADSET_PLUG = "android.media.action.HEADSET_PLUG"
    ACTION_SCO_AUDIO_STATE_CHANGED = "android.media.SCO_AUDIO_STATE_CHANGED"
    ACTION_SCO_AUDIO_STATE_UPDATED = "android.media.ACTION_SCO_AUDIO_STATE_UPDATED"
    ADJUST_LOWER = -1
    ADJUST_MUTE = -100
    ADJUST_RAISE = 1
    ADJUST_SAME = 0
    ADJUST_TOGGLE_MUTE = 101
    ADJUST_UNMUTE = 100
    AUDIOFOCUS_GAIN = 1
    AUDIOFOCUS_GAIN_TRANSIENT = 2
    AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE = 4
    AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK = 3
    AUDIOFOCUS_LOSS = -1
    AUDIOFOCUS_LOSS_TRANSIENT = -2
    AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK = -3
    AUDIOFOCUS_REQUEST_FAILED = 0
    AUDIOFOCUS_REQUEST_GRANTED = 1
    AUDIO_SESSION_ID_GENERATE = 0
    ERROR = -1
    ERROR_DEAD_OBJECT = -6
    EXTRA_AUDIO_PLUG_STATE = "android.media.extra.AUDIO_PLUG_STATE"
    EXTRA_ENCODINGS = "android.media.extra.ENCODINGS"
    EXTRA_MAX_CHANNEL_COUNT = "android.media.extra.MAX_CHANNEL_COUNT"
    EXTRA_RINGER_MODE = "android.media.EXTRA_RINGER_MODE"
    EXTRA_SCO_AUDIO_PREVIOUS_STATE = "android.media.extra.SCO_AUDIO_PREVIOUS_STATE"
    EXTRA_SCO_AUDIO_STATE = "android.media.extra.SCO_AUDIO_STATE"
    EXTRA_VIBRATE_SETTING = "android.media.EXTRA_VIBRATE_SETTING"
    EXTRA_VIBRATE_TYPE = "android.media.EXTRA_VIBRATE_TYPE"
    FLAG_ALLOW_RINGER_MODES = 2
    FLAG_PLAY_SOUND = 4
    FLAG_REMOVE_SOUND_AND_VIBRATE = 8
    FLAG_SHOW_UI = 1
    FLAG_VIBRATE = 16
    FX_FOCUS_NAVIGATION_DOWN = 2
    FX_FOCUS_NAVIGATION_LEFT = 3
    FX_FOCUS_NAVIGATION_RIGHT = 4
    FX_FOCUS_NAVIGATION_UP = 1
    FX_KEYPRESS_DELETE = 7
    FX_KEYPRESS_INVALID = 9
    FX_KEYPRESS_RETURN = 8
    FX_KEYPRESS_SPACEBAR = 6
    FX_KEYPRESS_STANDARD = 5
    FX_KEY_CLICK = 0
    GET_DEVICES_ALL = 3
    GET_DEVICES_INPUTS = 1
    GET_DEVICES_OUTPUTS = 2
    MODE_CURRENT = -1
    MODE_INVALID = -2
    MODE_IN_CALL = 2
    MODE_IN_COMMUNICATION = 3
    MODE_NORMAL = 0
    MODE_RINGTONE = 1
    NUM_STREAMS = 5
    PROPERTY_OUTPUT_FRAMES_PER_BUFFER = "android.media.property.OUTPUT_FRAMES_PER_BUFFER"
    PROPERTY_OUTPUT_SAMPLE_RATE = "android.media.property.OUTPUT_SAMPLE_RATE"
    PROPERTY_SUPPORT_MIC_NEAR_ULTRASOUND = "android.media.property.SUPPORT_MIC_NEAR_ULTRASOUND"
    PROPERTY_SUPPORT_SPEAKER_NEAR_ULTRASOUND = "android.media.property.SUPPORT_SPEAKER_NEAR_ULTRASOUND"
    RINGER_MODE_CHANGED_ACTION = "android.media.RINGER_MODE_CHANGED"
    RINGER_MODE_NORMAL = 2
    RINGER_MODE_SILENT = 0
    RINGER_MODE_VIBRATE = 1
    ROUTE_ALL = -1
    ROUTE_BLUETOOTH = 4
    ROUTE_BLUETOOTH_A2DP = 16
    ROUTE_BLUETOOTH_SCO = 4
    ROUTE_EARPIECE = 1
    ROUTE_HEADSET = 8
    ROUTE_SPEAKER = 2
    SCO_AUDIO_STATE_CONNECTED = 1
    SCO_AUDIO_STATE_CONNECTING = 2
    SCO_AUDIO_STATE_DISCONNECTED = 0
    SCO_AUDIO_STATE_ERROR = -1
    STREAM_ALARM = 4
    STREAM_DTMF = 8
    STREAM_MUSIC = 3
    STREAM_NOTIFICATION = 5
    STREAM_RING = 2
    STREAM_SYSTEM = 1
    STREAM_VOICE_CALL = 0
    USE_DEFAULT_STREAM_TYPE = 0x80000000
    VIBRATE_SETTING_CHANGED_ACTION = "android.media.VIBRATE_SETTING_CHANGED"
    VIBRATE_SETTING_OFF = 0
    VIBRATE_SETTING_ON = 1
    VIBRATE_SETTING_ONLY_SILENT = 2
    VIBRATE_TYPE_NOTIFICATION = 1
    VIBRATE_TYPE_RINGER = 0
    
    class OnAudioFocusChangeListener:
    
        @args(void, [int])
        def onAudioFocusChange(self, focusChange):
            pass
    
    @args(int, [AudioManager.OnAudioFocusChangeListener])
    def abandonAudioFocus(self, listener):
        pass
    
    @args(void, [int, int, int])
    def adjustStreamVolume(self, streamType, direction, flags):
        pass
    
    @args(void, [int, int, int])
    def adjustSuggestedStreamVolume(self, direction, suggestedStreamType, flags):
        pass
    
    @args(void, [int, int])
    def adjustVolume(self, direction, flags):
        pass
    
    @args(void, [KeyEvent])
    def dispatchMediaKeyEvent(self, keyEvent):
        pass
    
    ### API level 21
    @args(int, [])
    def generateAudioSessionId(self):
        pass
    
    ### API level 23
    @args([AudioDeviceInfo], [int])
    def getDevices(self, flags):
        pass
    
    @args(int, [])
    def getMode(self):
        pass
    
    @args(String, [String])
    def getParameters(self, keys):
        pass
    
    @args(String, [String])
    def getProperty(self, key):
        pass
    
    @args(int, [])
    def getRingerMode(self):
        pass
    
    @args(int, [int])
    def getStreamMaxVolume(self, streamType):
        pass
    
    @args(int, [int])
    def getStreamVolume(self, streamType):
        pass
    
    @args(int, [int])
    def getVibrateSetting(self, vibrateType):
        pass
    
    @args(bool, [])
    def isBluetoothA2dpOn(self):
        pass
    
    @args(bool, [])
    def isBluetoothScoAvailableOffCall(self):
        pass
    
    @args(bool, [])
    def isBluetoothScoOn(self):
        pass
    
    @args(bool, [])
    def isMicrophoneMute(self):
        pass
    
    @args(bool, [])
    def isMusicActive(self):
        pass
    
    @args(bool, [])
    def isSpeakerphoneOn(self):
        pass
    
    @args(bool, [int])
    def isStreamMute(self, streamType):
        pass
    
    @args(bool, [])
    def isVolumeFixed(self):
        pass
    
    @args(bool, [])
    def isWiredHeadsetOn(self):
        pass
    
    def loadSoundEffects(self):
        pass
    
    @args(void, [int, float])
    def playSoundEffect(self, effectType, volume):
        pass
    
    @args(void, [int])
    def playSoundEffect(self, effectType):
        pass
    
    #@args(void, [AudioDeviceCallback, Handler])
    #def registerAudioDeviceCallback(self, callback, handler):
    #    pass
    
    ### ...
    
    @args(int, [AudioManager.OnAudioFocusChangedListener, int, int])
    def requestAudioFocus(self, listener, streamType, durationHint):
        pass
    
    @args(void, [bool])
    def setBluetoothA2dpOn(self, on):
        pass
    
    @args(void, [bool])
    def setBluetoothScoOn(self, on):
        pass
    
    @args(void, [bool])
    def setMicrophoneMute(self, muted):
        pass
    
    @args(void, [int])
    def setMode(self, mode):
        pass
    
    @args(void, [String])
    def setParameters(self, keyValuePairs):
        pass
    
    @args(void, [int])
    def setRingerMode(self, ringerMode):
        pass
    
    @args(void, [bool])
    def setSpeakerphoneOn(self, on):
        pass
    
    @args(void, [int, bool])
    def setStreamMute(self, streamType, state):
        pass
    
    @args(void, [int, bool])
    def setStreamSolo(self, streamType, state):
        pass
    
    @args(void, [int, int, int])
    def setStreamVolume(self, streamType, index, flags):
        pass
    
    @args(void, [int, int])
    def setVibrateSetting(self, vibrateType, vibrateSetting):
        pass
    
    @args(bool, [int])
    def shouldVibrate(self, vibrateType):
        pass
    
    def startBluetoothSco(self):
        pass
    
    def stopBluetoothSco(self):
        pass
    
    def unloadSoundEffects(self):
        pass
    
    ### ...


class AudioRecord(Object):

    ERROR = -1
    ERROR_BAD_VALUE = -2
    ERROR_INVALID_OPERATION = -3
    READ_BLOCKING = 0
    READ_NON_BLOCKING = 1
    RECORDSTATE_RECORDING = 3
    RECORDSTATE_STOPPED = 1
    STATE_INITIALIZED = 1
    STATE_UNINITIALIZED = 0
    SUCCESS = 0
    
    class Builder(Object):
    
        def __init__(self):
            pass
        
        @args(AudioRecord, [])
        def build(self):
            pass
        
        @args(AudioRecord.Builder, [AudioFormat])
        def setAudioFormat(self, format):
            pass
        
        @args(AudioRecord.Builder, [int])
        def setAudioSource(self, source):
            pass
        
        @args(AudioRecord.Builder, [int])
        def setBufferSizeInBytes(self, bufferSizeInBytes):
            pass
    
    class OnRecordPositionUpdateListener:
    
        @args(void, [AudioRecord])
        def onMarkerReached(self, recorder):
            pass
        
        @args(void, [AudioRecord])
        def onPeriodicNotification(self, recorder):
            pass
    
    class OnRoutingChangedListener:
    
        @args(void, [AudioRecord])
        def onRoutingChanged(self, recorder):
            pass
    
    @args(void, [int, int, int, int, int])
    def __init__(self, audioSource, sampleRateInHz, channelConfig, audioFormat,
                       bufferSizeInBytes):
        pass
    
    @args(void, [AudioRecord.OnRoutingChangedListener, Handler])
    def addOnRoutingChangedListener(self, listener, handler):
        pass
    
    @args(int, [])
    def getAudioFormat(self):
        pass
    
    @args(int, [])
    def getAudioSessionId(self):
        pass
    
    @args(int, [])
    def getAudioSource(self):
        pass
    
    @args(int, [])
    def getBufferSizeInFrames(self):
        pass
    
    @args(int, [])
    def getChannelConfiguration(self):
        pass
    
    @args(int, [])
    def getChannelCount(self):
        pass
    
    @args(AudioFormat, [])
    def getFormat(self):
        pass
    
    @static
    @args(int, [int, int, int])
    def getMinBufferSize(sampleRateInHz, channelConfig, audioFormat):
        pass
    
    @args(int, [])
    def getNotificationMarkerPosition(self):
        pass
    
    @args(int, [])
    def getPositionNotificationPeriod(self):
        pass
    
    @args(AudioDeviceInfo, [])
    def getPreferredDevice(self):
        pass
    
    @args(int, [])
    def getRecordingState(self):
        pass
    
    @args(AudioDeviceInfo, [])
    def getRoutedDevice(self):
        pass
    
    @args(int, [])
    def getSampleRate(self):
        pass
    
    @args(int, [])
    def getState(self):
        pass
    
    @args(int, [[short], int, int])
    def read(self, audioData, offsetInShorts, sizeInShorts):
        pass
    
    @args(int, [[byte], int, int])
    def read(self, audioData, offsetInBytes, sizeInBytes):
        pass
    
    ### API level 23
    @args(int, [[float], int, int, int])
    def read(self, audioData, offsetInFloats, sizeInFloats, readMode):
        pass
    
    ### API level 23
    @args(int, [[short], int, int, int])
    def read(self, audioData, offsetInShorts, sizeInShorts, readMode):
        pass
    
    ### API level 23
    @args(int, [ByteBuffer, int, int])
    def read(self, audioBuffer, sizeInBytes, readMode):
        pass
    
    ### API level 23
    @args(int, [[byte], int, int, int])
    def read(self, audioData, sizeInBytes, sizeInBytes, readMode):
        pass
    
    @args(int, [ByteBuffer, int])
    def read(self, audioBuffer, sizeInBytes):
        pass
    
    def release(self):
        pass
    
    @args(void, [AudioRecord.OnRoutineChangedListener])
    def removeOnRoutingChangedListener(self, listener):
        pass
    
    @args(int, [int])
    def setNotificationMarkerPosition(self, markerInFrames):
        pass
    
    @args(int, [int])
    def setPositionNotificationPeriod(self, periodInFrames):
        pass
    
    @args(bool, [AudioDeviceInfo])
    def setPreferredDevice(self, deviceInfo):
        pass
    
    @args(void, [AudioRecord.OnRecordPositionUpdateListener, Handler])
    def setRecordPositionUpdateListener(self, listener, handler):
        pass
    
    @args(void, [AudioRecord.OnRecordPositionUpdateListener])
    def setRecordPositionUpdateListener(self, listener):
        pass
    
    def startRecording(self):
        pass
    
    @args(void, [MediaSyncEvent])
    def startRecording(self, syncEvent):
        pass
    
    def stop(self):
        pass


class AudioTrack(Object):

    ERROR = -1
    ERROR_BAD_VALUE = -2
    ERROR_INVALID_OPERATION = -3
    MODE_STATIC = 0
    MODE_STREAM = 1
    PLAYSTATE_PAUSED = 2
    PLAYSTATE_PLAYING = 3
    PLAYSTATE_STOPPED = 1
    STATE_INITIALIZED = 1
    STATE_NO_STATIC_DATA = 2
    STATE_UNINITIALIZED = 0
    SUCCESS = 0
    WRITE_BLOCKING = 0
    WRITE_NON_BLOCKING = 1
    
    class Builder(Object):
    
        def __init__(self):
            pass
        
        @args(AudioTrack, [])
        def build(self):
            pass
        
        @args(AudioTrack.Builder, [AudioAttributes])
        def setAudioAttributes(self, attributes):
            pass
        
        @args(AudioTrack.Builder, [AudioFormat])
        def setAudioFormat(self, format):
            pass
        
        @args(AudioTrack.Builder, [int])
        def setBufferSizeInBytes(self, bufferSizeInBytes):
            pass
        
        @args(AudioTrack.Builder, [int])
        def setSessionId(self, sessionId):
            pass
        
        @args(AudioTrack.Builder, [int])
        def setTransferMode(self, mode):
            pass
    
    class OnPlaybackPositionUpdateListener:
    
        @args(void, [AudioTrack])
        def onMarkerReached(self, track):
            pass
        
        @args(void, [AudioTrack])
        def onPeriodicNotification(self, track):
            pass
    
    class OnRoutingChangedListener:
    
        @args(void, [AudioTrack])
        def onRoutingChanged(self, track):
            pass
    
    @args(void, [int, int, int, int, int, int])
    def __init__(self, streamType, sampleRateInHz, channelConfig, audioFormat,
                 bufferSizeInBytes, mode):
        pass
    
    @args(void, [int, int, int, int, int, int])
    def __init__(self, streamType, sampleRateInHz, channelConfig, audioFormat,
                 bufferSizeInBytes, mode, sessionId):
        pass
    
    @args(void, [AudioAttributes, AudioFormat, int, int, int])
    def __init__(self, attributes, format, bufferSizeInBytes, mode, sessionId):
        pass
    
    @args(void, [AudioTrack.OnRoutingChangedListener, Handler])
    def addOnRoutingChangedListener(self, listener, handler):
        pass
    
    @args(int, [int])
    def attachAuxEffect(self, effectId):
        pass
    
    def flush(self):
        pass
    
    @args(int, [])
    def getAudioFormat(self):
        pass
    
    @args(int, [])
    def getAudioSessionId(self):
        pass
    
    @args(int, [])
    def getBufferSizeInFrames(self):
        pass
    
    @args(int, [])
    def getChannelConfiguration(self):
        pass
    
    @args(int, [])
    def getChannelCount(self):
        pass
    
    @args(AudioFormat, [])
    def getFormat(self):
        pass
    
    @static
    @args(float, [])
    def getMaxVolume():
        pass
    
    @protected
    @args(int, [])
    def getNativeFrameCount(self):
        pass

    @static
    @args(int, [int])
    def getNativeOutputSampleRate(streamType):
        pass
    
    @args(int, [])
    def getNotificationMarkerPosition(self):
        pass
    
    @args(int, [])
    def getPlayState(self):
        pass
    
    @args(int, [])
    def getPlaybackHeadPosition(self):
        pass
    
    @args(PlaybackParams, [])
    def getPlaybackParams(self):
        pass
    
    @args(int, [])
    def getPlaybackRate(self):
        pass
    
    @args(int, [])
    def getPositionNotificationPeriod(self):
        pass
    
    @args(AudioDeviceInfo, [])
    def getPreferredDevice(self):
        pass
    
    @args(AudioDeviceInfo, [])
    def getRoutedDevice(self):
        pass
    
    @args(int, [])
    def getSampleRate(self):
        pass
    
    @args(int, [])
    def getState(self):
        pass
    
    @args(int, [])
    def getStreamType(self):
        pass
    
    @args(bool, [AudioTimestamp])
    def getTimestamp(self, timestamp):
        pass
    
    def pause(self):
        pass
    
    def play(self):
        pass
    
    def release(self):
        pass
    
    @args(int, [])
    def reloadStaticData(self):
        pass
    
    @args(void, [AudioTrack.OnRoutingChangedListener])
    def removeOnRoutingChangedListener(self, listener):
        pass
    
    @args(int, [float])
    def setAuxEffectSendLevel(self, level):
        pass
    
    @args(int, [int, int, int])
    def setLoopPoints(self, startInFrames, endInFrames, loopCount):
        pass
    
    @args(int, [int])
    def setNotificationMarkerPosition(self, markerInFrames):
        pass
    
    @args(int, [int])
    def setPlaybackHeadPosition(self, positionInFrames):
        pass
    
    @args(void, [PlaybackParams])
    def setPlaybackParams(self, params):
        pass
    
    @args(void, [AudioTrack.OnPlaybackPositionUpdateListener, Handler])
    def setPlaybackPositionUpdateListener(self, listener, handler):
        pass
    
    @args(void, [AudioTrack.OnPlaybackPositionUpdateListener])
    def setPlaybackPositionUpdateListener(self, listener):
        pass
    
    @args(int, [int])
    def setPlaybackRate(self, sampleRateInHz):
        pass
    
    @args(int, [int])
    def setPositionNotificationPeriod(self, periodInFrames):
        pass
    
    @args(bool, [AudioDeviceInfo])
    def setPreferredDevice(self, deviceInfo):
        pass
    
    @args(int, [float, float])
    def setStereoVolume(self, leftGain, rightGain):
        pass
    
    @args(int, [float])
    def setVolume(self, gain):
        pass
    
    def stop(self):
        pass
    
    @args(int, [ByteBuffer, int, int])
    def write(self, audioData, sizeInBytes, writeMode):
        pass
    
    @args(int, [[short], int, int])
    def write(self, audioData, offsetInShorts, sizeInShorts):
        pass
    
    @args(int, [[byte], int, int])
    def write(self, audioData, offsetInBytes, sizeInBytes):
        pass
    
    @args(int, [[short], int, int, int])
    def write(self, audioData, offsetInShorts, sizeInShorts, writeMode):
        pass
    
    @args(int, [[byte], int, int, int])
    def write(self, audioData, offsetInBytes, sizeInBytes, writeMode):
        pass
    
    @args(int, [[float], int, int, int])
    def write(self, audioData, offsetInFloats, sizeInFloats, writeMode):
        pass
    
    @args(int, [ByteBuffer, int, int, long])
    def write(self, audioData, sizeInBytes, writeMode, timestamp):
        pass


class MediaPlayer(Object):

    MEDIA_ERROR_IO = -1004
    MEDIA_ERROR_MALFORMED = -1007
    MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK = 200
    MEDIA_ERROR_SERVER_DIED = 100
    MEDIA_ERROR_TIMED_OUT = -110
    MEDIA_ERROR_UNKNOWN = 1
    MEDIA_ERROR_UNSUPPORTED = -1010
    MEDIA_INFO_INTERLEAVING = 800
    MEDIA_INFO_BUFFERING_END = 702
    MEDIA_INFO_BUFFERING_START = 701
    MEDIA_INFO_METADATA_UPDATE = 802
    MEDIA_INFO_NOT_SEEKABLE = 801
    MEDIA_INFO_SUBTITLE_TIMED_OUT = 902
    MEDIA_INFO_UNKNOWN = 1
    MEDIA_INFO_UNSUPPORTED_SUBTITLE = 901
    MEDIA_INFO_VIDEO_RENDERING_START = 3
    MEDIA_INFO_VIDEO_TRACK_LAGGING = 700
    MEDIA_MIMETYPE_TEXT_SUBRIP = "application/x-subrip"
    VIDEO_SCALING_MODE_SCALE_TO_FIT = 1
    VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING = 2
    
    class OnBufferingUpdateListener:
    
        @args(void, [MediaPlayer, int])
        def onBufferingUpdate(self, mediaPlayer, percent):
            pass
    
    class OnCompletionListener:
    
        @args(void, [MediaPlayer])
        def onCompletion(self, mediaPlayer):
            pass
    
    class OnErrorListener:
    
        @args(bool, [MediaPlayer, int, int])
        def onError(self, mediaPlayer, percent):
            pass
    
    class OnInfoListener:
    
        @args(bool, [MediaPlayer, int, int])
        def onInfo(self, mediaPlayer, percent):
            pass
    
    class OnPreparedListener:
    
        @args(void, [MediaPlayer])
        def onPrepared(self, mediaPlayer):
            pass
    
    class OnSeekCompleteListener:
    
        @args(void, [MediaPlayer])
        def onSeekComplete(self, mediaPlayer):
            pass
    
    class OnTimedMetaDataAvailableListener:
    
        @args(void, [MediaPlayer, TimedMetaData])
        def onTimedMetaDataAvailable(self, mediaPlayer, data):
            pass
    
    class OnTimedTextListener:
    
        @args(void, [MediaPlayer, TimedText])
        def onTimedText(self, mediaPlayer, text):
            pass
    
    class OnVideoSizeChangedListener:
    
        @args(void, [MediaPlayer, int, int])
        def onVideoSizeChanged(self, mediaPlayer, width, height):
            pass
    
    class TrackInfo:
    
        MEDIA_TRACK_TYPE_AUDIO = 2
        MEDIA_TRACK_TYPE_METADATA = 5
        MEDIA_TRACK_TYPE_SUBTITLE = 4
        MEDIA_TRACK_TYPE_TIMEDTEXT = 3
        MEDIA_TRACK_TYPE_UNKNOWN = 0
        MEDIA_TRACK_TYPE_VIDEO = 1
        
        @args(int, [])
        def describeContents(self):
            pass
        
        @args(MediaFormat, [])
        def getFormat(self):
            pass
        
        @args(String, [])
        def getLanguage(self):
            pass
        
        @args(int, [])
        def getTrackType(self):
            pass
    
    def __init__(self):
        pass
    
    @args(void, [Context, Uri, String])
    def addTimedTextSource(self, context, uri, mimeType):
        pass
    
    @args(void, [String, String])
    def addTimedTextSource(self, path, mimeType):
        pass
    
    @args(void, [FileDescriptor, long, long, String])
    def addTimedTextSource(self, fd, offset, length, mimeType):
        pass
    
    @args(void, [FileDescriptor, String])
    def addTimedTextSource(self, fd, mimeType):
        pass
    
    @args(void, [int])
    def attachAuxEffect(self, effectId):
        pass
    
    ### ...
    
    @static
    @args(MediaPlayer, [Context, Uri, SurfaceHolder])
    def create(context, uri, holder):
        pass
    
    ### ...
    
    @static
    @args(MediaPlayer, [Context, int])
    def create(context, resId):
        pass
    
    @static
    @args(MediaPlayer, [Context, Uri])
    def create(context, uri):
        pass
    
    @args(void, [int])
    def deselectTrack(self, index):
        pass
    
    @args(int, [])
    def getAudioSessionId(self):
        pass
    
    @args(int, [])
    def getCurrentPosition(self):
        pass
    
    @args(int, [])
    def getDuration(self):
        pass
    
    #@args(PlaybackParams, [])
    #def getPlaybackParams(self):
    #    pass
    
    @args(int, [int])
    def getSelectedTrack(self, trackType):
        pass
    
    ### ...
    
    @args(int, [])
    def getVideoHeight(self):
        pass
    
    @args(int, [])
    def getVideoWidth(self):
        pass
    
    @args(bool, [])
    def isLooping(self):
        pass
    
    @args(bool, [])
    def isPlaying(self):
        pass
    
    def pause(self):
        pass
    
    def prepare(self):
        pass
    
    def prepareAsync(self):
        pass
    
    def release(self):
        pass
    
    def reset(self):
        pass
    
    @args(void, [int])
    def seekTo(self, msec):
        pass
    
    @args(void, [int])
    def selectTrack(self, msec):
        pass
    
    ### ...
    
    @args(void, [int])
    def setAudioSessionId(self, sessionId):
        pass
    
    @args(void, [int])
    def setAudioStreamType(self, streamType):
        pass
    
    @args(void, [float])
    def setAuxEffectSendLevel(self, level):
        pass
    
    @args(void, [String])
    def setDataSource(self, path):
        pass
    
    @args(void, [Context, Uri, Map(String, String)])
    def setDataSource(self, context, uri, headers):
        pass
    
    ### ...
    
    @args(void, [Context, Uri])
    def setDataSource(self, context, uri):
        pass
    
    ### ...
    
    @args(void, [SurfaceHolder])
    def setDisplay(self, surfaceHolder):
        pass
    
    @args(void, [bool])
    def setLooping(self, looping):
        pass
    
    @args(void, [MediaPlayer])
    def setNextMediaPlayer(self, next):
        pass
    
    @args(void, [MediaPlayer.OnBufferingUpdateListener])
    def setOnBufferingUpdateListener(self, listener):
        pass
    
    @args(void, [MediaPlayer.OnCompletionListener])
    def setOnCompletionListener(self, listener):
        pass
    
    @args(void, [MediaPlayer.OnErrorListener])
    def setOnErrorListener(self, listener):
        pass
    
    @args(void, [MediaPlayer.OnInfoListener])
    def setOnInfoListener(self, listener):
        pass
    
    @args(void, [MediaPlayer.OnPreparedListener])
    def setOnPreparedListener(self, listener):
        pass
    
    @args(void, [MediaPlayer.OnSeekCompleteListener])
    def setOnSeekCompleteListener(self, listener):
        pass
    
    @args(void, [MediaPlayer.OnTimedMetaDataAvailableListener])
    def setOnTimedMetaDataAvailableListener(self, listener):
        pass
    
    @args(void, [MediaPlayer.OnTimedTextListener])
    def setOnTimedTextListener(self, listener):
        pass
    
    @args(void, [MediaPlayer.OnVideoSizeChangedListener])
    def setOnVideoSizeChangedListener(self, listener):
        pass
    
    #@args(void, [PlaybackParams])
    #def setPlaybackParams(self, params):
    #    pass
    
    @args(void, [bool])
    def setScreenOnWhilePlaying(self, screenOn):
        pass
    
    #@args(void, [Surface])
    #def setSurface(self, surface):
    #    pass
    
    ### ...
    
    @args(void, [int])
    def setVideoScalingMode(self, mode):
        pass
    
    @args(void, [float, float])
    def setVolume(self, leftVolume, rightVolume):
        pass
    
    @args(void, [Context, int])
    def setWakeMode(self, context, mode):
        pass
    
    def start(self):
        pass
    
    def stop(self):
        pass


class MediaRecorder(Object):

    MEDIA_ERROR_SERVER_DIED = 100
    MEDIA_RECORDER_ERROR_UNKNOWN = 1
    MEDIA_RECORDER_INFO_MAX_DURATION_REACHED = 800
    MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED = 801
    MEDIA_RECORDER_INFO_UNKNOWN = 1
    
    class AudioEncoder(Object):
    
        AAC = 3
        AAC_ELD = 5
        AMR_NB = 1
        AMR_WB = 2
        DEFAULT = 0
        HE_AAC = 4
        VORBIS = 6
    
    class AudioSource(Object):
    
        CAMCORDER = 5
        DEFAULT = 0
        MIC = 1
        REMOTE_SUBMIX = 8
        VOICE_CALL = 4
        VOICE_COMMUNICATION = 7
        VOICE_DOWNLINK = 3
        VOICE_RECOGNITION = 6
        VOICE_UPLINK = 2
    
    class OnErrorListener:
    
        @args(void, [MediaRecorder, int, int])
        def onError(self, mr, what, extra):
            pass
    
    class OnInfoListener:
    
        @args(void, [MediaRecorder, int, int])
        def onInfo(self, mr, what, extra):
            pass
    
    class OutputFormat(Object):
    
        AAC_ADTS = 6
        AMR_NB = 3
        AMR_WB = 4
        DEFAULT = 0
        MPEG_4 = 2
        RAW_AMR = 3
        THREE_GPP = 1
        WEBM = 9
    
    class VideoEncoder(Object):
    
        DEFAULT = 0
        H263 = 1
        H264 = 2
        MPEG_4_SP = 3
        VP8 = 4
    
    class VideoSource(Object):
    
        CAMERA = 1
        DEFAULT = 0
        SURFACE = 2
    
    def __init__(self):
        pass
    
    @final
    @static
    @args(int, [])
    def getAudioSourceMax():
        pass
    
    @args(int, [])
    def getMaxAmplitude(self):
        pass
    
    @args(Surface, [])
    def getSurface(self):
        pass
    
    def prepare(self):
        pass
    
    def release(self):
        pass
    
    def reset(self):
        pass
    
    @args(void, [int])
    def setAudioChannels(self, numChannels):
        pass
    
    @args(void, [int])
    def setAudioEncoder(self, audioEncoder):
        pass
    
    @args(void, [int])
    def setAudioEncodingBitRate(self, bitRate):
        pass
    
    @args(void, [int])
    def setAudioSamplingRate(self, smaplingRate):
        pass
    
    @args(void, [int])
    def setAudioSource(self, audioSource):
        pass
    
    ### Deprecated in API level 21
    @args(void, [Camera])
    def setCamera(self, camera):
        pass
    
    @args(void, [double])
    def setCaptureRate(self, framesPerSecond):
        pass
    
    ### ...


class PlaybackParams(Object):

    __interfaces__ = [Parcelable]
    __fields__ = {
        "CREATOR": Parcelable.Creator(PlaybackParams)
        }
    
    AUDIO_FALLBACK_MODE_DEFAULT = 0
    AUDIO_FALLBACK_MODE_FAIL = 2
    AUDIO_FALLBACK_MODE_MUTE = 1
    
    def __init__(self):
        pass
    
    @args(PlaybackParams, [])
    def allowDefaults(self):
        pass
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @args(int, [])
    def getAudioFallbackMode(self):
        pass
    
    @args(float, [])
    def getPitch(self):
        pass
    
    @args(float, [])
    def getSpeed(self):
        pass
    
    @args(PlaybackParams, [int])
    def setAudioFallbackMode(self, audioFallbackMode):
        pass
    
    @args(PlaybackParams, [float])
    def setPitch(self, pitch):
        pass
    
    @args(PlaybackParams, [float])
    def setSpeed(self, speed):
        pass


class TimedMetaData(Object):

    @args([byte], [])
    def getMetaData(self):
        pass
    
    @args(long, [])
    def getTimestamp(self):
        pass


class TimedText(Object):

    @args(Rect, [])
    def getBounds(self):
        pass
    
    @args(String, [])
    def getText(self):
        pass
