__package__ = "com.example.ranges1"

from Common.helper import TestActivity
from serpentine.collections import Collections

class RangesActivity(TestActivity):

    def __init__(self):
    
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        
        for i in range(5):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(5.0):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(float(5.0)):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(long(5)):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(-5, 5):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(-5.0, 5.0):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(long(-5), long(5)):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(float(-5.0), float(5.0)):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(-5, 5, 2):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(-5.0, 5.0, 2.0):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(long(-5), long(5), long(2)):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(float(-5), float(5), float(2)):
            s += str(i) + " "
        
        expected = (
            "0 1 2 3 4 \n"
            "0.0 1.0 2.0 3.0 4.0 \n"
            "0.0 1.0 2.0 3.0 4.0 \n"
            "0 1 2 3 4 \n"
            "-5 -4 -3 -2 -1 0 1 2 3 4 \n"
            "-5.0 -4.0 -3.0 -2.0 -1.0 0.0 1.0 2.0 3.0 4.0 \n"
            "-5 -4 -3 -2 -1 0 1 2 3 4 \n"
            "-5.0 -4.0 -3.0 -2.0 -1.0 0.0 1.0 2.0 3.0 4.0 \n"
            "-5 -3 -1 1 3 \n"
            "-5.0 -3.0 -1.0 1.0 3.0 \n"
            "-5 -3 -1 1 3 \n"
            "-5.0 -3.0 -1.0 1.0 3.0 "
            )
        
        self.showResult(expected, s, True)
