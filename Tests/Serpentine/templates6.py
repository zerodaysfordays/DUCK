__package__ = "com.example.templates6"

from java.util import List, Object, String
from android.os import Bundle
from Common.helper import TestActivity

class TemplatesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l = MyTemplate(["Hello world!"])
        m = MyTemplate([123, 456, 789])
        s = l.get(0)
        s += " " + str(m.get(2))
        
        self.showResult("Hello world! 789", s)


# Define a template class that will be specialised depending on the types of
# the arguments passed to it.

class MyTemplate(Object):

    __replace__ = [P, Q]
    
    @args(void, [P(Q)])
    def __init__(self, value):
    
        Object.__init__(self)
        self.value = value
    
    @args(Q, [int])
    def get(self, index):
    
        return self.value[index]
