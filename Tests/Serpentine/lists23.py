__package__ = "com.example.lists23"

from Common.helper import TestActivity
from serpentine.collections import Collections

class ListsActivity(TestActivity):

    def __init__(self):
    
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        
        for i in range(10):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(-10):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(1, 5):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(5, 1):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(-1, -10, -1):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(-1, -10, 1):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(5, -5, -3):
            s += str(i) + " "
        
        s += "\n"
        
        for i in range(5, -5, 3):
            s += str(i) + " "
        
        s += "\n"
        
        try:
            range(1, 10, 0)
            s += "Zero range\n"
        except ValueError:
            s += "ValueError\n"
        
        expected = (
            "0 1 2 3 4 5 6 7 8 9 \n"
            "\n"
            "1 2 3 4 \n"
            "\n"
            "-1 -2 -3 -4 -5 -6 -7 -8 -9 \n"
            "\n"
            "5 2 -1 -4 \n"
            "\n"
            "ValueError\n"
            )
        
        self.showResult(expected, s, True)
