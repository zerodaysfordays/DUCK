__package__ = "com.example.for22"

from java.util import List
from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        for i in self.getList():
            s += i + " "
        
        expected = "0 1 2 "
        self.showResult(expected, s)
    
    @args(List(str), [])
    def getList(self):
    
        return ["0", "1", "2"]
