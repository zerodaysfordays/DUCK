__package__ = "com.example.loops"

from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class LoopsActivity(Activity):

    __interfaces__ = []
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        s = ""
        i = 0
        while i < 10:
            s = s.concat(" ").concat(s.valueOf(i))
            i = i + 1
        
        view.setText(s)
        self.setContentView(view)
