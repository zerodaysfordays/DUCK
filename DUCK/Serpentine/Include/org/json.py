# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "org.json"

from java.lang import Exception, Object, String
from java.util import Collection, Iterator, Map

class JSONArray(Object):

    def __init__(self):
        pass
    
    @args(void, [Collection])
    def __init__(self, copyFrom):
        pass
    
    @args(void, [JSONTokener])
    def __init__(self, readFrom):
        pass
    
    @args(void, [String])
    def __init__(self, json):
        pass
    
    @args(void, [Object])
    def __init__(self, array):
        pass
    
    @args(bool, [Object])
    def equals(self, object):
        pass
    
    @args(Object, [int])
    def get(self, index):
        pass
    
    @args(bool, [int])
    def getBoolean(self, index):
        pass
    
    @args(double, [int])
    def getDouble(self, index):
        pass
    
    @args(int, [int])
    def getInt(self, index):
        pass
    
    @args(JSONArray, [int])
    def getJSONArray(self, index):
        pass
    
    @args(JSONObject, [int])
    def getJSONObject(self, index):
        pass
    
    @args(long, [int])
    def getLong(self, index):
        pass
    
    @args(String, [int])
    def getString(self, index):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(bool, [int])
    def isNull(self, index):
        pass
    
    @args(String, [String])
    def join(self, separator):
        pass
    
    @args(int, [])
    def length(self):
        pass
    
    @args(Object, [int])
    def opt(self, index):
        pass
    
    @args(bool, [int])
    def optBoolean(self, index):
        pass
    
    @args(bool, [int, bool])
    def optBoolean(self, index, fallback):
        pass
    
    @args(double, [int, double])
    def optDouble(self, index, fallback):
        pass
    
    @args(double, [int])
    def optDouble(self, index):
        pass
    
    @args(int, [int])
    def optInt(self, index):
        pass
    
    @args(int, [int, int])
    def optInt(self, index, fallback):
        pass
    
    @args(JSONArray, [int])
    def optJSONArray(self, index):
        pass
    
    @args(JSONObject, [int])
    def optJSONObject(self, index):
        pass
    
    @args(long, [int, long])
    def optLong(self, index, fallback):
        pass
    
    @args(long, [int])
    def optLong(self, index):
        pass
    
    @args(String, [int])
    def optString(self, index):
        pass
    
    @args(String, [int, String])
    def optString(self, index, fallback):
        pass
    
    @args(JSONArray, [Object])
    def put(self, value):
        pass
    
    @args(JSONArray, [int, int])
    def put(self, index, value):
        pass
    
    @args(JSONArray, [int, bool])
    def put(self, index, value):
        pass
    
    @args(JSONArray, [int, long])
    def put(self, index, value):
        pass
    
    @args(JSONArray, [int, Object])
    def put(self, index, value):
        pass
    
    @args(JSONArray, [bool])
    def put(self, value):
        pass
    
    @args(JSONArray, [int, double])
    def put(self, index, value):
        pass
    
    @args(JSONArray, [int])
    def put(self, value):
        pass
    
    @args(JSONArray, [long])
    def put(self, value):
        pass
    
    @args(JSONArray, [double])
    def put(self, value):
        pass
    
    @args(Object, [int])
    def remove(self, index):
        pass
    
    @args(JSONObject, [JSONArray])
    def toJSONObject(self, names):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @args(String, [int])
    def toString(self, indentSpaces):
        pass


class JSONException(Exception):

    @args(void, [String])
    def __init__(self, s):
        pass


class JSONObject(Object):

    __static_fields__ = {"NULL": Object}
    
    def __init__(self):
        pass
    
    @args(void, [Map])
    def __init__(self, copyFrom):
        pass
    
    @args(void, [JSONTokener])
    def __init__(self, readFrom):
        pass
    
    @args(void, [String])
    def __init__(self, json):
        pass
    
    @args(void, [JSONObject, [String]])
    def __init__(self, copyFrom, names):
        pass
    
    @args(JSONObject, [String, Object])
    def accumulate(self, name, value):
        pass
    
    @args(Object, [String])
    def get(self, name):
        pass
    
    @args(bool, [String])
    def getBoolean(self, name):
        pass
    
    @args(double, [String])
    def getDouble(self, name):
        pass
    
    @args(int, [String])
    def getInt(self, name):
        pass
    
    @args(JSONArray, [String])
    def getJSONArray(self, name):
        pass
    
    @args(JSONObject, [String])
    def getJSONObject(self, name):
        pass
    
    @args(long, [String])
    def getLong(self, name):
        pass
    
    @args(String, [String])
    def getString(self, name):
        pass
    
    @args(bool, [String])
    def has(self, name):
        pass
    
    @args(bool, [String])
    def isNull(self, name):
        pass
    
    @args(Iterator(String), [])
    def keys(self):
        pass
    
    @args(int, [])
    def length(self):
        pass
    
    @args(JSONArray, [])
    def names(self):
        pass
    
    @static
    @args(String, [Number])
    def numberToString(number):
        pass
    
    @args(Object, [String])
    def opt(self, name):
        pass
    
    @args(bool, [String])
    def optBoolean(self, name):
        pass
    
    @args(bool, [String, bool])
    def optBoolean(self, name, fallback):
        pass
    
    @args(double, [String])
    def optDouble(self, name):
        pass
    
    @args(double, [String, double])
    def optDouble(self, name, fallback):
        pass
    
    @args(int, [String])
    def optInt(self, name):
        pass
    
    @args(int, [String, int])
    def optInt(self, name, fallback):
        pass
    
    @args(JSONArray, [String])
    def optJSONArray(self, name):
        pass
    
    @args(JSONObject, [String])
    def optJSONObject(self, name):
        pass
    
    @args(long, [String])
    def optLong(self, name):
        pass
    
    @args(long, [String, long])
    def optLong(self, name, fallback):
        pass
    
    @args(String, [String])
    def optString(self, name):
        pass
    
    @args(String, [String, String])
    def optString(self, name, fallback):
        pass
    
    @args(JSONObject, [String, int])
    def put(self, name, value):
        pass
    
    @args(JSONObject, [String, long])
    def put(self, name, value):
        pass
    
    @args(JSONObject, [String, Object])
    def put(self, name, value):
        pass
    
    @args(JSONObject, [String, bool])
    def put(self, name, value):
        pass
    
    @args(JSONObject, [String, double])
    def put(self, name, value):
        pass
    
    @args(JSONObject, [String, Object])
    def putOpt(self, name, value):
        pass
    
    @static
    @args(String, [String])
    def quote(data):
        pass
    
    @args(Object, [String])
    def remove(self, name):
        pass
    
    @args(JSONArray, [JSONArray])
    def toJSONArray(self, names):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @args(String, [int])
    def toString(self, indentSpaces):
        pass
    
    @static
    @args(Object, [Object])
    def wrap(object):
        pass


class JSONStringer(Object):

    def __init__(self):
        pass
    
    @args(JSONStringer, [])
    def array(self):
        pass
    
    @args(JSONStringer, [])
    def endArray(self):
        pass
    
    @args(JSONStringer, [])
    def endObject(self):
        pass
    
    @args(JSONStringer, [String])
    def key(self, name):
        pass
    
    @args(JSONStringer, [])
    def object(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @args(JSONStringer, [double])
    def value(self, value):
        pass
    
    @args(JSONStringer, [long])
    def value(self, value):
        pass
    
    @args(JSONStringer, [bool])
    def value(self, value):
        pass
    
    @args(JSONStringer, [Object])
    def value(self, value):
        pass


class JSONTokener(Object):

    @args(void, [String])
    def __init__(self, input):
        pass
    
    def back(self):
        pass
    
    @static
    @args(int, [char])
    def dehexchar(hex):
        pass
    
    @args(bool, [])
    def more(self):
        pass
    
    @args(String, [int])
    def next(self, length):
        pass
    
    @args(char, [])
    def next(self):
        pass
    
    @args(char, [char])
    def next(self, c):
        pass
    
    @args(char, [])
    def nextClean(self):
        pass
    
    @args(String, [char])
    def nextString(self, quote):
        pass
    
    @args(String, [char])
    def nextTo(self, excluded):
        pass
    
    @args(String, [String])
    def nextTo(self, excluded):
        pass
    
    @args(Object, [])
    def nextValue(self):
        pass
    
    @args(void, [String])
    def skipPast(self, thru):
        pass
    
    @args(char, [char])
    def skipTo(self, to):
        pass
    
    @args(JSONException, [String])
    def syntaxError(self, message):
        pass
    
    @args(String, [])
    def toString(self):
        pass
