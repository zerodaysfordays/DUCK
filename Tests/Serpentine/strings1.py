__package__ = "com.example.strings1"

import android.os

from Common.helper import TestActivity

class StringsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = str("Hello world!")
        b = str(123)
        c = str(123.456)
        d = str(long(123456))
        e = str(float(12.345))
        f = str(self)
        g = str(True)
        h = str(False)
        
        f = f.substring(0, f.indexOf("@"))
        
        self.showResult("Hello world!\n123\n123.456\n123456\n12.345\n"
                        "com.example.strings1.StringsActivity\ntrue\nfalse",
                        a + "\n" + b + "\n" + c + "\n" + d + "\n" + e + "\n" + \
                        f + "\n" + g + "\n" + h)
