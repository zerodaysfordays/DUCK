__package__ = "com.example.while11"

from Common.helper import TestActivity

class WhileActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        i = 0
        while True:
            x = i
            break
        
        s = str(x)
        
        expected = "0"
        
        self.showResult(expected, s)
