__package__ = "com.example.augmented1"

from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class AugmentedActivity(Activity):

    __interfaces__ = []
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        i = 0
        while i < 12:
            i += 1
        
        if i == 12:
            view.setText("Test passed!")
        else:
            view.setText("Test failed!")
        
        self.setContentView(view)
