__package__ = "com.example.imports9"

from Common.helper import TestActivity
from java.lang import String

# This test should fail because Math should not be imported as a side effect of
# importing the String class.

class ImportsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # If the example compiles then the test passed.
        m = Math.min(1, 2)
        self.showResult("", "")
