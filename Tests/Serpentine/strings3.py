__package__ = "com.example.strings3"

from java.lang import String
import android.os

from Common.helper import TestActivity

class StringsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = String.valueOf("Hello world!")
        b = String.valueOf(123)
        c = String.valueOf(123.456)
        d = String.valueOf(long(123456))
        e = String.valueOf(float(12.345))
        f = String.valueOf(self)
        g = String.valueOf(True)
        h = String.valueOf(False)
        
        f = f.substring(0, f.indexOf("@"))
        
        self.showResult("Hello world!\n123\n123.456\n123456\n12.345\n"
                        "com.example.strings3.StringsActivity\ntrue\nfalse",
                        a + "\n" + b + "\n" + c + "\n" + d + "\n" + e + "\n" + \
                        f + "\n" + g + "\n" + h)
