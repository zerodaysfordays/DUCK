# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.graphics.drawable.shapes"

from java.lang import Object
from android.graphics import Canvas, Paint
from android.os import Cloneable

class Shape(Object):

    __interfaces__ = [Cloneable]
    
    def __init__(self):
        pass
    
    @args(Shape, [])
    def clone(self):
        pass
    
    @abstract
    @args(void, [Canvas, Paint])
    def draw(self, canvas, paint):
        pass
    
    @final
    @args(float, [])
    def getHeight(self):
        pass
    
    @final
    @args(float, [])
    def getWidth(self):
        pass
    
    @args(bool, [])
    def hasAlpha(self):
        pass
    
    @final
    @args(void, [float, float])
    def resize(self, width, height):
        pass
    
    @protected
    @args(void, [float, float])
    def onResize(self, width, height):
        pass
