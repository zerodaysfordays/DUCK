__package__ = "serpentine.activities"

import android.app
import android.R
from android.bluetooth import BluetoothAdapter, BluetoothManager
from android.content import Context
from android.media import AudioManager
from android.os import Build

class Activity(android.app.Activity):

    def __init__(self):
    
        android.app.Activity.__init__(self)
    
    def onCreate(self, bundle):
    
        android.app.Activity.onCreate(self, bundle)
        self.setTheme(android.R.style.Theme_DeviceDefault)
    
    @args(AudioManager, [])
    def getAudioService(self):
    
        return CAST(self.getSystemService(Context.AUDIO_SERVICE), AudioManager)
    
    @args(BluetoothAdapter, [])
    def getBluetoothAdapter(self):
    
        if Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR1:
            return BluetoothAdapter.getDefaultAdapter()
        
        return CAST(self.getSystemService(Context.BLUETOOTH_SERVICE),
                    BluetoothManager).getAdapter()
