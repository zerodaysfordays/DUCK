__package__ = "com.example.arrays1"

from java.nio import ByteBuffer
from android.os import Bundle

from Common.helper import TestActivity

class ArraysActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # Create an array of bytes.
        b = ByteBuffer.allocate(3)
        b.put(byte(1))
        b.put(byte(2))
        b.put(byte(3))
        a = b.array()
        self.showOutput(a)
    
    @args(void, [[byte]])
    def showOutput(self, a):
    
        l = len(a)
        text = str(l) + " items: "
        i = 0
        while i < l:
            text += str(a[i])
            i += 1
            if i != l:
                text += ","
        
        self.showResult("3 items: 1,2,3", text)
