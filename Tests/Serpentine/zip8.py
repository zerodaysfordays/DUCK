__package__ = "com.example.zip8"

from java.lang import String
from java.util import List
from Common.helper import TestActivity

class ZipActivity(TestActivity):

    __fields__ = {
        "numbers": List(int),
        "strings": List(String)
        }
    
    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        self.numbers = [1, 2, 3]
        self.strings = ["Aa", "Bb", "Cc"]
        
        s = self.getString()
        
        expected = "1 Aa\n2 Bb\n3 Cc\n"
        self.showResult(expected, s, True)
    
    @args(String, [])
    def getString(self):
    
        z = zip(self.numbers, self.strings)
        s = ""
        
        for p in z:
            s += str(p.first()) + " " + p.second() + "\n"
        
        return s
