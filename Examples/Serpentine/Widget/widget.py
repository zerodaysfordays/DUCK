"""
# Widget Example

This example is a minimal set of code for an application.

![A screenshot of the application.](widget.png)

See the following guides in Android's documentation for information about
implementing App Widgets:

* docs/guide/topics/appwidgets/index.html
* docs/guide/topics/manifest/provider-element.html
* docs/guide/topics/providers/content-provider-creating.html
"""

#from java.lang import Class
from android.appwidget import AppWidgetProvider
from android.content import ContentProvider
from android.widget import RemoteViews, RemoteViewsService
from android.util import Log

from app_resources import R

"""The WidgetProvider class is derived from the standard AppWidgetProvider
class and represents the application. Android will create an instance of this
class when the user runs it."""

class WidgetProvider(AppWidgetProvider):

    """The initialisation method simply calls the corresponding method in the
    base class."""
    
    def __init__(self):
    
        AppWidgetProvider.__init__(self)
        Log.i("DUCK *** AppWidgetProvider", "__init__")
    
    def onUpdate(self, context, manager, ids):
    
        Log.i("DUCK *** AppWidgetProvider", "onUpdate")
        
        for app_widget_id in ids:
        
            Log.i("DUCK ******", str(app_widget_id))
            views = RemoteViews(context.getPackageName(), R.layout.main)
            layout_id = views.getLayoutId()
            Log.i("DUCK ******", str(layout_id))
            views.removeAllViews(layout_id)
            manager.updateAppWidget(app_widget_id, views)


class WidgetService(RemoteViewsService):

    def __init__(self):
    
        RemoteViewsService.__init__(self)
        Log.i("DUCK *** WidgetService", "__init__")


class DataProvider(ContentProvider):

    def __init__(self):
    
        ContentProvider.__init__(self)
        Log.i("DUCK *** DataProvider", "__init__")
    
    def delete(self, uri, selection, selectionArgs):
    
        return 0
    
    def getType(self, uri):
    
        return "vnd.android.cursor.item"
    
    def insert(self, uri, values):
    
        return None
    
    def onCreate(self):
    
        return True
    
    def query(self, uri, projection, selection, selectionArgs, sortOrder):
    
        return None
    
    def update(self, uri, values, selection, selectionArgs):
    
        return 0
