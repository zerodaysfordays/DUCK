__package__ = "com.example.tuples3"

from Common.helper import TestActivity

class TuplesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        self.a, b = 1, 2
        s = str(self.a) + " " + str(b) + " "
        
        self.a, b = b, self.a
        s += str(self.a) + " " + str(b)
        
        expected = "1 2 2 1"
        self.showResult(expected, s)
