# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.app"

from java.lang import CharSequence, Exception, Object, String
from android.content import BroadcastReceiver, ComponentCallbacks2, \
    ComponentName, Context, Intent, IntentFilter, SharedPreferences
from android.content import ContextWrapper, DialogInterface
from android.content.res import Configuration, Resources
from android.graphics.drawable import Drawable
from android.os import Bundle, Handler, Parcelable, UserHandle
from android.util import AndroidException
from android.view import ContextMenu, ContextThemeWrapper, KeyEvent, Menu, \
    MenuItem, View, ViewGroup, Window, WindowManager
from android.widget import SpinnerAdapter

class ActionBar(Object):

    DISPLAY_HOME_AS_UP = 4
    DISPLAY_SHOW_CUSTOM = 16
    DISPLAY_SHOW_HOME = 2
    DISPLAY_SHOW_TITLE = 8
    DISPLAY_USE_LOGO = 1
    NAVIGATION_MODE_LIST = 1
    NAVIGATION_MODE_STANDARD = 0
    NAVIGATION_MODE_TABS = 2
    
    class LayoutParams(ViewGroup.MarginLayoutParams):
    
        @args(void, [int, int])
        def __init__(self, width, height):
            pass
        
        @args(void, [int, int, int])
        def __init__(self, width, height, gravity):
            pass
        
        @args(void, [int])
        def __init__(self, gravity):
            pass
        
        @args(void, [ActionBar.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [ViewGroup.LayoutParams])
        def __init__(self, source):
            pass
    
    class OnMenuVisibilityListener:
    
        @args(void, [bool])
        def onMenuVisibilityChanged(self, isVisible):
            pass
    
    class OnNavigationListener:
    
        @args(bool, [int, long])
        def onNavigationItemSelected(self, itemPosition, itemId):
            pass
    
    class Tab(Object):
    
        INVALID_POSITION = 0xffffffff
        
        def __init__(self):
            pass
        
        @abstract
        @args(CharSequence, [])
        def getContentDescription(self):
            pass
        
        @abstract
        @args(View, [])
        def getCustomView(self):
            pass
        
        @abstract
        @args(Drawable, [])
        def getIcon(self):
            pass
        
        @abstract
        @args(int, [])
        def getPosition(self):
            pass
        
        @abstract
        @args(Object, [])
        def getTag(self):
            pass
        
        @abstract
        @args(CharSequence, [])
        def getText(self):
            pass
        
        @abstract
        def select(self):
            pass
        
        @abstract
        @args(ActionBar.Tab, [int])
        def setContentDescription(self, resId):
            pass
        
        @abstract
        @args(ActionBar.Tab, [CharSequence])
        def setContentDescription(self, contentDesc):
            pass
        
        @abstract
        @args(ActionBar.Tab, [int])
        def setCustomView(self, layoutResId):
            pass
        
        @abstract
        @args(ActionBar.Tab, [View])
        def setCustomView(self, view):
            pass
        
        @abstract
        @args(ActionBar.Tab, [Drawable])
        def setIcon(self, icon):
            pass
        
        @abstract
        @args(ActionBar.Tab, [int])
        def setIcon(self, resId):
            pass
        
        @abstract
        @args(ActionBar.Tab, [ActionBar.TabListener])
        def setTabListener(self, listener):
            pass
        
        @abstract
        @args(ActionBar.Tab, [Object])
        def setObject(self, tag):
            pass
        
        @abstract
        @args(ActionBar.Tab, [int])
        def setText(self, resId):
            pass
        
        @abstract
        @args(ActionBar.Tab, [CharSequence])
        def setText(self, text):
            pass
    
    class TabListener:
    
        @args(void, [ActionBar.Tab, FragmentTransaction])
        def onTabReselected(self, tab, ft):
            pass
        
        @args(void, [ActionBar.Tab, FragmentTransaction])
        def onTabSelected(self, tab, ft):
            pass
        
        @args(void, [ActionBar.Tab, FragmentTransaction])
        def onTabUnselected(self, tab, ft):
            pass
    
    def __init__(self):
        pass
    
    @abstract
    @args(void, [ActionBar.Tab, bool])
    def addTab(self, tab, setSelected):
        pass
    
    @abstract
    @args(void, [ActionBar.Tab, int])
    def addTab(self, tab, position):
        pass
    
    @abstract
    @args(void, [ActionBar.Tab, int, bool])
    def addTab(self, tab, position, setSelected):
        pass
    
    @abstract
    @args(void, [ActionBar.Tab])
    def addTab(self, tab):
        pass
    
    @abstract
    @args(View, [])
    def getCustomView(self):
        pass
    
    @abstract
    @args(int, [])
    def getDisplayOptions(self):
        pass
    
    @abstract
    @args(int, [])
    def getHeight(self):
        pass
    
    @abstract
    @args(int, [])
    def getHeight(self):
        pass
    
    @abstract
    @args(int, [])
    def getNavigationItemCount(self):
        pass
    
    @abstract
    @args(int, [])
    def getNavigationMode(self):
        pass
    
    @abstract
    @args(ActionBar.Tab, [])
    def getSelectedTab(self):
        pass
    
    @abstract
    @args(CharSequence, [])
    def getSubtitle(self):
        pass
    
    @abstract
    @args(ActionBar.Tab, [int])
    def getTabAt(self, index):
        pass
    
    @abstract
    @args(int, [])
    def getTabCount(self):
        pass
    
    @abstract
    @args(Context, [])
    def getThemedContext(self):
        pass
    
    @abstract
    @args(CharSequence, [])
    def getTitle(self):
        pass
    
    @abstract
    def hide(self):
        pass
    
    @abstract
    @args(bool, [])
    def isShowing(self):
        pass
    
    @abstract
    @args(ActionBar.Tab, [])
    def newTab(self):
        pass
    
    @abstract
    @args(void, [])
    def removeAllTabs(self):
        pass
    
    @abstract
    @args(void, [ActionBar.OnMenuVisibilityListener])
    def removeOnMenuVisibilityListener(self, listener):
        pass
    
    @abstract
    @args(void, [ActionBar.Tab])
    def removeTab(self, tab):
        pass
    
    @abstract
    @args(void, [int])
    def removeTabAt(self, position):
        pass
    
    @abstract
    @args(void, [ActionBar.Tab])
    def selectTab(self, tab):
        pass
    
    @abstract
    @args(void, [Drawable])
    def setBackgroundDrawable(self, drawable):
        pass
    
    @abstract
    @args(void, [int])
    def setCustomView(self, resId):
        pass
    
    @abstract
    @args(void, [View])
    def setCustomView(self, view):
        pass
    
    @abstract
    @args(void, [View, ActionBar.LayoutParams])
    def setCustomView(self, view, layoutParams):
        pass
    
    @abstract
    @args(void, [bool])
    def setDisplayHomeAsUpEnabled(self, showHomeAsUp):
        pass
    
    @abstract
    @args(void, [int, int])
    def setDisplayOptions(self, options, mask):
        pass
    
    @abstract
    @args(void, [int])
    def setDisplayOptions(self, options):
        pass
    
    @abstract
    @args(void, [bool])
    def setDisplayShowCustomEnabled(self, showCustom):
        pass
    
    @abstract
    @args(void, [bool])
    def setDisplayShowHomeEnabled(self, showHome):
        pass
    
    @abstract
    @args(void, [bool])
    def setDisplayShowTitleEnabled(self, showTitle):
        pass
    
    @abstract
    @args(void, [bool])
    def setDisplayUseLogoEnabled(self, showLogo):
        pass
    
    @args(void, [float])
    def setElevation(self, elevation):
        pass
    
    @args(void, [int])
    def setHideOffset(self, offset):
        pass
    
    @args(void, [bool])
    def setHideOnContentScrollEnabled(self, hideOnContentScroll):
        pass
    
    @args(void, [CharSequence])
    def setHomeActionContentDescription(self, description):
        pass
    
    @args(void, [int])
    def setHomeActionContentDescription(self, resId):
        pass
    
    @args(void, [Drawable])
    def setHomeAsUpIndicator(self, indicator):
        pass
    
    @args(void, [int])
    def setHomeAsUpIndicator(self, resId):
        pass
    
    @args(void, [bool])
    def setHomeButtonEnabled(self, enabled):
        pass
    
    @abstract
    @args(void, [Drawable])
    def setIcon(self, icon):
        pass
    
    @abstract
    @args(void, [int])
    def setIcon(self, resId):
        pass
    
    @abstract
    @args(void, [SpinnerAdapter, ActionBar.OnNavigationListener])
    def setListNavigationCallbacks(self, adapter, callback):
        pass
    
    @abstract
    @args(void, [int])
    def setLogo(self, resId):
        pass
    
    @abstract
    @args(void, [Drawable])
    def setLogo(self, logo):
        pass
    
    @abstract
    @args(void, [int])
    def setNavigationMode(self, mode):
        pass
    
    @abstract
    @args(void, [int])
    def setSelectedNavigationItem(self, position):
        pass
    
    @args(void, [Drawable])
    def setSplitBackgroundDrawable(self, drawable):
        pass
    
    @args(void, [Drawable])
    def setStackedBackgroundDrawable(self, drawable):
        pass
    
    @abstract
    @args(void, [int])
    def setSubtitle(self, resId):
        pass
    
    @abstract
    @args(void, [CharSequence])
    def setSubtitle(self, subtitle):
        pass
    
    @abstract
    @args(void, [int])
    def setTitle(self, resId):
        pass
    
    @abstract
    @args(void, [CharSequence])
    def setTitle(self, title):
        pass
    
    @abstract
    @args(void, [])
    def show(self):
        pass


class Activity(ContextThemeWrapper):

    __interfaces__ = [ComponentCallbacks2, View.OnCreateContextMenuListener,
                      Window.Callback]
    
    RESULT_CANCELED = 0
    RESULT_FIRST_USER = 1
    RESULT_OK = -1
    
    def __init__(self):
        pass
    
    @args(void, [View, ViewGroup.LayoutParams])
    def addContentView(self, view, params):
        pass
    
    def closeContextMenu(self):
        pass
    
    def closeOptionsMenu(self):
        pass
    
    @args(PendingIntent, [int, Intent, int])
    def createPendingResult(self, requestCode, data, flags):
        pass
    
    @args(final, [int])
    def dismissDialog(self, id):
        pass
    
    @args(bool, [MotionEvent])
    def dispatchGenericMotionEvent(self, ev):
        pass
    
    @args(bool, [KeyEvent])
    def dispatchKeyEvent(self, event):
        pass
    
    @args(bool, [KeyEvent])
    def dispatchKeyShortcutEvent(self, event):
        pass
    
    @args(bool, [AccessibilityEvent])
    def dispatchPopulateAccessibilityEvent(self, event):
        pass
    
    @args(bool, [MotionEvent])
    def dispatchTouchEvent(self, ev):
        pass
    
    @args(bool, [MotionEvent])
    def dispatchTrackballEvent(self, ev):
        pass
    
    @args(void, [String, FileDescriptor, PrintWriter, [String]])
    def dump(self, prefix, fd, writer, args):
        pass
    
    @args(View, [int])
    def findViewById(self, id):
        pass
    
    def finish(self):
        pass
    
    @args(void, [int])
    def finishActivity(self, requestCode):
        pass
    
    @args(void, [Activity, int])
    def finishActivityFromChild(self, child, requestCode):
        pass
    
    def finishAffinity(self):
        pass
    
    @args(void, [Activity])
    def finishFromChild(self, child):
        pass
    
    @args(ActionBar, [])
    def getActionBar(self):
        pass
    
    @args(ComponentName, [])
    def getCallingActivity(self):
        pass
    
    @args(String, [])
    def getCallingPackage(self):
        pass
    
    @args(int, [])
    def getChangingConfigurations(self):
        pass
    
    @args(ComponentName, [])
    def getComponentName(self):
        pass
    
    @args(View, [])
    def getCurrentFocus(self):
        pass
    
    @args(Intent, [])
    def getIntent(self):
        pass
    
    @args(String, [])
    def getLocalClassName(self):
        pass
    
    @final
    @args(Activity, [])
    def getParent(self):
        pass
    
    @args(Intent, [])
    def getParentActivityIntent(self):
        pass
    
    @args(SharedPreferences, [int])
    def getPreferences(self, mode):
        pass
    
    @args(int, [])
    def getRequestedOrientation(self):
        pass
    
    @args(Window, [])
    def getWindow(self):
        pass
    
    @args(WindowManager, [])
    def getWindowManager(self):
        pass
    
    @args(void, [int, int, Intent])
    def onActivityResult(self, requestCode, resultCode, data):
        pass
    
    def onBackPressed(self):
        pass
    
    @args(void, [Configuration])
    def onConfigurationChanged(self, config):
        pass

    def onContentChanged(self):
        pass
    
    @args(bool, [MenuItem])
    def onContextItemSelected(self, item):
        pass
    
    @args(void, [Menu])
    def onContextMenuClosed(self, menu):
        pass
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
        pass
    
    @args(void, [Bundle, PersistableBundle])
    def onCreate(self, savedInstanceState, persistentState):
        pass
    
    @args(void, [ContextMenu, View, ContextMenu.ContextMenuInfo])
    def onCreateContextMenu(self, menu, v, menuInfo):
        pass
    
    @args(CharSequence, [])
    def onCreateDescription(self):
        pass
    
    @args(void, [TaskStackBuilder])
    def onCreateNavigateUpTaskStack(self, builder):
        pass
    
    @args(bool, [Menu])
    def onCreateOptionsMenu(self, menu):
        pass
    
    def onDestroy(self):
        pass
    
    @args(void, [Intent])
    def onNewIntent(self, intent):
        pass
    
    @args(bool, [MenuItem])
    def onOptionsItemSelected(self, item):
        pass
    
    @args(bool, [Menu])
    def onPrepareOptionsMenu(self, menu):
        pass
    
    def onPause(self):
        pass
    
    @args(void, [Bundle])
    def onPostCreate(self, savedInstanceState):
        pass
    
    def onPostResume(self):
        pass
    
    def onRestart(self):
        pass
    
    @args(void, [Bundle])
    def onRestoreInstanceState(self, savedInstanceState):
        pass
    
    def onResume(self):
        pass
    
    @args(void, [Bundle])
    def onSaveInstanceState(self, outState):
        pass
    
    def onStart(self):
        pass
    
    def onStop(self):
        pass
    
    @args(void, [CharSequence, int])
    def onTitleChanged(self, title, color):
        pass
    
    def onUserLeaveHint(self):
        pass
    
    # ...
    
    @args(void, [View])
    def openContextMenu(self, view):
        pass
    
    def openOptionsMenu(self):
        pass
    
    @args(void, [int, int])
    def overridePendingTransition(self, enterAnim, exitAnim):
        pass
    
    def postponeEnterTransition(self):
        pass
    
    def recreate(self):
        pass
    
    @args(void, [View])
    def registerForContextMenu(self, view):
        pass
    
    # ...
    
    @args(void, [int])
    def setContentView(self, layoutResID):
        pass
    
    @args(void, [View])
    def setContentView(self, view):
        pass
    
    @args(void, [Intent])
    def startActivity(self, intent):
        pass
    
    @args(void, [Intent, Bundle])
    def startActivity(self, intent, options):
        pass
    
    @args(void, [Intent, int])
    def startActivityForResult(self, intent, requestCode):
        pass
    
    @args(void, [Intent, int, Bundle])
    def startActivityForResult(self, intent, requestCode, options):
        pass
    
    # ...
    
    @args(void, [View])
    def unregisterForContextMenu(self, view):
        pass


class Dialog(Object):

    __interfaces__ = [DialogInterface, Window.Callback, KeyEvent.Callback,
                      View.OnCreateContextMenuListener]
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, int])
    def __init__(self, context, themeResId):
        pass
    
    @protected
    @args(void, [Context, bool, DialogInterface.OnCancelListener])
    def __init__(self, context, cancelable, cancelListener):
        pass
    
    @args(void, [View, ViewGroup.LayoutParams])
    def addContentView(self, view, params):
        pass
    
    def cancel(self):
        pass
    
    def closeOptionsMenu(self):
        pass
    
    def create(self):
        pass
    
    def dismiss(self):
        pass
    
    @args(bool, [MotionEvent])
    def dispatchGenericMotionEvent(self, event):
        pass
    
    @args(bool, [KeyEvent])
    def dispatchKeyEvent(self, event):
        pass
    
    @args(bool, [KeyEvent])
    def dispatchKeyShortcutEvent(self, event):
        pass
    
    @args(bool, [AccessibilityEvent])
    def dispatchPopulateAccessibilityEvent(self, event):
        pass
    
    @args(bool, [MotionEvent])
    def dispatchTouchEvent(self, event):
        pass
    
    @args(bool, [MotionEvent])
    def dispatchTrackballEvent(self, event):
        pass
    
    @args(View, [int])
    def findViewById(self, id):
        pass
    
    @args(ActionBar, [])
    def getActionBar(self):
        pass
    
    @final
    @args(Context, [])
    def getContext(self):
        pass
    
    @args(View, [])
    def getCurrentFocus(self):
        pass
    
    @args(LayoutInflater, [])
    def getLayoutInflater(self):
        pass
    
    @final
    @args(Activity, [])
    def getOwnerActivity(self):
        pass
    
    @final
    @args(SearchEvent, [])
    def getSearchEvent(self):
        pass
    
    @final
    @args(int, [])
    def getVolumeControlStream(self):
        pass
    
    @args(Window, [])
    def getWindow(self):
        pass
    
    def hide(self):
        pass
    
    def invalidateOptionsMenu(self):
        pass
    
    @args(bool, [])
    def isShowing(self):
        pass
    
    @args(void, [ActionMode])
    def onActionModeFinished(self, mode):
        pass
    
    @args(void, [ActionMode])
    def onActionModeStarted(self, mode):
        pass
    
    def onAttachedToWindow(self):
        pass
    
    def onBackPressed(self):
        pass
    
    def onContentChanged(self):
        pass
    
    @args(bool, [MenuItem])
    def onContextItemSelected(self, item):
        pass
    
    @args(void, [Menu])
    def onContextMenuClosed(self, menu):
        pass
    
    @args(void, [ContextMenu, View, ContextMenu.ContextMenuInfo])
    def onCreateContextMenu(self, menu, view, menuInfo):
        pass
    
    @args(bool, [Menu])
    def onCreateOptionsMenu(self, menu):
        pass
    
    @args(bool, [int, Menu])
    def onCreatePanelMenu(self, featureId, menu):
        pass
    
    @args(View, [int])
    def onCreatePanelView(self, featureId):
        pass
    
    def onDetachedFromWindow(self):
        pass
    
    @args(bool, [MotionEvent])
    def onGenericMotionEvent(self, event):
        pass
    
    @args(bool, [int, KeyEvent])
    def onKeyDown(self, keyCode, event):
        pass
    
    @args(bool, [int, KeyEvent])
    def onKeyLongPress(self, keyCode, event):
        pass
    
    @args(bool, [int, int, KeyEvent])
    def onKeyMultiple(self, keyCode, repeatCount, event):
        pass
    
    @args(bool, [int, KeyEvent])
    def onKeyShortcut(self, keyCode, event):
        pass
    
    @args(bool, [int, KeyEvent])
    def onKeyUp(self, keyCode, event):
        pass
    
    @args(bool, [int, MenuItem])
    def onMenuItemSelected(self, featureId, item):
        pass
    
    @args(bool, [int, Menu])
    def onMenuOpened(self, featureId, menu):
        pass
    
    @args(bool, [MenuItem])
    def onOptionsItemSelected(self, item):
        pass
    
    @args(void, [Menu])
    def onOptionsMenuClosed(self, menu):
        pass
    
    @args(void, [int, Menu])
    def onPanelClosed(self, featureId, menu):
        pass
    
    @args(bool, [Menu])
    def onPrepareOptionsMenu(self, menu):
        pass
    
    @args(bool, [int, View, Menu])
    def onPreparePanel(self, featureId, view, menu):
        pass
    
    @args(void, [Bundle])
    def onRestoreInstanceState(self, savedInstanceState):
        pass
    
    @args(Bundle, [])
    def onSaveInstanceState(self):
        pass
    
    @args(bool, [])
    def onSearchRequested(self):
        pass
    
    @args(bool, [SearchEvent])
    def onSearchRequested(self, searchEvent):
        pass
    
    @args(bool, [MotionEvent])
    def onTouchEvent(self, event):
        pass
    
    @args(bool, [MotionEvent])
    def onTrackballEvent(self, event):
        pass
    
    @args(void, [WindowManager.LayoutParams])
    def onWindowAttributesChanged(self, params):
        pass
    
    @args(void, [bool])
    def onWindowFocusChanged(self, hasFocus):
        pass
    
    @args(ActionMode, [ActionMode.Callback])
    def onWindowStartingActionMode(self, callback):
        pass
    
    @args(ActionMode, [ActionMode.Callback, int])
    def onWindowStartingActionMode(self, callback, type):
        pass
    
    @args(void, [View])
    def openContextMenu(self, view):
        pass
    
    def openOptionsMenu(self):
        pass
    
    @args(void, [View])
    def registerForContextMenu(self, view):
        pass
    
    @final
    @args(bool, [int])
    def requestWindowFeature(self, featureId):
        pass
    
    @args(void, [Message])
    def setCancelMessage(self, msg):
        pass
    
    @args(void, [bool])
    def setCancelable(self, flag):
        pass
    
    @args(void, [bool])
    def setCanceledOnTouchOutside(self, cancel):
        pass
    
    @args(void, [View])
    def setContentView(self, view):
        pass
    
    @args(void, [int])
    def setContentView(self, layoutResID):
        pass
    
    @args(void, [View, ViewGroup.LayoutParams])
    def setContentView(self, view, params):
        pass
    
    @args(void, [Message])
    def setDismissMessage(self, msg):
        pass
    
    @final
    @args(void, [int, Drawable])
    def setFeatureDrawable(self, featureId, drawable):
        pass
    
    @final
    @args(void, [int, int])
    def setFeatureDrawableAlpha(self, featureId, alpha):
        pass
    
    @final
    @args(void, [int, int])
    def setFeatureDrawableResource(self, featureId, resId):
        pass
    
    @final
    @args(void, [int, Uri])
    def setFeatureDrawableUri(self, featureId, uri):
        pass
    
    @args(void, [DialogInterface.OnCancelListener])
    def setOnCancelListener(self, listener):
        pass
    
    @args(void, [DialogInterface.OnDismissListener])
    def setOnDismissListener(self, listener):
        pass
    
    @args(void, [DialogInterface.OnKeyListener])
    def setOnKeyListener(self, onKeyListener):
        pass
    
    @args(void, [DialogInterface.OnShowListener])
    def setOnShowListener(self, listener):
        pass
    
    @final
    @args(void, [Activity])
    def setOwnerActivity(self, activity):
        pass
    
    @args(void, [int])
    def setTitle(self, titleId):
        pass
    
    @args(void, [CharSequence])
    def setTitle(self, title):
        pass
    
    @final
    @args(void, [int])
    def setVolumeControlStream(self, streamType):
        pass
    
    def show(self):
        pass
    
    @args(void, [bool])
    def takeKeyEvents(self, get):
        pass
    
    @args(void, [View])
    def unregisterForContextMenu(self, view):
        pass


class Fragment(Object):

    class SavedState(Object):
    
        pass
    
    def __init__(self):
        pass
    
    @args(Activity, [])
    def getActivity(self):
        pass
    
    @args(Bundle, [])
    def getArguments(self):
        pass
    
    @args(int, [])
    def getId(self):
        pass
    
    @args(Activity, [])
    def getActivity(self):
        pass
    
    @args(Fragment, [])
    def getParentFragment(self):
        pass
    
    @args(Resources, [])
    def getResources(self):
        pass
    
    @args(String, [int])
    def getString(self, resId):
        pass
    
    @args(String, [])
    def getTag(self):
        pass
    
    @args(Fragment, [])
    def getTargetFragment(self):
        pass
    
    @args(int, [])
    def getTargetRequestCode(self):
        pass
    
    @args(CharSequence, [int])
    def getText(self, resId):
        pass
    
    @args(bool, [])
    def getUserVisibleHint(self):
        pass
    
    @args(View, [])
    def getView(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @static
    @args(Fragment, [Context, String])
    def instantiate(context, fname):
        pass
    
    @static
    @args(Fragment, [Context, String, Bundle])
    def instantiate(context, fname, args):
        pass
    
    @args(bool, [])
    def isAdded(self):
        pass
    
    @args(bool, [])
    def isDetached(self):
        pass
    
    @args(bool, [])
    def isHidden(self):
        pass
    
    @args(bool, [])
    def isInLayout(self):
        pass
    
    @args(bool, [])
    def isRemoving(self):
        pass
    
    @args(bool, [])
    def isResumed(self):
        pass
    
    @args(bool, [])
    def isVisible(self):
        pass
    
    @args(void, [Bundle])
    def onActivityCreated(self, savedInstanceState):
        pass
    
    @args(void, [int, int, Intent])
    def onActivityResult(self, requestCode, resultCode, data):
        pass
    
    @args(void, [Activity])
    def onAttach(self, activity):
        pass
    
    @args(void, [Configuration])
    def onConfigurationChanged(self, newConfig):
        pass
    
    @args(void, [MenuItem])
    def onContextItemSelected(self, item):
        pass
    
    @args(void, [Bundle])
    def onCreate(self, savedInstanceState):
        pass
    
    @args(void, [Menu, MenuInflater])
    def onCreateOptionsMenu(self, menu, inflater):
        pass
    
    @args(View, [LayoutInflater, ViewGroup, Bundle])
    def onCreateView(self, inflater, container, savedInstanceState):
        pass
    
    def onDestroy(self):
        pass
    
    def onDestroyOptionsMenu(self):
        pass
    
    def onDestroyView(self):
        pass
    
    def onDetach(self):
        pass
    
    @args(void, [bool])
    def onHiddenChanged(self, hidden):
        pass
    
    def onLowMemory(self):
        pass
    
    @args(bool, [MenuItem])
    def onOptionsItemSelected(self, item):
        pass
    
    @args(void, [Menu])
    def onOptionsMenuClosed(self, menu):
        pass
    
    def onPause(self):
        pass
    
    @args(void, [Menu])
    def onPrepareOptionsMenu(self, menu):
        pass
    
    def onResume(self):
        pass
    
    @args(void, [Bundle])
    def onSaveInstanceState(self, outState):
        pass
    
    def onStart(self):
        pass
    
    def onStop(self):
        pass
    
    @args(void, [int])
    def onTrimMemory(self, level):
        pass
    
    @args(void, [View, Bundle])
    def onViewCreated(self, view, savedInstanceState):
        pass
    
    @args(void, [Bundle])
    def onViewStateRestored(self, savedInstanceState):
        pass
    
    @args(void, [View])
    def registerForContextMenu(self, view):
        pass
    
    @args(void, [Bundle])
    def setArguments(self, args):
        pass
    
    @args(void, [bool])
    def setHasOptionsMenu(self, hasMenu):
        pass
    
    @args(void, [Fragment.SavedState])
    def setInitialSavedState(self, state):
        pass
    
    @args(void, [bool])
    def setMenuVisibility(self, menuVisible):
        pass
    
    @args(void, [bool])
    def setRetainInstance(self, retain):
        pass
    
    @args(void, [Fragment, int])
    def setTargetFragment(self, fragment, requestCode):
        pass
    
    @args(void, [bool])
    def setUserVisibleHint(self, isVisibleToUser):
        pass
    
    @args(void, [Intent])
    def startActivity(self, intent):
        pass
    
    @args(void, [Intent, int])
    def startActivityForResult(self, intent, requestCode):
        pass
    
    @args(void, [Intent, int, Bundle])
    def startActivityForResult(self, intent, requestCode, options):
        pass
    
    @args(void, [View])
    def unregisterForContextMenu(self, view):
        pass


class FragmentTransaction(Object):

    TRANSIT_ENTER_MASK = 0x1000
    TRANSIT_EXIT_MASK = 0x2000
    TRANSIT_FRAGMENT_CLOSE = 0x2002
    TRANSIT_FRAGMENT_FADE = 0x1003
    TRANSIT_FRAGMENT_OPEN = 0x1001
    TRANSIT_NONE = 0
    TRANSIT_UNSET = 0xffffffff
    
    def __init__(self):
        pass
    
    @abstract
    @args(FragmentTransaction, [int, Fragment])
    def add(self, containerViewId, fragment):
        pass
    
    @abstract
    @args(FragmentTransaction, [Fragment, String])
    def add(self, fragment, tag):
        pass
    
    @abstract
    @args(FragmentTransaction, [int, Fragment, String])
    def add(self, containerViewId, fragment, tag):
        pass
    
    @abstract
    @args(FragmentTransaction, [View, String])
    def addSharedElement(self, sharedElement, name):
        pass
    
    @abstract
    @args(FragmentTransaction, [String])
    def addToBackStack(self, name):
        pass
    
    @abstract
    @args(FragmentTransaction, [Fragment])
    def attach(self, fragment):
        pass
    
    @abstract
    @args(int, [])
    def commit(self):
        pass
    
    @abstract
    @args(int, [])
    def commitAllowingStateLoss(self):
        pass
    
    @abstract
    @args(FragmentTransaction, [Fragment])
    def detach(self, fragment):
        pass
    
    @abstract
    @args(FragmentTransaction, [])
    def disallowAddToBackStack(self):
        pass
    
    @abstract
    @args(FragmentTransaction, [Fragment])
    def hide(self, fragment):
        pass
    
    @abstract
    @args(bool, [])
    def isAddToBackStackAllowed(self):
        pass
    
    @abstract
    @args(bool, [])
    def isEmpty(self):
        pass
    
    @abstract
    @args(FragmentTransaction, [Fragment])
    def remove(self, fragment):
        pass
    
    @abstract
    @args(FragmentTransaction, [int, Fragment, String])
    def replace(self, containerViewId, fragment, tag):
        pass
    
    @abstract
    @args(FragmentTransaction, [int, Fragment])
    def replace(self, containerViewId, fragment):
        pass
    
    @abstract
    @args(FragmentTransaction, [int])
    def setBreadCrumbShortTitle(self, res):
        pass
    
    @abstract
    @args(FragmentTransaction, [CharSequence])
    def setBreadCrumbShortTitle(self, text):
        pass
    
    @abstract
    @args(FragmentTransaction, [CharSequence])
    def setBreadCrumbTitle(self, text):
        pass
    
    @abstract
    @args(FragmentTransaction, [int])
    def setBreadCrumbTitle(self, res):
        pass
    
    @abstract
    @args(FragmentTransaction, [int, int, int, int])
    def setCustomAnimations(self, enter, exit, popEnter, popExit):
        pass
    
    @abstract
    @args(FragmentTransaction, [int, int])
    def setCustomAnimations(self, enter, exit):
        pass
    
    @abstract
    @args(FragmentTransaction, [int])
    def setTransition(self, transition):
        pass
    
    @abstract
    @args(FragmentTransaction, [int])
    def setTransitionStyle(self, styleResourceId):
        pass
    
    @abstract
    @args(FragmentTransaction, [Fragment])
    def show(self, fragment):
        pass


class ListActivity(Activity):

    def __init__(self):
        pass
    
    @args(ListAdapter, [])
    def getListAdapter(self):
        pass
    
    @args(ListView, [])
    def getListView(self):
        pass
    
    @args(long, [])
    def getSelectedItemId(self):
        pass
    
    @args(int, [])
    def getSelectedItemPosition(self):
        pass
    
    @args(void, [])
    def onContentChanged(self):
        pass
    
    @args(void, [ListAdapter])
    def setListAdapter(self, adapter):
        pass
    
    @args(void, [int])
    def setSelection(self, position):
        pass
    
    @protected
    def onDestroy(self):
        pass
    
    @protected
    @args(void, [ListView, View, int, long])
    def onListItemClick(self, l, v, position, id):
        pass
    
    @protected
    @args(void, [Bundle])
    def onRestoreInstanceState(self, state):
        pass


class PendingIntent(Object):

    __interfaces__ = [Parcelable]
    __fields__ = {
        "CREATOR": Parcelable.Creator(PendingIntent)
        }
    
    FLAG_CANCEL_CURRENT = 0x10000000
    FLAG_IMMUTABLE      = 0x04000000
    FLAG_NO_CREATE      = 0x20000000
    FLAG_ONE_SHOT       = 0x40000000
    FLAG_UPDATE_CURRENT = 0x08000000
    
    class CanceledException(AndroidException):
    
        def __init__(self):
            pass
        
        @args(void, [String])
        def __init__(self):
            pass
        
        @args(void, [Exception])
        def __init__(self):
            pass
    
    class OnFinished:
    
        @args(void, [PendingIntent, Intent, int, String, Bundle])
        def onSendFinished(self, pendingIntent, intent, resultCode, resultData,
                           resultExtras):
            pass
    
    def cancel(self):
        pass
    
    @static
    @args(PendingIntent, [Context, int, [Intent], int])
    def getActivities(context, requestCode, intents, flags):
        pass
    
    @static
    @args(PendingIntent, [Context, int, [Intent], int, Bundle])
    def getActivities(context, requestCode, intents, flags, options):
        pass
    
    @static
    @args(PendingIntent, [Context, int, Intent, int])
    def getActivity(context, requestCode, intent, flags):
        pass
    
    @static
    @args(PendingIntent, [Context, int, Intent, int])
    def getActivity(context, requestCode, intent, flags):
        pass
    
    @static
    @args(PendingIntent, [Context, int, Intent, int])
    def getBroadcast(context, requestCode, intent, flags):
        pass
    
    @args(String, [])
    def getCreatorPackage(self):
        pass
    
    @args(int, [])
    def getCreatorUid(self):
        pass
    
    @args(UserHandle, [])
    def getCreatorUserHandle(self):
        pass
    
    @args(IntentSender, [])
    def getIntentSender(self):
        pass
    
    @static
    @args(PendingIntent, [Context, int, Intent, int])
    def getService(context, requestCode, intent, flags):
        pass
    
    @args(String, [])
    def getTargetPackage(self):
        pass
    
    @static
    @args(PendingIntent, [Parcel])
    def readPendingIntentOrNullFromParcel(input):
        pass
    
    @args(void, [int])
    def send(self, code):
        pass
    
    @args(void, [Context, int, Intent, PendingIntent.OnFinished, Handler, String])
    def send(self, context, code, intent, onFinished, handler, requiredPermission):
        pass
    
    @args(void, [Context, int, Intent, PendingIntent.OnFinished, Handler, String, Bundle])
    def send(self, context, code, intent, onFinished, handler, requiredPermission, options):
        pass
    
    @args(void, [int, PendingIntent.OnFinished, Handler])
    def send(self, code, onFinished, handler):
        pass
    
    @args(void, [Context, int, Intent, PendingIntent.OnFinished, Handler])
    def send(self, context, code, intent, onFinished, handler):
        pass
    
    def send(self):
        pass
    
    @args(void, [Context, int, Intent])
    def send(self, context, code, intent):
        pass
    
    @static
    @args(void, [PendingIntent, Parcel])
    def writePendingIntentOrNullToParcel(sender, output):
        pass


class Service(ContextWrapper):

    __interfaces__ = [ComponentCallbacks2]
    
    START_CONTINUATION_MASK = 15
    START_FLAG_REDELIVERY = 1
    START_FLAG_RETRY = 2
    START_NOT_STICKY = 2
    START_REDELIVER_INTENT = 3
    START_STICKY = 1
    START_STICKY_COMPATIBILITY = 0
    
    def __init__(self):
        pass
    
#    @abstract
#    @args(IBinder, [Intent])
#    def onBind(self, intent):
#        pass
    
    @args(void, [Configuration])
    def onConfigurationChanged(self, newConfig):
        pass
    
    def onCreate(self):
        pass
    
    def onDestroy(self):
        pass
    
    def onLowMemory(self):
        pass
    
    @args(void, [Intent])
    def onRebind(self, intent):
        pass
    
    @args(int, [Intent, int, int])
    def onStartCommand(self, intent, flags, startId):
        pass
    
    @args(void, [Intent])
    def onTaskRemoved(self, rootIntent):
        pass
    
    @args(void, [int])
    def onTrimMemory(self, level):
        pass
    
    @args(bool, [Intent])
    def onUnbind(self, intent):
        pass
    
    #@final
    #@args(void, [int, Notification])
    #def startForeground(self, id, notification):
    #    pass
    
    #@final
    #@args(void, [bool])
    #def stopForeground(self, removeNotification):
    #    pass
    
    @final
    def stopSelf(self):
        pass
    
    @final
    @args(void, [int])
    def stopSelf(self, startId):
        pass
    
    @final
    @args(bool, [int])
    def stopSelfResult(self, startId):
        pass
