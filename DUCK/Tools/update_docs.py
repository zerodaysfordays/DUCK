#!/usr/bin/env python

import os, sys
from pygments import highlight
from pygments.lexers import BashLexer, PythonLexer, TextLexer
from pygments.formatters import HtmlFormatter
from markdown import markdown

extra_styles = \
'''.highlight pre {
  background: #f0f0f0;
  padding: 0.5em;
  border-left: 1px solid #a0a0a0;
  border-right: 1px solid #a0a0a0;
  border-top: 1px dashed #c0c0c0;
  border-bottom: 1px dashed #c0c0c0
}

.navbar {
  background: #e0e8f0;
  padding-top: 0.25em;
  padding-bottom: 0.25em
}

.navbar span {
  float: left;
  text-align: center;
  width: 33%
}

h1 {
  text-align: center
}'''

navigation_bar = \
'''<div class="navbar">&nbsp;
<span><a href="%(home)s">DUCK</a></span>
<span><a href="%(examples)s">Examples</a> and
      <a href="%(demos)s">Demos</a></span>
<span><a href="%(docs)s">Documentation</a></span>
</div>'''

def markup(text, in_code, code_type, formatter):

    if in_code and text:
        if code_type == '"':
            return [highlight(text, TextLexer(), formatter)]
        elif code_type == "\\":
            return [highlight(text, BashLexer(), formatter)]
        else:
            return [highlight(text, PythonLexer(), formatter)]
    elif not in_code and text:
        return [markdown(text)]
    else:
        return []


def process_file(file_name, relpath):

    formatter = HtmlFormatter(style="colorful")
    
    lines = open(file_name).readlines()
    title = ""
    previous = ""
    new = []
    current = ""
    in_code = False
    code_type = ""
    
    for line in lines:
    
        if not title:
            if line.startswith("# "):
                title = line[2:].strip()
            else:
                l = line.strip()
                if l == (len(l) * "=") and len(l) == len(previous):
                    title = previous
            
            previous = line.strip()
        
        if line.startswith("```"):
        
            new += markup(current, in_code, code_type, formatter)
            
            in_code = not in_code
            if in_code:
                code_type = line[3:].strip()
            
            current = ""
        
        else:
            current += line
    
    # Process any unprocessed text.
    new += markup(current, in_code, code_type, formatter)
    
    output_file = file_name[:-3] + ".html"
    f = open(output_file, "w")
    f.write('<html>\n<head>\n')
    f.write('<title>%s</title>\n' % title)
    f.write('<style type="text/css">\n')
    f.write(formatter.get_style_defs() + '\n')
    f.write(extra_styles)
    f.write('\n</style>\n')
    f.write('</head>\n')
    
    f.write('<body>\n')
    f.write(navigation_bar % {
        "home": (len(relpath) * "../") + "README.html",
        "examples": (len(relpath) * "../") + "Examples/Serpentine/README.html",
        "demos": (len(relpath) * "../") + "Demos/Serpentine/README.html",
        "docs": (len(relpath) * "../") + "Documents/index.html"
        })
    
    for html in new:
        f.write(html)
    f.write('</body>\n</html>\n')
    f.close()
    
    print "Processed", file_name, "to create", output_file


def process(directory, relpath = []):

    for name in os.listdir(directory):
    
        obj = os.path.join(directory, name)
        
        if os.path.isdir(obj):
            process(obj, relpath + [name])
        
        elif obj.endswith(".md"):
            process_file(obj, relpath)


if __name__ == "__main__":

    process(os.curdir)
    sys.exit()
