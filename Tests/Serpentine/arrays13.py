__package__ = "com.example.arrays13"

from java.nio import ByteBuffer

from Common.helper import TestActivity

class ArraysActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        buf = ByteBuffer.allocate(28)
        
        buf.put(byte(0))
        buf.put(array([byte(1), byte(2), byte(3)]))
        buf.putInt(4)
        buf.putFloat(5)
        buf.putDouble(6.0)
        buf.putLong(7)
        
        a = buf.array()
        s = ""
        
        for i in a:
            s += str(i) + " "
        
        expected = "0 1 2 3 0 0 0 4 64 -96 0 0 64 24 0 0 0 0 0 0 0 0 0 0 0 0 0 7 "
        
        self.showResult(expected, s)
