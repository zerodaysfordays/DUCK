__package__ = "com.example.imports10"

from Common.helper import TestActivity
from android.os import Bundle

# This test should fail because Math should not be imported as a side effect of
# importing the String class.

class ImportsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # If the example compiles then the test passed.
        t = android.os.SystemClock.elapsedRealtime()
        self.showResult("", "")
