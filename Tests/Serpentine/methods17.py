__package__ = "com.example.methods17"

from java.lang import Object, String
from android.os import Bundle

from Common.helper import TestActivity

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = str(self.fn())
        
        self.showResult("123", s)
    
    # Declare that the method returns an integer but return a float.
    @args(int, [])
    def fn(self):
    
        return 123.0
