# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.provider"

from java.io import File
from java.lang import Object, String
from java.util import List, Set
from android.content import Context
from android.net import Uri
from android.os import Parcel, Parcelable

class BaseColumns:

    _COUNT = "_count"
    _ID = "_id"


class CalendarContract(Object):

    ACCOUNT_TYPE_LOCAL = "LOCAL"
    ACTION_EVENT_REMINDER = "android.intent.action.EVENT_REMINDER"
    ACTION_HANDLE_CUSTOM_EVENT = "android.provider.calendar.action.HANDLE_CUSTOM_EVENT"
    AUTHORITY = "com.android.calendar"
    CALLER_IS_SYNCADAPTER = "caller_is_syncadapter"
    EXTRA_CUSTOM_APP_URI = "customAppUri"
    EXTRA_EVENT_ALL_DAY = "allDay"
    EXTRA_EVENT_BEGIN_TIME = "beginTime"
    EXTRA_EVENT_END_TIME = "endTime"
    
    __fields__ = {
        "CONTENT_URI": Uri
        }
    
    class CalendarColumns:
    
        ALLOWED_ATTENDEE_TYPES = "allowedAttendeeTypes"
        ALLOWED_AVAILABILITY = "allowedAvailability"
        ALLOWED_REMINDERS = "allowedReminders"
        CALENDAR_ACCESS_LEVEL = "calendar_access_level"
        CALENDAR_COLOR = "calendar_color"
        CALENDAR_COLOR_KEY = "calendar_color_index"
        CALENDAR_DISPLAY_NAME = "calendar_displayName"
        CALENDAR_TIME_ZONE = "calendar_timezone"
        CAL_ACCESS_CONTRIBUTOR = 500
        CAL_ACCESS_EDITOR = 600
        CAL_ACCESS_FREEBUSY = 100
        CAL_ACCESS_NONE = 0
        CAL_ACCESS_OVERRIDE = 400
        CAL_ACCESS_OWNER = 700
        CAL_ACCESS_READ = 200
        CAL_ACCESS_RESPOND = 300
        CAL_ACCESS_ROOT = 800
        CAN_MODIFY_TIME_ZONE = "canModifyTimeZone"
        CAN_ORGANIZER_RESPOND = "canOrganizerRespond"
        IS_PRIMARY = "isPrimary"
        MAX_REMINDERS = "maxReminders"
        OWNER_ACCOUNT = "ownerAccount"
        SYNC_EVENTS = "sync_events"
        VISIBLE = "visible"
    
    class Calendars(Object):
    
        CALENDAR_LOCATION = "calendar_location"
        DEFAULT_SORT_ORDER = "calendar_displayName"
        NAME = "name"
    
        __interfaces__ = [BaseColumns, CalendarContract.SyncColumns, CalendarContract.CalendarColumns]
        __fields__ = {
            "CONTENT_URI": Uri
            }
    
    class CalendarSyncColumns:
    
        pass
    
    class Events(Object):
    
        __interfaces__ = [BaseColumns] # ...
        __fields__ = {
            "CONTENT_EXCEPTION_URI": Uri,
            "CONTENT_URI": Uri
            }
    
    class SyncColumns:
    
        __interfaces__ = [CalendarContract.CalendarSyncColumns]
        
        ACCOUNT_NAME = "account_name"
        ACCOUNT_TYPE = "account_type"
        CAN_PARTIALLY_UPDATE = "canPartiallyUpdate"
        DELETED = "deleted"
        DIRTY = "dirty"
        MUTATORS = "mutators"
        _SYNC_ID = "_sync_id"


class CallLog(Object):

    AUTHORITY = "call_log"
    
    __fields__ = {
        "CONTENT_URI": Uri
        }
    
    class Calls(Object):
    
        CACHED_FORMATTED_NUMBER = "formatted_number" 
        CACHED_LOOKUP_URI =  "lookup_uri" 
        CACHED_MATCHED_NUMBER =  "matched_number"
        CACHED_NAME = "name"
        CACHED_NORMALIZED_NUMBER = "normalized_number"
        CACHED_NUMBER_LABEL = "numberlabel"
        CACHED_NUMBER_TYPE = "numbertype"
        CACHED_PHOTO_ID = "photo_id"
        CACHED_PHOTO_URI = "photo_uri"
        CONTENT_ITEM_TYPE = "vnd.android.cursor.item/calls"
        CONTENT_TYPE = "vnd.android.cursor.dir/calls"
        COUNTRY_ISO = "countryiso"
        DATA_USAGE = "data_usage"
        DATE = "date"
        DEFAULT_SORT_ORDER = "date DESC"
        DURATION = "duration"
        EXTRA_CALL_TYPE_FILTER = "android.provider.extra.CALL_TYPE_FILTER"
        FEATURES = "features"
        FEATURES_VIDEO = 1
        GEOCODED_LOCATION = "geocoded_location"
        INCOMING_TYPE = 1
        IS_READ = "is_read"
        LIMIT_PARAM_KEY = "limit"
        MISSED_TYPE = 3
        NEW = "new"
        NUMBER = "number"
        NUMBER_PRESENTATION = "presentation"
        OFFSET_PARAM_KEY = "offset"
        OUTGOING_TYPE = 2
        PHONE_ACCOUNT_COMPONENT_NAME = "subscription_component_name"
        PHONE_ACCOUNT_ID = "subscription_id"
        PRESENTATION_ALLOWED = 1
        PRESENTATION_PAYPHONE = 4
        PRESENTATION_RESTRICTED = 2
        PRESENTATION_UNKNOWN = 3
        TRANSCRIPTION = "transcription"
        TYPE = "type"
        VOICEMAIL_TYPE = 4
        VOICEMAIL_URI = "voicemail_uri"
        
        __interfaces__ = [BaseColumns]
        __fields__ = {
            "CONTENT_FILTER_URI": Uri,
            "CONTENT_URI": Uri,
            "CONTENT_URI_WITH_VOICEMAIL": Uri
            }
        
        def __init__(self):
            pass
        
        @static
        @args(String, [Context])
        def getLastOutgoingCall(context):
            pass
    
    def __init__(self):
        pass


class ContactsContract(Object):

    class Contacts(Object):
    
        __fields__ = {
            "CONTENT_LOOKUP_URI": Uri,
            "CONTENT_URI": Uri
            }
    
    class ContactsColumns(Object):
    
        CONTACT_LAST_UPDATED_TIMESTAMP = "contact_last_updated_timestamp"
        DISPLAY_NAME = "display_name"
        HAS_PHONE_NUMBER = "has_phone_number"
        IN_DEFAULT_DIRECTORY = "in_default_directory"
        IN_VISIBLE_GROUP = "in_visible_group"
        IS_USER_PROFILE = "is_user_profile"
        LOOKUP_KEY = "lookup"
        NAME_RAW_CONTACT_ID = "name_raw_contact_id"
        PHOTO_FILE_ID = "photo_file_id"
        PHOTO_ID = "photo_id"
        PHOTO_THUMBNAIL_URI = "photo_thumb_uri"
        PHOTO_URI = "photo_uri"
    
    class CommonDataKinds(Object):
    
        class Organization(Object):
        
            CONTENT_ITEM_TYPE = "vnd.android.cursor.item/organization"
            COMPANY = "data1"
        
        class Phone(Object):
        
            CONTENT_ITEM_TYPE = "vnd.android.cursor.item/phone_v2"
            NORMALIZED_NUMBER = "data4"
            NUMBER = "data1"
    
    class Data(Object):
    
        __interfaces__ = [ContactsContract.DataColumnsWithJoins]
        
        __fields__ = {
            "CONTENT_URI": Uri
            }
    
    class DataColumns:
    
        MIMETYPE = "mimetype"
    
    class DataColumnsWithJoins:
    
        __interfaces__ = [ContactsContract.DataColumns]
    
    class DisplayNameSources:
    
        EMAIL = 10
        NICKNAME = 35
        ORGANIZATION = 30
        PHONE = 20
        STRUCTURED_NAME = 40
        STRUCTURED_PHONETIC_NAME = 37
        UNDEFINED = 0


class MediaStore(Object):

    ACTION_IMAGE_CAPTURE = "android.media.action.IMAGE_CAPTURE"
    ACTION_IMAGE_CAPTURE_SECURE = "android.media.action.IMAGE_CAPTURE_SECURE"
    EXTRA_OUTPUT = "output"
    
    class Audio(Object):
    
        pass
    
    class Files(Object):
    
        pass
    
    class Images(Object):
    
        class Media(Object):
        
            CONTENT_TYPE = "vnd.android.cursor.dir/image"
            DEFAULT_SORT_ORDER = "bucket_display_name"
            
            __fields__ = {
                "EXTERNAL_CONTENT_URI": Uri,
                "INTERNAL_CONTENT_URI": Uri
                }
            
            def __init__(self):
                pass
            
            @final
            @static
            @args(Bitmap, [ContentResolver, Uri])
            def getBitmap(cr, url):
                pass
            
            @static
            @args(Uri, [String])
            def getContentUri(volumeName):
                pass
            
            @final
            @static
            @args(String, [ContentResolver, String, String, String])
            def insertImage(cr, imagePath, name, description):
                pass
            
            @final
            @static
            @args(String, [ContentResolver, Bitmap, String, String])
            def insertImage(cr, source, title, description):
                pass
            
            @final
            @static
            @args(Cursor, [ContentResolver, Uri, [String], String, String])
            def query(cr, uri, projection, where, orderBy):
                pass
            
            @final
            @static
            @args(Cursor, [ContentResolver, Uri, [String], String, [String], String])
            def query(cr, uri, projection, selection, selectionArgs, orderBy):
                pass
            
            @final
            @static
            @args(Cursor, [ContentResolver, Uri, [String]])
            def query(cr, uri, projection):
                pass
    
    class MediaColumns(Object):
    
        pass
    
    class Video(Object):
    
        pass
    
    def __init__(self):
        pass
    
    @args(Uri, [])
    def getMediaScannerUri(self):
        pass
    
    @args(String, [Context])
    def getVersion(self, context):
        pass


class Telephony(Object):

    class BaseMmsColumns:
    
        CONTENT_CLASS = "ct_cls"
        CONTENT_LOCATION = "ct_l"
        CONTENT_TYPE = "ct_t"
        CREATOR = "creator"
        DATE = "date"
        DATE_SENT = "date_sent"
        DELIVERY_REPORT = "d_rpt"
        DELIVERY_TIME = "d_tm"
        EXPIRY = "exp"
        LOCKED = "locked"
        MESSAGE_BOX = "msg_box"
        MESSAGE_BOX_ALL = 0
        MESSAGE_BOX_DRAFTS = 3
        MESSAGE_BOX_FAILED = 5
        MESSAGE_BOX_INBOX = 1
        MESSAGE_BOX_OUTBOX = 4
        MESSAGE_BOX_SENT = 2
        MESSAGE_CLASS = "m_cls"
        MESSAGE_ID = "m_id"
        MESSAGE_SIZE = "m_size"
        MESSAGE_TYPE = "m_type"
        MMS_VERSION = "v"
        PRIORITY = "pri"
        READ = "read"
        READ_REPORT = "rr"
        READ_STATUS = "read_status"
        REPORT_ALLOWED = "rpt_a"
        RESPONSE_STATUS = "resp_st"
        RESPONSE_TEXT = "resp_txt"
        RETRIEVE_STATUS = "retr_st"
        RETRIEVE_TEXT = "retr_txt"
        RETRIEVE_TEXT_CHARSET = "retr_txt_cs"
        SEEN = "seen"
        STATUS = "st"
        SUBJECT = "sub"
        SUBJECT_CHARSET = "sub_cs"
        SUBSCRIPTION_ID = "sub_id"
        TEXT_ONLY = "thread_id"
        THREAD_ID = "tr_id"
        TRANSACTION_ID = "tr_id"
    
    class CanonicalAddressesColumns:
    
        ADDRESS = "address"
    
    class Carriers(Object):

        APN = "apn"
        AUTH_TYPE = "authtype"
        BEARER = "bearer"
        CARRIER_ENABLED = "carrier_enabled"
        CURRENT = "current"
        DEFAULT_SORT_ORDER = "name ASC"
        MCC = "mcc"
        MMSC = "mmsc"
        MMSPORT = "mmsport"
        MMSPROXY = "mmsproxy"
        MNC = "mnc"
        MVNO_MATCH_DATA = "mvno_match_data"
        MVNO_TYPE = "mvno_type"
        NAME = "name"
        NUMERIC = "numeric"
        PASSWORD = "password"
        PORT = "port"
        PROTOCOL = "protocol"
        PROXY = "proxy"
        ROAMING_PROTOCOL = "roaming_protocol"
        SERVER = "server"
        SUBSCRIPTION_ID = "sub_id"
        TYPE = "type"
        USER = "user"
        
        __fields__ = {
            "CONTENT_URI": Uri
            }
    
    class Mms(Object):
    
        __interfaces__ = [Telephony.BaseMmsColumns]
        __fields__ = {
            "CONTENT_URI": Uri,
            "REPORT_REQUEST_URI": Uri,
            "REPORT_STATUS_URI": Uri
            }
        
        DEFAULT_SORT_ORDER = "date DESC"
        
        class Addr(Object):
        
            __interfaces__ = [BaseColumns]
            
            ADDRESS = "address"
            CHARSET = "charset"
            CONTACT_ID = "contact_id"
            MSG_ID = "msg_id"
            TYPE = "type"
        
        class Draft(Object):
        
            __interfaces__ = [Telephony.BaseMmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
        
        class Inbox(Object):
        
            __interfaces__ = [Telephony.BaseMmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
        
        class Intents(Object):
        
            CONTENT_CHANGED_ACTION =  "android.intent.action.CONTENT_CHANGED"
            DELETED_CONTENTS = "deleted_contents"
        
        class Outbox(Object):
        
            __interfaces__ = [Telephony.BaseMmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
        
        class Part(Object):
        
            __interfaces__ = [BaseColumns]
            
            CHARSET = "chset"
            CONTENT_DISPOSITION = "cd"
            CONTENT_ID = "cid"
            CONTENT_LOCATION = "cl"
            CONTENT_TYPE = "ct"
            CT_START = "ctt_s"
            CT_TYPE = "ctt_t"
            FILENAME = "fn"
            MSG_ID = "mid"
            NAME = "name"
            SEQ = "seq"
            TEXT = "text"
            _DATA = "_data"
        
        class Rate(Object):
        
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            SENT_TIME = "sent_time"
        
        class Sent(Object):
        
            __interfaces__ = [Telephony.BaseMmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
    
    class MmsSms(Object):
    
        __interfaces__ = [BaseColumns]
        __fields__ = {
            "CONTENT_CONVERSATIONS_URI": Uri,
            "CONTENT_DRAFT_URI": Uri,
            "CONTENT_FILTER_BYPHONE_URI": Uri,
            "CONTENT_LOCKED_URI": Uri,
            "CONTENT_UNDELIVERED_URI": Uri,
            "CONTENT_URI": Uri,
            "SEARCH_URI": Uri
            }
        
        ERR_TYPE_GENERIC = 1
        ERR_TYPE_GENERIC_PERMANENT = 10
        ERR_TYPE_MMS_PROTO_PERMANENT = 12
        ERR_TYPE_MMS_PROTO_TRANSIENT = 3
        ERR_TYPE_SMS_PROTO_PERMANENT = 11
        ERR_TYPE_SMS_PROTO_TRANSIENT = 2
        ERR_TYPE_TRANSPORT_FAILURE = 4
        MMS_PROTO = 1
        NO_ERROR = 0
        SMS_PROTO = 0
        TYPE_DISCRIMINATOR_COLUMN = "transport_type"
        
        class PendingMessages(Object):
        
            __interfaces__ = [BaseColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DUE_TIME = "due_time"
            ERROR_CODE = "err_code"
            ERROR_TYPE = "err_type"
            LAST_TRY = "last_try"
            MSG_ID = "msg_id"
            MSG_TYPE = "msg_type"
            PROTO_TYPE = "proto_type"
            RETRY_INDEX = "retry_index"
            SUBSCRIPTION_ID = "pending_sub_id"
    
    class Sms(Object):
    
        __interfaces__ = [BaseColumns, Telephony.TextBasedSmsColumns]
        __fields__ = {
            "CONTENT_URI": Uri
            }
        
        DEFAULT_SORT_ORDER = "date DESC"
        
        class Conversations(Object):
        
            __interfaces__ = [BaseColumns, Telephony.TextBasedSmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
            MESSAGE_COUNT = "msg_count"
            SNIPPET = "snippet"
        
        class Draft(Object):
        
            __interfaces__ = [BaseColumns, Telephony.TextBasedSmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
        
        class Inbox(Object):
        
            __interfaces__ = [BaseColumns, Telephony.TextBasedSmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
        
        class Intents(Object):
        
            ACTION_CHANGE_DEFAULT = "android.provider.Telephony.ACTION_CHANGE_DEFAULT"
            DATA_SMS_RECEIVED_ACTION = "android.intent.action.DATA_SMS_RECEIVED"
            EXTRA_PACKAGE_NAME = "package"
            RESULT_SMS_DUPLICATED = 5
            RESULT_SMS_GENERIC_ERROR = 2
            RESULT_SMS_HANDLED = 1
            RESULT_SMS_OUT_OF_MEMORY = 3
            RESULT_SMS_UNSUPPORTED = 4
            SIM_FULL_ACTION = "android.provider.Telephony.SIM_FULL"
            SMS_CB_RECEIVED_ACTION = "android.provider.Telephony.SMS_CB_RECEIVED"
            SMS_DELIVER_ACTION = "android.provider.Telephony.SMS_DELIVER"
            SMS_EMERGENCY_CB_RECEIVED_ACTION = "android.provider.Telephony.SMS_EMERGENCY_CB_RECEIVED"
            SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED"
            SMS_REJECTED_ACTION = "android.provider.Telephony.SMS_REJECTED"
            SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED_ACTION = "android.provider.Telephony.SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED"
            WAP_PUSH_DELIVER_ACTION = "android.provider.Telephony.WAP_PUSH_DELIVER"
            WAP_PUSH_RECEIVED_ACTION = "android.provider.Telephony.WAP_PUSH_RECEIVED"
            
            @static
            @args([SmsMessage], [Intent])
            def getMessagesFromIntent(intent):
                pass
        
        class Outbox(Object):
        
            __interfaces__ = [BaseColumns, Telephony.TextBasedSmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
        
        class Sent(Object):
        
            __interfaces__ = [BaseColumns, Telephony.TextBasedSmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
        
        @static
        @args(String, [Context])
        def getDefaultSmsPackage(context):
            pass
    
    class TextBasedSmsColumns:
    
        ADDRESS = "address"
        BODY = "body"
        CREATOR = "creator"
        DATE = "date"
        DATE_SENT = "date_sent"
        ERROR_CODE = "error_code"
        LOCKED = "locked"
        MESSAGE_TYPE_ALL = 0
        MESSAGE_TYPE_DRAFT = 3
        MESSAGE_TYPE_FAILED = 5
        MESSAGE_TYPE_INBOX = 1
        MESSAGE_TYPE_OUTBOX = 4
        MESSAGE_TYPE_QUEUED = 6
        MESSAGE_TYPE_SENT = 2
        PERSON = "person"
        PROTOCOL = "protocol"
        READ = "read"
        REPLY_PATH_PRESENT = "reply_path_present"
        SEEN = "seen"
        SERVICE_CENTER = "service_center"
        STATUS = "status"
        STATUS_COMPLETE = 0
        STATUS_FAILED = 64
        STATUS_NONE = -1
        STATUS_PENDING = 32
        SUBJECT = "subject"
        SUBSCRIPTION_ID = "sub_id"
        THREAD_ID = "thread_id"
        TYPE = "type"
    
    class Threads(Object):
    
        __interfaces__ = [Telephony.ThreadsColumns]
        __fields__ = {
            "CONTENT_URI": Uri,
            "OBSOLETE_THREADS_URI": Uri
            }
        
        BROADCAST_THREAD = 1
        COMMON_THREAD = 0
        
        @static
        @args(long, [Context, String])
        def getOrCreateThreadId(context, recipient):
            pass
        
        @static
        @args(long, [Context, Set(String)])
        def getOrCreateThreadId(context, recipients):
            pass
    
    class ThreadsColumns:
    
        __interfaces__ = [BaseColumns]
        
        ARCHIVED = "archived"
        DATE = "date"
        ERROR = "error"
        HAS_ATTACHMENT = "has_attachment"
        MESSAGE_COUNT = "message_count"
        READ = "read"
        RECIPIENT_IDS = "recipient_ids"
        SNIPPET = "snippet"
        SNIPPET_CHARSET = "snippet_cs"
        TYPE = "type"
