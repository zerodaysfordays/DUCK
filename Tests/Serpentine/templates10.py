__package__ = "com.example.templates10"

from java.util import List, Object, String
from android.os import Bundle
from Common.helper import TestActivity

class TemplatesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l = MyTemplate("Hello world!")
        s = l.get(0)
        
        self.showResult("Hello world!", s)


# Define a template class that will be specialised depending on the types of
# the arguments passed to it.

class MyTemplate(Object):

    __replace__ = [P, Q]
    
    @args(void, [Q])
    def __init__(self, value):
    
        Object.__init__(self)
        self.values = [value]
    
    @args(void, [P(Q)])
    def __init__(self, values):
    
        Object.__init__(self)
        self.values = values
    
    @args(Q, [int])
    def get(self, index):
    
        return self.values[index]
    
    @args(void, [Q])
    def append(self, value):
    
        self.values.add(value)
