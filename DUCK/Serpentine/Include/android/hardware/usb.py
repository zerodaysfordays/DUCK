# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.hardware.usb"

from java.lang import Object, String
from java.util import HashMap
from android.app import PendingIntent
from android.os import Parcelable, ParcelFileDescriptor

class UsbAccessory(Object):

    __interfaces__ = [Parcelable]
    __fields__ = {"CREATOR": Parcelable.Creator(UsbAccessory)}
    
    @args(String, [])
    def getDescription(self):
        pass
    
    @args(String, [])
    def getManufacturer(self):
        pass
    
    @args(String, [])
    def getModel(self):
        pass
    
    @args(String, [])
    def getSerial(self):
        pass
    
    @args(String, [])
    def getUri(self):
        pass
    
    @args(String, [])
    def getVersion(self):
        pass


class UsbConfiguration(Object):

    __interfaces__ = [Parcelable]
    __fields__ = {"CREATOR": Parcelable.Creator(UsbConfiguration)}
    
    @args(int, [])
    def getId(self):
        pass
    
    @args(UsbInterface, [int])
    def getInterface(self, index):
        pass
    
    @args(int, [])
    def getInterfaceCount(self):
        pass
    
    @args(int, [])
    def getMaxPower(self):
        pass
    
    @args(String, [])
    def getName(self):
        pass
    
    @args(bool, [])
    def isRemoteWakeup(self):
        pass
    
    @args(bool, [])
    def isSelfPowered(self):
        pass


class UsbConstants(Object):

    USB_CLASS_APP_SPEC            = 0xfe
    USB_CLASS_AUDIO               = 0x01
    USB_CLASS_CDC_DATA            = 0x0a
    USB_CLASS_COMM                = 0x02
    USB_CLASS_CONTENT_SEC         = 0x0d
    USB_CLASS_CSCID               = 0x0b
    USB_CLASS_HID                 = 0x03
    USB_CLASS_HUB                 = 0x09
    USB_CLASS_MASS_STORAGE        = 0x08
    USB_CLASS_MISC                = 0xef
    USB_CLASS_PER_INTERFACE       = 0x00
    USB_CLASS_PHYSICA             = 0x05
    USB_CLASS_PRINTER             = 0x07
    USB_CLASS_STILL_IMAGE         = 0x06
    USB_CLASS_VENDOR_SPEC         = 0xff
    USB_CLASS_VIDEO               = 0x0e
    USB_CLASS_WIRELESS_CONTROLLER = 0xe0
    
    USB_DIR_IN  = 0x80
    USB_DIR_OUT = 0x00
    
    USB_ENDPOINT_DIR_MASK       = 0x80
    USB_ENDPOINT_NUMBER_MASK    = 0x0f
    USB_ENDPOINT_XFERTYPE_MASK  = 0x03
    USB_ENDPOINT_XFER_BULK      = 0x02
    USB_ENDPOINT_XFER_CONTROL   = 0x00
    USB_ENDPOINT_XFER_INT       = 0x03
    USB_ENDPOINT_XFER_ISOC      = 0x01
    
    USB_INTERFACE_SUBCLASS_BOOT = 0x01
    USB_SUBCLASS_VENDOR_SPEC    = 0xff
    
    USB_TYPE_CLASS    = 0x20
    USB_TYPE_MASK     = 0x60
    USB_TYPE_RESERVED = 0x60
    USB_TYPE_STANDARD = 0x00
    USB_TYPE_VENDOR   = 0x40
    
    def __init__(self):
        pass


class UsbDevice(Object):

    __interfaces__ = [Parcelable]
    __fields__ = {"CREATOR": Parcelable.Creator(UsbDevice)}
    
    @args(UsbConfiguration, [int])
    def getConfiguration(self, index):
        pass
    
    @args(int, [])
    def getConfigurationCount(self):
        pass
    
    @args(int, [])
    def getDeviceClass(self):
        pass
    
    @static
    @args(int, [String])
    def getDeviceId(name):
        pass
    
    @args(int, [])
    def getDeviceId(self):
        pass
    
    @args(String, [])
    def getDeviceName(self):
        pass
    
    @static
    @args(String, [int])
    def getDeviceName(id):
        pass
    
    @args(int, [])
    def getDeviceProtocol(self):
        pass
    
    @args(int, [])
    def getDeviceSubclass(self):
        pass
    
    @args(UsbInterface, [int])
    def getInterface(self, index):
        pass
    
    @args(int, [])
    def getInterfaceCount(self):
        pass
    
    @args(String, [])
    def getManufacturerName(self):
        pass
    
    @args(int, [])
    def getProductId(self):
        pass
    
    @args(String, [])
    def getProductName(self):
        pass
    
    @args(String, [])
    def getSerialNumber(self):
        pass
    
    @args(int, [])
    def getVendorId(self):
        pass
    
    @args(String, [])
    def getVersion(self):
        pass


class UsbDeviceConnection(Object):

    @args(int, [UsbEndpoint, [byte], int, int])
    def bulkTransfer(self, endpoint, buffer, length, timeout):
        pass
    
    @args(int, [UsbEndpoint, [byte], int, int, int])
    def bulkTransfer(self, endpoint, buffer, offset, length, timeout):
        pass
    
    @args(bool, [UsbInterface, bool])
    def claimInterface(self, interface, force):
        pass
    
    def close(self):
        pass
    
    @args(int, [int, int, int, int, [byte], int, int])
    def controlTransfer(self, requestType, request, value, index, buffer, length, timeout):
        pass
    
    @args(int, [int, int, int, int, [byte], int, int, int])
    def controlTransfer(self, requestType, request, value, index, buffer, offset, length, timeout):
        pass
    
    @args(int, [])
    def getFileDescriptor(self):
        pass
    
    @args([byte], [])
    def getRawDescriptors(self):
        pass
    
    @args(String, [])
    def getSerial(self):
        pass
    
    @args(bool, [UsbInterface])
    def releaseInterface(self, interface):
        pass
    
    @args(UsbRequest, [])
    def requestWait(self):
        pass
    
    @args(bool, [UsbConfiguration])
    def setConfiguration(self, configuration):
        pass
    
    @args(bool, [UsbInterface])
    def setInterface(self, interface):
        pass


class UsbEndpoint(Object):

    __interfaces__ = [Parcelable]
    __fields__ = {"CREATOR": Parcelable.Creator(UsbEndpoint)}
    
    @args(int, [])
    def getAddress(self):
        pass
    
    @args(int, [])
    def getAttributes(self):
        pass
    
    @args(int, [])
    def getDirection(self):
        pass
    
    @args(int, [])
    def getEndpointNumber(self):
        pass
    
    @args(int, [])
    def getInterval(self):
        pass
    
    @args(int, [])
    def getMaxPacketSize(self):
        pass
    
    @args(int, [])
    def getType(self):
        pass


class UsbInterface(Object):

    __interfaces__ = [Parcelable]
    __fields__ = {"CREATOR": Parcelable.Creator(UsbInterface)}
    
    @args(int, [])
    def getAlternateSetting(self):
        pass
    
    @args(UsbEndpoint, [int])
    def getEndpoint(self, index):
        pass
    
    @args(int, [])
    def getEndpointCount(self):
        pass
    
    @args(int, [])
    def getId(self):
        pass
    
    @args(int, [])
    def getInterfaceClass(self):
        pass
    
    @args(int, [])
    def getInterfaceProtocol(self):
        pass
    
    @args(int, [])
    def getInterfaceSubclass(self):
        pass
    
    @args(String, [])
    def getName(self):
        pass


class UsbManager(Object):

    ACTION_USB_ACCESSORY_ATTACHED = "android.hardware.usb.action.USB_ACCESSORY_ATTACHED"
    ACTION_USB_ACCESSORY_DETACHED = "android.hardware.usb.action.USB_ACCESSORY_DETACHED"
    ACTION_USB_DEVICE_ATTACHED = "android.hardware.usb.action.USB_DEVICE_ATTACHED"
    ACTION_USB_DEVICE_DETACHED = "android.hardware.usb.action.USB_DEVICE_DETACHED"
    EXTRA_ACCESSORY = "accessory"
    EXTRA_DEVICE = "device"
    EXTRA_PERMISSION_GRANTED = "permission"
    
    @args([UsbAccessory], [])
    def getAccessoryList(self):
        pass
    
    @args(HashMap(String, UsbDevice), [])
    def getDeviceList(self):
        pass
    
    @args(bool, [UsbDevice])
    def hasPermission(self, device):
        pass
    
    @args(bool, [UsbAccessory])
    def hasPermission(self, accessory):
        pass
    
    @args(ParcelFileDescriptor, [UsbAccessory])
    def openAccessory(self, accessory):
        pass
    
    @args(UsbDeviceConnection, [UsbDevice])
    def openDevice(self, device):
        pass
    
    @args(void, [UsbAccessory, PendingIntent])
    def requestPermission(self, accessory, pendingIntent):
        pass
    
    @args(void, [UsbDevice, PendingIntent])
    def requestPermission(self, device, pendingIntent):
        pass


class UsbRequest(Object):

    def __init__(self):
        pass
    
    @args(bool, [])
    def cancel(self):
        pass
    
    def close(self):
        pass
    
    @args(Object, [])
    def getClientData(self):
        pass
    
    @args(UsbEndpoint, [])
    def getEndpoint(self):
        pass
    
    @args(bool, [UsbDeviceConnection, UsbEndpoint])
    def initialize(self, connection, endpoint):
        pass
    
    @args(bool, [ByteBuffer, int])
    def queue(self, buffer, length):
        pass
    
    @args(void, [Object])
    def setClientData(self, data):
        pass
