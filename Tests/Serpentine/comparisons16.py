__package__ = "com.example.comparisons16"

import android.os

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        
        f = False
        t = True
        
        if f and t:
            s = "False and True -> True"
        else:
            s = "False and True -> False"
        
        s += "\n"
        
        if f or t:
            s += "False or True -> True"
        else:
            s += "False or True -> False"
        
        expected = "False and True -> False\nFalse or True -> True"
        actual = s
        self.showResult(expected, actual)
