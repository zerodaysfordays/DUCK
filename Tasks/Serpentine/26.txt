Handle null characters in strings
=================================

Null characters need to be specially encoded in DEX files, otherwise the DEX
file checker will be confused about the length of any strings containing them.
