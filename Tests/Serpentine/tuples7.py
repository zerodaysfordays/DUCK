__package__ = "com.example.tuples7"

from Common.helper import TestActivity

class TuplesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l1 = range(5)
        l2 = range(0, 10, 2)
        s = ""
        
        for i, j in zip(l1, l2):
            s += str(i) + " " + str(j) + "\n"
        
        expected = "0 0\n1 2\n2 4\n3 6\n4 8\n"
        self.showResult(expected, s, True)
