Remove low-level resource class building from the compiler
==========================================================

Either construct resource classes (if required) using AST nodes or intermediate
source files. Alternatively, use magic functions to produce the constants that
refer to resources.

As of commit 32705b1d887b we currently generate intermediate source code for
resources.
