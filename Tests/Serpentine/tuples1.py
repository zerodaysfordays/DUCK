__package__ = "com.example.tuples1"

from Common.helper import TestActivity

class TuplesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = (1, 2, 3)
        s = ""
        
        for i in a:
            s += str(i) + " "
        
        expected = "1 2 3 "
        self.showResult(expected, s)
