__package__ = "com.example.maps10"

from android.os import Bundle

from Common.helper import TestActivity

class MapsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = {"abc": 1}
        
        s = str(a) + "\n"
        
        try:
            b = a["def"]
            s += str(b)
        except KeyError:
            s += "No entry"
        
        self.showResult("{abc=1}\nNo entry", s)
