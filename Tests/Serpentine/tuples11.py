__package__ = "com.example.tuples11"

from java.lang import String
from java.util import List
from Common.helper import TestActivity

class TuplesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a, b = self.getValues()
        s = str(a) + " " + str(b)
        
        expected = "1 2"
        self.showResult(expected, s)
    
    @args(List(int), [])
    def getValues(self):
    
        return 1, 2
