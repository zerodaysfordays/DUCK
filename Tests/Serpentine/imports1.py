__package__ = "com.example.imports1"

import Common.helper

class ImportsActivity(Common.helper.TestActivity):

    def __init__(self):
        Common.helper.TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        Common.helper.TestActivity.onCreate(self, bundle)
        
        # If the example compiles then the test passed.
        self.showResult("", "")
