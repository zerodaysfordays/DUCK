# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.text"

from java.lang import Appendable, CharSequence, Object

class ClipboardManager(Object):

    @api(1,11)
    def __init__(self):
        pass
    
    @api(1,11)
    @abstract
    @args(CharSequence, [])
    def getText(self):
        pass
    
    @api(1,11)
    @abstract
    @args(bool, [])
    def hasText(self):
        pass
    
    @api(1,11)
    @abstract
    @args(void, [CharSequence])
    def setText(self, text):
        pass


class Editable:

    __interfaces__ = [CharSequence, GetChars, Spannable, Appendable]
    
    class Factory(Object):
    
        def __init__(self):
            pass
        
        @static
        @args(Editable.Factory, [])
        def getInstance():
            pass
        
        @args(Editable, [CharSequence])
        def newEditable(self, sequence):
            pass
    
    @args(Editable, [CharSequence])
    def append(self, text):
        pass
    
    @args(Editable, [CharSequence, int, int])
    def append(self, text, start, end):
        pass
    
    @args(Editable, [char])
    def append(self, text):
        pass
    
    @args(void, [])
    def clear(self):
        pass
    
    @args(void, [])
    def clearSpans(self):
        pass
    
    @args(Editable, [int, int])
    def delete(self, start, end):
        pass
    
    @args(Editable, [int, CharSequence, int, int])
    def insert(self, where, text, start, end):
        pass
    
    @args(Editable, [int, CharSequence])
    def insert(self, where, text):
        pass
    
    @args(Editable, [int, int, CharSequence, int, int])
    def replace(self, start, end, source, sourceStart, sourceEnd):
        pass
    
    @args(Editable, [int, int, CharSequence])
    def replace(self, start, end, source):
        pass


class GetChars:

    __interfaces__ = [CharSequence]
    
    @args(void, [int, int, [char], int])
    def getChars(self, start, end, dest, destoff):
        pass


class InputType:

    TYPE_CLASS_DATETIME = 4
    TYPE_CLASS_NUMBER = 2
    TYPE_CLASS_PHONE = 3
    TYPE_CLASS_TEXT = 1
    TYPE_DATETIME_VARIATION_DATE = 0x10
    TYPE_DATETIME_VARIATION_NORMAL = 0x00
    TYPE_DATETIME_VARIATION_TIME = 0x20
    TYPE_MASK_CLASS     = 0x0000000f
    TYPE_MASK_FLAGS     = 0x00fff000
    TYPE_MASK_VARIATION = 0x00000ff0
    TYPE_NULL = 0
    TYPE_NUMBER_FLAG_DECIMAL       = 0x2000
    TYPE_NUMBER_FLAG_SIGNED        = 0x1000
    TYPE_NUMBER_VARIATION_NORMAL   = 0x0000
    TYPE_NUMBER_VARIATION_PASSWORD = 0x0010
    TYPE_TEXT_FLAG_AUTO_COMPLETE  = 0x10000
    TYPE_TEXT_FLAG_AUTO_CORRECT   = 0x08000
    TYPE_TEXT_FLAG_CAP_CHARACTERS = 0x01000
    TYPE_TEXT_FLAG_CAP_SENTENCES  = 0x04000
    TYPE_TEXT_FLAG_CAP_WORDS      = 0x02000
    TYPE_TEXT_FLAG_IME_MULTI_LINE = 0x40000
    TYPE_TEXT_FLAG_MULTI_LINE     = 0x20000
    TYPE_TEXT_FLAG_NO_SUGGESTIONS = 0x80000
    TYPE_TEXT_VARIATION_EMAIL_ADDRESS     = 0x20
    TYPE_TEXT_VARIATION_EMAIL_SUBJECT     = 0x30
    TYPE_TEXT_VARIATION_FILTER            = 0xb0
    TYPE_TEXT_VARIATION_LONG_MESSAGE      = 0x50
    TYPE_TEXT_VARIATION_NORMAL            = 0x00
    TYPE_TEXT_VARIATION_PASSWORD          = 0x80
    TYPE_TEXT_VARIATION_PERSON_NAME       = 0x60
    TYPE_TEXT_VARIATION_PHONETIC          = 0xc0
    TYPE_TEXT_VARIATION_POSTAL_ADDRESS    = 0x70
    TYPE_TEXT_VARIATION_SHORT_MESSAGE     = 0x40
    TYPE_TEXT_VARIATION_URI               = 0x10
    TYPE_TEXT_VARIATION_VISIBLE_PASSWORD  = 0x90
    TYPE_TEXT_VARIATION_WEB_EDIT_TEXT     = 0xa0
    TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS = 0xd0
    TYPE_TEXT_VARIATION_WEB_PASSWORD      = 0xe0


class NoCopySpan:

    pass


class Spannable:

    __interfaces__ = [Spanned]
    
    class Factory(Object):
    
        def __init__(self):
            pass
        
        @static
        @args(Spannable.Factory, [])
        def getInstance():
            pass
        
        @args(Spannable, [CharSequence])
        def newSpannable(self, source):
            pass
    
    @args(void, [Object])
    def removeSpan(self, what):
        pass
    
    @args(void, [Object, int, int, int])
    def setSpan(self, what, start, end, flags):
        pass


class Spanned:

    __interfaces__ = [CharSequence]
    __parameters__ = [T]
    
    SPAN_COMPOSING           = 0x100
    SPAN_EXCLUSIVE_EXCLUSIVE = 0x021
    SPAN_EXCLUSIVE_INCLUSIVE = 0x022
    SPAN_INCLUSIVE_EXCLUSIVE = 0x011
    SPAN_INCLUSIVE_INCLUSIVE = 0x012
    SPAN_INTERMEDIATE        = 0x200
    SPAN_MARK_MARK           = 0x011
    SPAN_MARK_POINT          = 0x012
    SPAN_PARAGRAPH           = 0x033
    SPAN_POINT_MARK          = 0x021
    SPAN_POINT_MARK_MASK     = 0x033
    SPAN_POINT_POINT         = 0x022
    SPAN_PRIORITY            = 0x00ff0000
    SPAN_PRIORITY_SHIFT      = 0x010
    SPAN_USER                = 0xff000000
    SPAN_USER_SHIFT          = 0x018
    
    @args(int, [Object])
    def getSpanEnd(self, tag):
        pass
    
    @args(int, [Object])
    def getSpanFlags(self, tag):
        pass
    
    @args(int, [Object])
    def getSpanStart(self, tag):
        pass
    
    @args([T], [int, int, Class(T)])
    def getSpans(self, start, end, type):
        pass
    
    @args(int, [int, int, Class])
    def nextSpanTransition(self, start, limit, type):
        pass


class TextWatcher:

    __interfaces__ = [NoCopySpan]
    
    @args(void, [Editable])
    def afterTextChanged(self, editable):
        pass
    
    @args(void, [CharSequence, int, int, int])
    def beforeTextChanged(self, charSequence, start, count, after):
        pass
    
    @args(void, [CharSequence, int, int, int])
    def onTextChanged(self, charSequence, start, before, count):
        pass
