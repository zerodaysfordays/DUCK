__package__ = "com.example.dict1"

from Common.helper import TestActivity

class DictActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        keys = ["Aa", "Bb", "Cc"]
        values = [1, 2, 3]
        d = dict(keys, values)
        
        s = ""
        for key in keys:
            s += key + ": " + str(d[key]) + "\n"
        
        expected = "Aa: 1\nBb: 2\nCc: 3\n"
        self.showResult(expected, s, True)
