__package__ = "com.example.testresources4"

from java.lang import String
from android.app import Activity
import android.os
from android.widget import TextView

import app_resources

class TestResourcesActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        res = self.getResources()
        
        t = res.getBoolean(app_resources.R.bool.true_value)
        f = res.getBoolean(app_resources.R.bool.false_value)
        
        actual = str(t) + " " + str(f) + "\n"
        
        n0 = res.getInteger(app_resources.R.integer.number0)
        n1 = res.getInteger(app_resources.R.integer.number1)
        n2 = res.getInteger(app_resources.R.integer.number2)
        
        actual += str(n0) + " " + str(n1) + " " + str(n2) + "\n"
        
        red = res.getColor(app_resources.R.color.red444)
        green = res.getColor(app_resources.R.color.green888)
        blue = res.getColor(app_resources.R.color.blue4444)
        cyan = res.getColor(app_resources.R.color.cyan8888)
        
        actual += str(red) + " " + str(green) + " " + str(blue) + " " + \
                  str(cyan) + "\n"
        
        expected = "true false\n0 -1 123\n3840 65280 61455 -16711681\n"
        
        self.showResult(expected, actual)
    
    @args(void, [String, String])
    def showResult(self, expected, actual):
        
        view = TextView(self)
        
        if actual == expected:
            view.setText("Test passed!")
        else:
            view.setText("Test failed:\nExpected:\n" + expected + "\nObtained:\n" + actual)
            view.setBackgroundColor(0xff400000)
        
        self.setContentView(view)
