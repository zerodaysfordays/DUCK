__package__ = "com.example.for11"

from java.lang import Integer
import android.os
from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = array(int, 1)
        s = ""
        
        for a[0] in array([1, 2, 3, 4, 5]):
            s += str(a[0]) + " "
        
        s += str(a[0])
        
        expected = "1 2 3 4 5 5"
        self.showResult(expected, s)
