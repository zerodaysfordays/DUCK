__package__ = "com.example.comparisons37"

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = str(0 == 0 == 0) + " " + \
            str(0 == 0 == 1) + " " + \
            str(0 == 1 == 0) + " " + \
            str(0 == 1 == 1) + " " + \
            str(1 == 0 == 0) + " " + \
            str(1 == 0 == 1) + " " + \
            str(1 == 1 == 0) + " " + \
            str(1 == 1 == 1)
        
        expected = "true false false false false false false true"
        self.showResult(expected, s)
