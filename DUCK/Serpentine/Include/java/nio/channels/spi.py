# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.nio.channels.spi"

from java.lang import Object
from java.nio.channels import Channel, InterruptibleChannel, SelectableChannel,\
    SelectionKey

class AbstractInterruptibleChannel(Object):

    __interfaces__ = [Channel, InterruptibleChannel]
    
    @protected
    def __init__(self):
        pass
    
    @final
    def close(self):
        pass
    
    @synchronized
    @final
    @args(bool, [])
    def isOpen(self):
        pass
    
    @final
    def begin(self):
        pass
    
    @final
    @args(void, [bool])
    def end(self, success):
        pass
    
    def implCloseChannel(self):
        pass


class AbstractSelectableChannel(SelectableChannel):

    @protected
    def __init__(self):
        pass
    
    @final
    @args(Object, [])
    def blockingLock(self):
        pass
    
    @final
    @args(SelectableChannel, [bool])
    def configureBlocking(self, blockingMode):
        pass
    
    @final
    @args(bool, [])
    def isBlocking(self):
        pass
    
    @synchronized
    @final
    @args(bool, [])
    def isRegistered(self):
        pass
    
    @synchronized
    @final
    @args(SelectionKey, [Selector])
    def keyFor(self, selector):
        pass
    
    @final
    @args(SelectorProvider, [])
    def provider(self):
        pass
    
    @final
    @args(SelectionKey, [Selector, int, Object])
    def register(self, selector, interestSet, attachment):
        pass
    
    @protected
    @synchronized
    @final
    def implCloseChannel(self):
        pass
    
    @protected
    @abstract
    def implCloseSelectableChannel(self):
        pass
    
    @protected
    @abstract
    @args(void, [bool])
    def implConfigureBlocking(self, blocking):
        pass


class SelectorProvider(Object):

    @protected
    def __init__(self):
        pass
    
    @args(Channel, [])
    def inheritedChannel(self):
        pass
    
    @abstract
    @args(DatagramChannel, [])
    def openDatagramChannel(self):
        pass
    
    @abstract
    @args(Pipe, [])
    def openPipe(self):
        pass
    
    @abstract
    @args(AbstractSelector, [])
    def openSelector(self):
        pass
    
    @abstract
    @args(ServerSocketChannel, [])
    def openServerSocketChannel(self):
        pass
    
    @abstract
    @args(SocketChannel, [])
    def openSocketChannel(self):
        pass
    
    @synchronized
    @static
    @args(SelectorProvider, [])
    def provider(self):
        pass
