Remove unused and unnecessary methods
=====================================

Remove unused library code methods that aren't needed to provide an interface.
These will be virtual methods that are present in any of the interface classes
of a library class.

Determining that a method is an interface method may be something that can be
done when the method is compiled by finding all the interfaces of the class and
reserving methods that match those provided by the interfaces.
