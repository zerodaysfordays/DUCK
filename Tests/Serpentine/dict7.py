__package__ = "com.example.dict7"

from serpentine.collections import Pair
from Common.helper import TestActivity

class DictActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        lists = [["Aa", "Alice"], ["Bb", "Bob"], ["Cc", "Carol"]]
        d = dict(lists)
        
        s = ""
        for l in lists:
            key = l[0]
            s += key + ": " + d[key] + "\n"
        
        expected = "Aa: Alice\nBb: Bob\nCc: Carol\n"
        self.showResult(expected, s, True)
