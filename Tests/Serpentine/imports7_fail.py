__package__ = "com.example.imports7"

from Common.helper import TestActivity

# This test should fail because Common.helper should not be imported as a side
# effect of importing the TestActivity class.

class ImportsActivity(TestActivity):

    def __init__(self):
        Common.helper.TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        Common.helper.TestActivity.onCreate(self, bundle)
        
        # If the example compiles then the test passed.
        self.showResult("", "")
