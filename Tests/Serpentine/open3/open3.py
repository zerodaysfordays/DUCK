__package__ = "com.example.open3"

from java.lang import String
from android.app import Activity
from android.widget import TextView

from android.os import Environment
from serpentine.builtins import Files

class OpenActivity(Activity):

    def __init__(self):
        Activity.__init__(self)
    
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        file = Files.createExternalFile(Environment.DIRECTORY_DOWNLOADS, None,
                                        "open3.txt", None, None)
        
        f = Files.open(file, "rw")
        f.writeUTF("Hello world!")
        f.close()
        
        view = TextView(self)
        view.setText("File " + file.getAbsolutePath() + " created.")
        
        self.setContentView(view)
