# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.database"

from java.io import Closeable
from java.lang import Object, String
from android.net import Uri
from android.os import Bundle, Handler

class CharArrayBuffer(Object):

    __fields__ = {"data": [char], "sizeCopied": int}
    
    @args(void, [int])
    def __init__(self, size):
        pass
    
    @args(void, [[char]])
    def __init__(self, buf):
        pass


class ContentObserver(Object):

    @args(void, [Handler])
    def __init__(self, handler):
        pass
    
    @args(bool, [])
    def deliverSelfNotifications(self):
        pass
    
    @final
    @args(void, [bool, Uri])
    def dispatchChange(self, selfChange, uri):
        pass
    
    @final
    @args(void, [bool])
    def dispatchChange(self, selfChange):
        pass
    
    @args(void, [bool, Uri])
    def onChange(self, selfChange, uri):
        pass
    
    @args(void, [bool])
    def onChange(self, selfChange):
        pass


class Cursor:

    __interfaces__ = [Closeable]
    
    FIELD_TYPE_BLOB = 4
    FIELD_TYPE_FLOAT = 2
    FIELD_TYPE_INTEGER = 1
    FIELD_TYPE_NULL = 0
    FIELD_TYPE_STRING = 3
    
    def close(self):
        pass
    
    @args(void, [int, CharArrayBuffer])
    def copyStringToBuffer(self, columnIndex, buffer):
        pass
    
    ### Deprecated in API level 16
    def deactivate(self):
        pass
    
    @args([byte], [int])
    def getBlob(self, columnIndex):
        pass
    
    @args(int, [])
    def getColumnCount(self):
        pass
    
    @args(int, [String])
    def getColumnIndex(self, columnName):
        pass
    
    @args(int, [String])
    def getColumnIndexOrThrow(self, columnName):
        pass
    
    @args(String, [int])
    def getColumnName(self, columnIndex):
        pass
    
    @args([String], [])
    def getColumnNames(self):
        pass
    
    @args(int, [])
    def getCount(self):
        pass
    
    @args(double, [int])
    def getDouble(self, columnIndex):
        pass
    
    @args(Bundle, [])
    def getExtras(self):
        pass
    
    @args(float, [int])
    def getFloat(self, columnIndex):
        pass
    
    @args(int, [int])
    def getInt(self, columnIndex):
        pass
    
    @args(long, [int])
    def getLong(self, columnIndex):
        pass
    
    @args(Uri, [])
    def getNotificationUri(self):
        pass
    
    @args(int, [])
    def getPosition(self):
        pass
    
    @args(short, [int])
    def getShort(self, columnIndex):
        pass
    
    @args(String, [int])
    def getString(self, columnIndex):
        pass
    
    @args(int, [int])
    def getType(self, columnIndex):
        pass
    
    @args(bool, [])
    def getWantsAllOnMoveCalls(self):
        pass
    
    @args(bool, [])
    def isAfterLast(self):
        pass
    
    @args(bool, [])
    def isBeforeFirst(self):
        pass
    
    @args(bool, [])
    def isClosed(self):
        pass
    
    @args(bool, [])
    def isFirst(self):
        pass
    
    @args(bool, [])
    def isLast(self):
        pass
    
    @args(bool, [])
    def isNull(self):
        pass
    
    @args(bool, [int])
    def move(self, offset):
        pass
    
    @args(bool, [])
    def moveToFirst(self):
        pass
    
    @args(bool, [])
    def moveToLast(self):
        pass
    
    @args(bool, [])
    def moveToNext(self):
        pass
    
    @args(bool, [int])
    def moveToPosition(self, position):
        pass
    
    @args(bool, [])
    def moveToPrevious(self):
        pass
    
    @args(bool, [ContentObserver])
    def registerContentObserver(self, observer):
        pass
    
    @args(bool, [DataSetObserver])
    def registerContentObserver(self, observer):
        pass
    
    ### Deprecated in API level 11
    def requery(self):
        pass
    
    @args(Bundle, [Bundle])
    def respond(self, extras):
        pass
    
    @args(void, [Bundle])
    def setExtras(self, extras):
        pass
    
    @args(void, [ContentResolver, Uri])
    def setNotificationUri(self, contentResolver, uri):
        pass
    
    @args(void, [ContentObserver])
    def unregisterContentObserver(self, observer):
        pass
    
    @args(void, [DataSetObserver])
    def unregisterDataSetObserver(self, observer):
        pass


class DataSetObserver(Object):

    def __init__(self):
        pass
    
    def onChanged(self):
        pass
    
    def onInvalidated(self):
        pass
