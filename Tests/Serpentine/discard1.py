__package__ = "com.example.discard"

from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class ArraysActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        """Docstrings should not be processed."""
        
        """Other strings should also not appear in the generated code."""
        
        Activity.onCreate(self, bundle)
        
        """This strings should also not appear."""
        
        view = TextView(self)
        view.setText("")
        self.setContentView(view)
