__package__ = "com.example.arrays17"

from java.util import String
from serpentine.arrays import Arrays
from Common.helper import TestActivity

class ArraysActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s1 = "Hello World!"
        a = s1.getBytes("ASCII")
        b = Arrays.slice(a, 0, 6)
        c = Arrays.slice(a, 6, 12)
        
        s2 = String(b, "ASCII") + String(c, "ASCII")
        expected = String("Hello World!".getBytes("ASCII"), "ASCII")
        
        self.showResult(expected, s2)
