__package__ = "com.example.iterators1"

from java.lang import Integer
from java.util import ArrayList
from android.app import Activity
import android.os
from android.widget import TextView

class IntegerList(ArrayList):

    __item_types__ = [Integer]
    
    def __init__(self):
        ArrayList.__init__(self)


class IteratorsActivity(Activity):

    __interfaces__ = []
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        numbers = IntegerList()
        numbers.add(Integer(68))
        numbers.add(Integer(85))
        numbers.add(Integer(67))
        numbers.add(Integer(75))
        
        view = TextView(self)
        iterator = numbers.iterator()
        
        s = ""
        while iterator.hasNext():
            v = iterator.next()
            s = s.concat(" ").concat(s.valueOf(v.intValue()))
        
        view.setText(s)
        self.setContentView(view)
