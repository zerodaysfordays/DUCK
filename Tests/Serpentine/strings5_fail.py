__package__ = "com.example.strings5"

from java.lang import String

from Common.helper import TestActivity

class StringsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = String("abc")
        t = String(s.subsequence(0, 3))
        
        self.showResult(s, s)
