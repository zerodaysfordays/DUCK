__package__ = "com.example.comparisons19"

import android.os

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        expected = ""
        s = ""
        
        s += str(False) + " "
        s += str(True) + "\n"
        
        expected += "false true\n"
        
        s += str(False and False) + " "
        s += str(False and True) + " "
        s += str(True and False) + " "
        s += str(True and True) + "\n"
        s += str(False and False and False) + " "
        s += str(False and False and True) + " "
        s += str(False and True and False) + " "
        s += str(False and True and True) + " "
        s += str(True and False and False) + " "
        s += str(True and False and True) + " "
        s += str(True and True and False) + " "
        s += str(True and True and True) + "\n"
        
        expected += ("false false false true\n"
                     "false false false false false false false true\n")
        
        s += str(False or False) + " "
        s += str(False or True) + " "
        s += str(True or False) + " "
        s += str(True or True) + "\n"
        s += str(False or False or False) + " "
        s += str(False or False or True) + " "
        s += str(False or True or False) + " "
        s += str(False or True or True) + " "
        s += str(True or False or False) + " "
        s += str(True or False or True) + " "
        s += str(True or True or False) + " "
        s += str(True or True or True) + "\n"
        
        expected += ("false true true true\n"
                     "false true true true true true true true\n")
        
        actual = s
        self.showResult(expected, actual)
