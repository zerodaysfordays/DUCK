Method prototype inference
==========================

If a method has the same number of arguments as one inherited from a base class
or interface, perhaps we could just apply the same prototype to the method
without having to declare it in the subclass.
