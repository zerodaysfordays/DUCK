"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# See https://source.android.com/devices/tech/dalvik/dalvik-bytecode.html and
# https://source.android.com/devices/tech/dalvik/instruction-formats.html for
# information about Dalvik bytecode.

class Instruction:

    notes = []
    
    def __repr__(self):
        return "%s %s %s" % (self.__class__.__name__,
            ",".join(map(repr, self.args)),
            "; " + " ".join(self.notes))
    
    def __str__(self):
        return "%s %s" % (self.__class__.__name__,
            ",".join(map(repr, self.args)))

class UnsignedInteger:

    def __init__(self, value, bits):
    
        self.value = value
        self.bits = bits
    
    def __add__(self, other):
        return self.value + other
    
    def __radd__(self, other):
        return other + self.value
    
    def __sub__(self, other):
        return self.value - other
    
    def __rsub__(self, other):
        return other - self.value
    
    def __lshift__(self, places):
        return self.value << places
    
    def __rshift__(self, places):
        return self.value >> places
    
    def __or__(self, other):
        return self.value | other
    
    def __ror__(self, other):
        return other | self.value
    
    def __trunc__(self):
        return self.value

class Constant(UnsignedInteger):

    """Represents signed and unsigned integer constants that, when written,
    will be stored in an unsigned integer using a given number of bits to
    represent the range of possible values. Internally, the value is stored as
    a signed integer.
    """
    def __init__(self, value, bits):
    
        UnsignedInteger.__init__(self, value, bits)
        
        if self.value > 0 and self.value & (1 << (self.bits - 1)):
            self.value -= (1 << self.bits)
        
        self.shift = 0
    
    def __repr__(self):
        return "#%i" % self.value

class Offset(Constant):

    def __repr__(self):
        if self.value >= 0:
            return "+%i (0x%x)" % (self.value, self.value)
        else:
            return "%i" % self.value

class Index(UnsignedInteger):

    def __init__(self, value, bits):
    
        self.value = value
        self.bits = bits
    
    def __repr__(self):
        return "@%i" % self.value

class Index8(Index):

    def __init__(self, value):
        Index.__init__(self, value, 8)

class Index16(Index):

    def __init__(self, value):
        Index.__init__(self, value, 16)

class Count(UnsignedInteger):

    def __repr__(self):
        return "<Count %i>" % self.value

class Count4(Count):

    def __init__(self, value):
        Count.__init__(self, value, 4)

class Count8(Count):

    def __init__(self, value):
        Count.__init__(self, value, 8)

class Register(UnsignedInteger):

    def __repr__(self):
        return "v%i" % self.value
    
    def __trunc__(self):
        mask = (1 << self.bits) - 1
        return self.value & mask

class Register4(Register):

    def __init__(self, value):
        Register.__init__(self, value, 4)

registers = {}

def v(n, bits):
    return registers.setdefault(n, Register(n, bits))


class format_10x(Instruction):

    size = 1
    
    def __init__(self):
    
        self.args = []
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # 00|op
        self.args = []
        return offset + 1
    
    def encode(self):
        return [self.opcode]

class format_12x(Instruction):

    size = 1
    
    def __init__(self, vA = 0, vB = 0):
    
        self.args = [vA, vB]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # B|A|op    op vA,vB
        vA = v((bytecodes[offset] >> 8) & 0x0f, 4)
        vB = v(bytecodes[offset] >> 12, 4)
        self.args = [vA, vB]
        return offset + 1
    
    def encode(self):
    
        vA, vB = map(int, self.args)
        return [(vB << 12) | (vA << 8) | self.opcode]

class format_11n(Instruction):

    size = 1
    
    def __init__(self, vA = 0, B = 0):
    
        self.args = [vA, B]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # B|A|op    op vA,#+B
        vA = v((bytecodes[offset] >> 8) & 0x0f, 4)
        B = Constant(bytecodes[offset] >> 12, 4)
        self.args = [vA, B]
        return offset + 1
    
    def encode(self):
    
        vA, B = map(int, self.args)
        return [(B << 12) | (vA << 8) | self.opcode]

class format_11x(Instruction):

    size = 1
    
    def __init__(self, vAA = 0):
    
        self.args = [vAA]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op     op vAA
        vAA = v(bytecodes[offset] >> 8, 8)
        self.args = [vAA]
        return offset + 1
    
    def encode(self):
    
        vAA, = map(int, self.args)
        return [(vAA << 8) | self.opcode]

class format_10t(Instruction):

    size = 1
    
    def __init__(self, AA = 0):
    
        self.args = [AA]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op     op +AA
        AA = Offset(bytecodes[offset] >> 8, 8)
        self.args = [AA]
        return offset + 1
    
    def encode(self):
    
        AA, = map(int, self.args)
        return [(AA << 8) | self.opcode]

class format_20t(Instruction):

    size = 2
    
    def __init__(self, AAAA = 0):
    
        self.args = [AAAA]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # 00|op     op +AAAA
        # AAAA
        AAAA = Offset(bytecodes[offset + 1], 16)
        self.args = [AAAA]
        return offset + 2
    
    def encode(self):
    
        AAAA, = map(int, self.args)
        return [self.opcode, AAAA]

class format_20bc(Instruction):

    size = 2
    
    def __init__(self, AA = 0, BBBB = 0):
    
        self.args = [AA, BBBB]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op     op AA,kind@BBBB
        # BBBB
        AA = Offset(bytecodes[offset] >> 8, 8)
        BBBB = Index(bytecodes[offset + 1], 16)
        self.args = [AA, BBBB]
        return offset + 2
    
    def encode(self):
    
        AA, BBBB = map(int, self.args)
        return [(AA << 8) | self.opcode, BBBB]

class format_22x(Instruction):

    size = 2
    
    def __init__(self, vAA = 0, vBBBB = 0):
    
        self.args = [vAA, vBBBB]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op     op vAA, vBBBB
        # BBBB
        vAA = v(bytecodes[offset] >> 8, 8)
        vBBBB = v(bytecodes[offset + 1], 16)
        self.args = [vAA, vBBBB]
        return offset + 2
    
    def encode(self):
    
        vAA, vBBBB = map(int, self.args)
        return [(vAA << 8) | self.opcode, vBBBB]

class format_21t(Instruction):

    size = 2
    
    def __init__(self, vAA = 0, BBBB = 0):
    
        self.args = [vAA, BBBB]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op     op vAA,+BBBB
        # BBBB
        vAA = v(bytecodes[offset] >> 8, 8)
        BBBB = Offset(bytecodes[offset + 1], 16)
        self.args = [vAA, BBBB]
        return offset + 2
    
    def encode(self):
    
        vAA, BBBB = map(int, self.args)
        return [(vAA << 8) | self.opcode, BBBB]

class format_21s(Instruction):

    size = 2
    
    def __init__(self, vAA = 0, BBBB = 0):
    
        self.args = [vAA, BBBB]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op     op vAA,#+BBBB
        # BBBB
        vAA = v(bytecodes[offset] >> 8, 8)
        BBBB = Constant(bytecodes[offset + 1], 16)
        self.args = [vAA, BBBB]
        return offset + 2
    
    def encode(self):
    
        AA, BBBB = map(int, self.args)
        return [(AA << 8) | self.opcode, BBBB]

class format_21h(Instruction):

    size = 2
    
    def __init__(self, vAA = 0, BBBB = 0):
    
        self.args = [vAA, BBBB]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op     op vAA,+BBBB0000
        # BBBB      op vAA,+BBBB000000000000
        vAA = v(bytecodes[offset] >> 8, 8)
        BBBB = Constant(bytecodes[offset + 1], 16)
        self.args = [vAA, BBBB]
        return offset + 2
    
    def encode(self):
    
        AA, BBBB = map(int, self.args)
        return [(AA << 8) | self.opcode, BBBB]

class format_21c(Instruction):

    size = 2
    
    def __init__(self, vAA = 0, BBBB = 0):
    
        self.args = [vAA, BBBB]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op     op vAA,type@BBBB
        # BBBB      op vAA,field@BBBB
        #           op vAA,string@BBBB
        vAA = v(bytecodes[offset] >> 8, 8)
        BBBB = Index(bytecodes[offset + 1], 16)
        self.args = [vAA, BBBB]
        return offset + 2
    
    def encode(self):
    
        AA, BBBB = map(int, self.args)
        return [(AA << 8) | self.opcode, BBBB]

class format_23x(Instruction):

    size = 2
    
    def __init__(self, vAA = 0, vBB = 0, vCC = 0):
    
        self.args = [vAA, vBB, vCC]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op     op vAA,vBB,vCC
        # CC|BB
        vAA = v(bytecodes[offset] >> 8, 8)
        vBB = v(bytecodes[offset + 1] & 0xff, 8)
        vCC = v(bytecodes[offset + 1] >> 8, 8)
        self.args = [vAA, vBB, vCC]
        return offset + 2
    
    def encode(self):
    
        vAA, vBB, vCC = map(int, self.args)
        return [(vAA << 8) | self.opcode, (vCC << 8) | vBB]

class format_22b(Instruction):

    size = 2
    
    def __init__(self, vAA = 0, vBB = 0, CC = 0):
    
        self.args = [vAA, vBB, CC]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op     op vAA,vBB,#+CC
        # CC|BB
        vAA = v(bytecodes[offset] >> 8, 8)
        vBB = v(bytecodes[offset + 1] & 0xff, 8)
        CC = Constant(bytecodes[offset + 1] >> 8, 8)
        self.args = [vAA, vBB, CC]
        return offset + 2
    
    def encode(self):
    
        vAA, vBB, CC = map(int, self.args)
        return [(vAA << 8) | self.opcode, (CC << 8) | vBB]

class format_22t(Instruction):

    size = 2
    
    def __init__(self, vA = 0, vB = 0, CCCC = 0):
    
        self.args = [vA, vB, CCCC]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # B|A|op    op vA,vB,+CCCC
        # CCCC
        vA = v((bytecodes[offset] >> 8) & 0x0f, 4)
        vB = v(bytecodes[offset] >> 12, 4)
        CCCC = Offset(bytecodes[offset + 1], 16)
        self.args = [vA, vB, CCCC]
        return offset + 2
    
    def encode(self):
    
        vA, vB, CCCC = map(int, self.args)
        return [(vB << 12) | (vA << 8) | self.opcode, CCCC]

class format_22s(Instruction):

    size = 2
    
    def __init__(self, vA = 0, vB = 0, CCCC = 0):
    
        self.args = [vA, vB, CCCC]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # B|A|op    op vA,vB,#+CCCC
        # CCCC
        vA = v((bytecodes[offset] >> 8) & 0x0f, 4)
        vB = v(bytecodes[offset] >> 12, 4)
        CCCC = Constant(bytecodes[offset + 1], 16)
        self.args = [vA, vB, CCCC]
        return offset + 2
    
    def encode(self):
    
        vA, vB, CCCC = map(int, self.args)
        return [(vB << 12) | (vA << 8) | self.opcode, CCCC]

class format_22c(Instruction):

    size = 2
    
    def __init__(self, vA = 0, vB = 0, CCCC = 0):
    
        self.args = [vA, vB, CCCC]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # B|A|op    op vA,vB,type@CCCC
        # CCCC      op vA,vB,field@CCCC
        vA = v((bytecodes[offset] >> 8) & 0x0f, 4)
        vB = v(bytecodes[offset] >> 12, 4)
        CCCC = Index(bytecodes[offset + 1], 16)
        self.args = [vA, vB, CCCC]
        return offset + 2
    
    def encode(self):
    
        vA, vB, CCCC = map(int, self.args)
        return [(vB << 12) | (vA << 8) | self.opcode, CCCC]

class format_22cs(format_22c):

    size = 2
    
    def decode(self, bytecodes, offset):

        # B|A|op    op vA,vB,fieldoff@CCCC
        # CCCC
        return format_22c.decode(self, bytecodes, offset)

class format_30t(Instruction):

    size = 3
    
    def __init__(self, AAAAAAAA = 0):
    
        self.args = [AAAAAAAA]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # 00|op         op +AAAAAAAA
        # AAAA (low)
        # AAAA (high)
        AAAAAAAA = Offset(bytecodes[offset + 1] | (bytecodes[offset + 2] << 16), 32)
        self.args = [AAAAAAAA]
        return offset + 3
    
    def encode(self):
    
        AAAAAAAA, = map(int, self.args)
        return [self.opcode, AAAAAAAA & 0xffff, AAAAAAAA >> 16]

class format_32x(Instruction):

    size = 3
    
    def __init__(self, vAAAA = 0, vBBBB = 0):
    
        self.args = [vAAAA, vBBBB]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # 00|op     op vAAAA,vBBBB
        # AAAA
        # BBBB
        vAAAA = v(bytecodes[offset + 1], 16)
        vBBBB = v(bytecodes[offset + 2], 16)
        self.args = [vAAAA, vBBBB]
        return offset + 3
    
    def encode(self):
    
        vAAAA, vBBBB = map(int, self.args)
        return [self.opcode, vAAAA, vBBBB]

class format_31i(Instruction):

    size = 3
    
    def __init__(self, vAA = 0, BBBBBBBB = 0):
    
        self.args = [vAA, BBBBBBBB]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op         op vAA,#+BBBBBBBB
        # BBBB (low)
        # BBBB (high)
        vAA = v(bytecodes[offset] >> 8, 8)
        BBBBBBBB = Constant(bytecodes[offset + 1] | (bytecodes[offset + 2] << 16), 32)
        self.args = [vAA, BBBBBBBB]
        return offset + 3
    
    def encode(self):
    
        vAA, BBBBBBBB = map(int, self.args)
        return [(vAA << 8) | self.opcode, BBBBBBBB & 0xffff, BBBBBBBB >> 16]

class format_31t(Instruction):

    size = 3
    
    def __init__(self, vAA = 0, BBBBBBBB = 0):
    
        self.args = [vAA, BBBBBBBB]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op         op vAA,+BBBBBBBB
        # BBBB (low)
        # BBBB (high)
        vAA = v(bytecodes[offset] >> 8, 8)
        BBBBBBBB = Offset(bytecodes[offset + 1] | (bytecodes[offset + 2] << 16), 32)
        self.args = [vAA, BBBBBBBB]
        return offset + 3
    
    def encode(self):
    
        vAA, BBBBBBBB = map(int, self.args)
        return [(vAA << 8) | self.opcode, BBBBBBBB & 0xffff, BBBBBBBB >> 16]

class format_31c(Instruction):

    size = 3
    
    def __init__(self, vAA = 0, BBBBBBBB = 0):
    
        self.args = [vAA, BBBBBBBB]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op         op vAA,string@BBBBBBBB
        # BBBB (low)
        # BBBB (high)
        vAA = v(bytecodes[offset] >> 8, 8)
        BBBBBBBB = Index(bytecodes[offset + 1] | (bytecodes[offset + 2] << 16), 32)
        self.args = [vAA, BBBBBBBB]
        return offset + 3
    
    def encode(self):
    
        vAA, BBBBBBBB = map(int, self.args)
        return [(vAA << 8) | self.opcode, BBBBBBBB & 0xffff, BBBBBBBB >> 16]

class format_35c(Instruction):

    size = 3
    
    def __init__(self, A = 0, BBBB = 0, vC = 0, vD = 0, vE = 0, vF = 0, vG = 0):
    
        self.args = [A] + [vC, vD, vE, vF, vG][:int(A)] + [BBBB]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # A|G|op    op {vC, vD, vE, vF, vG},meth@BBBB   [A=5]
        # BBBB      op {vC, vD, vE, vF, vG},type@BBBB   [A=5]
        # F|E|D|C   op {vC, vD, vE, vF},kind@BBBB       [A=4]
        #           op {vC, vD, vE},kind@BBBB           [A=3]
        #           op {vC, vD},kind@BBBB               [A=2]
        #           op {vC},kind@BBBB                   [A=1]
        #           op {},kind@BBBB                     [A=0]
        A = Count(bytecodes[offset] >> 12, 4)
        BBBB = Index(bytecodes[offset + 1], 16)
        vC = v(bytecodes[offset + 2] & 0x0f, 4)
        vD = v((bytecodes[offset + 2] >> 4) & 0x0f, 4)
        vE = v((bytecodes[offset + 2] >> 8) & 0x0f, 4)
        vF = v(bytecodes[offset + 2] >> 12, 4)
        vG = v((bytecodes[offset] >> 8) & 0x0f, 4)
        if A.value == 5:
            self.args = [A, vC, vD, vE, vF, vG, BBBB]
            return offset + 3
        elif A.value == 4:
            self.args = [A, vC, vD, vE, vF, BBBB]
            return offset + 3
        elif A.value == 3:
            self.args = [A, vC, vD, vE, BBBB]
            return offset + 3
        elif A.value == 2:
            self.args = [A, vC, vD, BBBB]
            return offset + 3
        elif A.value == 1:
            self.args = [A, vC, BBBB]
            return offset + 3
        else:
            self.args = [A, BBBB]
            return offset + 3
    
    def encode(self):
    
        args = map(int, self.args)
        A = args[0]
        
        vC, vD, vE, vF, vG = args[1:1 + A] + ([0] * (5 - A))
        BBBB = args[-1]
        return [(A << 12) | (vG << 8) | self.opcode, BBBB,
                (vF << 12) | (vE << 8) | (vD << 4) | vC]

class format_35ms(format_35c):

    size = 3
    
    def decode(self, bytecodes, offset):

        # A|G|op    op {vC, vD, vE, vF, vG},vtaboff@BBBB   [A=5]
        # BBBB      op {vC, vD, vE, vF},vtaboff@BBBB       [A=4]
        # F|E|D|C   op {vC, vD, vE},vtaboff@BBBB           [A=3]
        #           op {vC, vD},vtaboff@BBBB               [A=2]
        #           op {vC},vtaboff@BBBB                   [A=1]
        return format_35c.decode(self, bytecodes, offset)

class format_35mi(format_35c):

    size = 3
    
    def decode(self, bytecodes, offset):
    
        # A|G|op    op {vC, vD, vE, vF, vG},vtaboff@BBBB   [A=5]
        # BBBB      op {vC, vD, vE, vF},vtaboff@BBBB       [A=4]
        # F|E|D|C   op {vC, vD, vE},vtaboff@BBBB           [A=3]
        #           op {vC, vD},vtaboff@BBBB               [A=2]
        #           op {vC},vtaboff@BBBB                   [A=1]
        return format_35c.decode(self, bytecodes, offset)

class format_3rc(Instruction):

    size = 3
    
    def __init__(self, AA = 0, BBBB = 0, vCCCC = 0):
    
        self.args = [AA, BBBB, vCCCC]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op     op {vCCCC..vNNNN},meth@BBBB
        # BBBB      op {vCCCC..vNNNN},type@BBBB
        # CCCC
        AA = bytecodes[offset] >> 8
        BBBB = Index(bytecodes[offset + 1], 16)
        vCCCC = v(bytecodes[offset + 2], 16)
        self.args = [AA, BBBB, vCCCC]
        return offset + 3
    
    def encode(self):
    
        AA, BBBB, vCCCC = map(int, self.args)
        return [(AA << 8) | self.opcode, BBBB, vCCCC]

class format_3rms(format_3rc):

    size = 3
    
    def decode(self, bytecodes, offset):
    
        # AA|op     op {vCCCC..vNNNN},vtaboff@BBBB
        # BBBB
        # CCCC
        return format_3rc.decode(self, bytecodes, offset)

class format_3rmi(format_3rc):

    size = 3
    
    def decode(self, bytecodes, offset):
    
        # AA|op     op {vCCCC..vNNNN},inline@BBBB
        # BBBB
        # CCCC
        return format_3rc(self, bytecodes, offset)

class format_51l(Instruction):

    size = 5
    
    def __init__(self, vAA = 0, B16 = 0):
    
        self.args = [vAA, B16]
        self.decode(self.encode(), 0)
    
    def decode(self, bytecodes, offset):

        # AA|op         op vAA,#+BBBBBBBBBBBBBBBB
        # BBBB (low)
        # BBBB
        # BBBB
        # BBBB (high)
        vAA = v(bytecodes[offset] >> 8, 8)
        B16 = Constant(bytecodes[offset + 1] | (bytecodes[offset + 2] << 16) |
            (bytecodes[offset + 3] << 32) | (bytecodes[offset + 4] << 48), 64)
        self.args = [vAA, B16]
        return offset + 5
    
    def encode(self):
    
        vAA, B16 = map(int, self.args)
        return [(vAA << 8) | self.opcode, B16 & 0xffff, (B16 >> 16) & 0xffff,
                (B16 >> 32) & 0xffff, (B16 >> 48) & 0xffff]

class nop(format_10x):

    size = 1
    
    opcode = 0x00
    
    def decode(self, bytecodes, offset):
    
        data_format = bytecodes[offset] >> 8
        offset = format_10x.decode(self, bytecodes, offset)
        
        if data_format == 0x01:
        
            # packed-switch-payload format
        
            self.targets = []
            self.size = bytecodes[offset]
            self.first_key = bytecodes[offset + 1] | (bytecodes[offset + 2] << 16)
            
            start = offset + 3
            i = 0
            while i < self.size:
                low = bytecodes[start + (i * 2)]
                high = bytecodes[start + (i * 2) + 1]
                self.targets.append(low | (high << 16))
                i += 1
            
            self.notes = ["first key=%i" % self.first_key,
                          "targets=%s" % repr(self.targets)]
            return start + (self.size * 2)
        
        elif data_format == 0x02:
        
            # sparse-switch-payload format
            
            self.keys = []
            self.targets = []
            self.size = bytecodes[offset]
            
            start = offset + 1
            i = 0
            while i < self.size:
                low = bytecodes[start + (i * 2)]
                high = bytecodes[start + (i * 2) + 1]
                self.keys.append(low | (high << 16))
                i += 1
            
            start = offset + 1 + (self.size * 2)
            i = 0
            while i < self.size:
                low = bytecodes[start + (i * 2)]
                high = bytecodes[start + (i * 2) + 1]
                self.targets.append(low | (high << 16))
                i += 1
            
            self.notes = ["keys=%s" % repr(self.keys),
                          "targets=%s" % repr(self.targets)]
            return start + (self.size * 2)
        
        elif data_format == 0x03:
        
            # fill-array-data-payload format
            
            self.width = bytecodes[offset]
            self.size = bytecodes[offset + 1] | (bytecodes[offset + 2] << 16)
            self.data = []
            
            p = offset + 3
            q = 0
            i = 0
            while i < self.size:
            
                value = 0
                j = 0
                while j < self.width:
                
                    # Read the relevant byte from the bytecode.
                    byte = (bytecodes[p] >> q) & 0xff
                    # Shift the value appropriately. This may need reworking
                    # for big-endian systems.
                    value = value | (byte << (j * 8))
                    j += 1
                    # Access the other byte in the bytecode.
                    q = 8 - q
                    # Move to the next bytecode if both bytes have been read.
                    if q == 0:
                        p += 1
                
                self.data.append(value)
                i += 1
            
            # If we are in the middle of a bytecode then move to the next.
            if q != 0:
                p += 1
            
            self.notes = ["data=%s" % repr(self.data)]
            return p
        else:
            return offset

class move(format_12x):
    opcode = 0x01
class move_from16(format_22x):
    opcode = 0x02
class move_16(format_32x):
    opcode = 0x03
class move_wide(format_12x):
    opcode = 0x04
class move_wide_from16(format_22x):
    opcode = 0x05
class move_wide_16(format_32x):
    opcode = 0x06
class move_object(format_12x):
    opcode = 0x07
class move_object_from16(format_22x):
    opcode = 0x08
class move_object_16(format_32x):
    opcode = 0x09
class move_result(format_11x):
    opcode = 0x0a
class move_result_wide(format_11x):
    opcode = 0x0b
class move_result_object(format_11x):
    opcode = 0x0c
class move_exception(format_11x):
    opcode = 0x0d
class return_void(format_10x):
    opcode = 0x0e
class return_(format_11x):
    opcode = 0x0f
class return_wide(format_11x):
    opcode = 0x10
class return_object(format_11x):
    opcode = 0x11
class const_4(format_11n):
    opcode = 0x12
class const_16(format_21s):
    opcode = 0x13
class const(format_31i):
    opcode = 0x14
class const_high_16(format_21h):
    opcode = 0x15
class const_wide_16(format_21s):
    opcode = 0x16
class const_wide_32(format_31i):
    opcode = 0x17
class const_wide(format_51l):
    opcode = 0x18
class const_wide_high16(format_21h):
    opcode = 0x19
class const_string(format_21c):
    opcode = 0x1a
class const_string_jumbo(format_31c):
    opcode = 0x1b
class const_class(format_21c):
    opcode = 0x1c
class monitor_enter(format_11x):
    opcode = 0x1d
class monitor_exit(format_11x):
    opcode = 0x1e
class check_cast(format_21c):
    opcode = 0x1f
class instance_of(format_22c):
    opcode = 0x20
class array_length(format_12x):
    opcode = 0x21
class new_instance(format_21c):
    opcode = 0x22
class new_array(format_22c):
    opcode = 0x23
class filled_new_array(format_35c):
    opcode = 0x24
class filled_new_array_range(format_3rc):
    opcode = 0x25
class fill_array_data(format_31t):
    opcode = 0x26
class throw(format_11x):
    opcode = 0x27
class goto(format_10t):
    opcode = 0x28
class goto_16(format_20t):
    opcode = 0x29
class goto_32(format_30t):
    opcode = 0x2a

class packed_switch(format_31t):

    opcode = 0x2b
    
    def decode(self, bytecodes, offset):
    
        offset = format_31t.decode(self, bytecodes, offset)
        return offset

class sparse_switch(format_31t):

    opcode = 0x2c
    
    def decode(self, bytecodes, offset):
    
        offset = format_31t.decode(self, bytecodes, offset)
        return offset

# Define a class for each bytecode, derived from the appropriate base class
# that specifies its format.

class cmpl_float(format_23x):
    opcode = 0x2d
class cmpg_float(format_23x):
    opcode = 0x2e
class cmpl_double(format_23x):
    opcode = 0x2f
class cmpg_double(format_23x):
    opcode = 0x30
class cmp_long(format_23x):
    opcode = 0x31
class if_eq(format_22t):
    opcode = 0x32
class if_ne(format_22t):
    opcode = 0x33
class if_lt(format_22t):
    opcode = 0x34
class if_ge(format_22t):
    opcode = 0x35
class if_gt(format_22t):
    opcode = 0x36
class if_le(format_22t):
    opcode = 0x37
class if_eqz(format_21t):
    opcode = 0x38
class if_nez(format_21t):
    opcode = 0x39
class if_ltz(format_21t):
    opcode = 0x3a
class if_gez(format_21t):
    opcode = 0x3b
class if_gtz(format_21t):
    opcode = 0x3c
class if_lez(format_21t):
    opcode = 0x3d
class aget(format_23x):
    opcode = 0x44
class aget_wide(format_23x):
    opcode = 0x45
class aget_object(format_23x):
    opcode = 0x46
class aget_boolean(format_23x):
    opcode = 0x47
class aget_byte(format_23x):
    opcode = 0x48
class aget_char(format_23x):
    opcode = 0x49
class aget_short(format_23x):
    opcode = 0x4a
class aput(format_23x):
    opcode = 0x4b
class aput_wide(format_23x):
    opcode = 0x4c
class aput_object(format_23x):
    opcode = 0x4d
class aput_boolean(format_23x):
    opcode = 0x4e
class aput_byte(format_23x):
    opcode = 0x4f
class aput_char(format_23x):
    opcode = 0x50
class aput_short(format_23x):
    opcode = 0x51
class iget(format_22c):
    opcode = 0x52
class iget_wide(format_22c):
    opcode = 0x53
class iget_object(format_22c):
    opcode = 0x54
class iget_boolean(format_22c):
    opcode = 0x55
class iget_byte(format_22c):
    opcode = 0x56
class iget_char(format_22c):
    opcode = 0x57
class iget_short(format_22c):
    opcode = 0x58
class iput(format_22c):
    opcode = 0x59
class iput_wide(format_22c):
    opcode = 0x5a
class iput_object(format_22c):
    opcode = 0x5b
class iput_boolean(format_22c):
    opcode = 0x5c
class iput_byte(format_22c):
    opcode = 0x5d
class iput_char(format_22c):
    opcode = 0x5e
class iput_short(format_22c):
    opcode = 0x5f
class sget(format_21c):
    opcode = 0x60
class sget_wide(format_21c):
    opcode = 0x61
class sget_object(format_21c):
    opcode = 0x62
class sget_boolean(format_21c):
    opcode = 0x63
class sget_byte(format_21c):
    opcode = 0x64
class sget_char(format_21c):
    opcode = 0x65
class sget_short(format_21c):
    opcode = 0x66
class sput(format_21c):
    opcode = 0x67
class sput_wide(format_21c):
    opcode = 0x68
class sput_object(format_21c):
    opcode = 0x69
class sput_boolean(format_21c):
    opcode = 0x6a
class sput_byte(format_21c):
    opcode = 0x6b
class sput_char(format_21c):
    opcode = 0x6c
class sput_short(format_21c):
    opcode = 0x6d
class invoke_virtual(format_35c):
    opcode = 0x6e
class invoke_super(format_35c):
    opcode = 0x6f
class invoke_direct(format_35c):
    opcode = 0x70
class invoke_static(format_35c):
    opcode = 0x71
class invoke_interface(format_35c):
    opcode = 0x72
class invoke_virtual_range(format_3rc):
    opcode = 0x74
class invoke_super_range(format_3rc):
    opcode = 0x75
class invoke_direct_range(format_3rc):
    opcode = 0x76
class invoke_static_range(format_3rc):
    opcode = 0x77
class invoke_interface_range(format_3rc):
    opcode = 0x78
class neg_int(format_12x):
    opcode = 0x7b
class not_int(format_12x):
    opcode = 0x7c
class neg_long(format_12x):
    opcode = 0x7d
class not_long(format_12x):
    opcode = 0x7e
class neg_float(format_12x):
    opcode = 0x7f
class neg_double(format_12x):
    opcode = 0x80
class int_to_long(format_12x):
    opcode = 0x81
class int_to_float(format_12x):
    opcode = 0x82
class int_to_double(format_12x):
    opcode = 0x83
class long_to_int(format_12x):
    opcode = 0x84
class long_to_float(format_12x):
    opcode = 0x85
class long_to_double(format_12x):
    opcode = 0x86
class float_to_int(format_12x):
    opcode = 0x87
class float_to_long(format_12x):
    opcode = 0x88
class float_to_double(format_12x):
    opcode = 0x89
class double_to_int(format_12x):
    opcode = 0x8a
class double_to_long(format_12x):
    opcode = 0x8b
class double_to_float(format_12x):
    opcode = 0x8c
class int_to_byte(format_12x):
    opcode = 0x8d
class int_to_char(format_12x):
    opcode = 0x8e
class int_to_short(format_12x):
    opcode = 0x8f
class add_int(format_23x):
    opcode = 0x90
class sub_int(format_23x):
    opcode = 0x91
class mul_int(format_23x):
    opcode = 0x92
class div_int(format_23x):
    opcode = 0x93
class rem_int(format_23x):
    opcode = 0x94
class and_int(format_23x):
    opcode = 0x95
class or_int(format_23x):
    opcode = 0x96
class xor_int(format_23x):
    opcode = 0x97
class shl_int(format_23x):
    opcode = 0x98
class shr_int(format_23x):
    opcode = 0x99
class ushr_int(format_23x):
    opcode = 0x9a
class add_long(format_23x):
    opcode = 0x9b
class sub_long(format_23x):
    opcode = 0x9c
class mul_long(format_23x):
    opcode = 0x9d
class div_long(format_23x):
    opcode = 0x9e
class rem_long(format_23x):
    opcode = 0x9f
class and_long(format_23x):
    opcode = 0xa0
class or_long(format_23x):
    opcode = 0xa1
class xor_long(format_23x):
    opcode = 0xa2
class shl_long(format_23x):
    opcode = 0xa3
class shr_long(format_23x):
    opcode = 0xa4
class ushr_long(format_23x):
    opcode = 0xa5
class add_float(format_23x):
    opcode = 0xa6
class sub_float(format_23x):
    opcode = 0xa7
class mul_float(format_23x):
    opcode = 0xa8
class div_float(format_23x):
    opcode = 0xa9
class rem_float(format_23x):
    opcode = 0xaa
class add_double(format_23x):
    opcode = 0xab
class sub_double(format_23x):
    opcode = 0xac
class mul_double(format_23x):
    opcode = 0xad
class div_double(format_23x):
    opcode = 0xae
class rem_double(format_23x):
    opcode = 0xaf
class add_int_2addr(format_12x):
    opcode = 0xb0
class sub_int_2addr(format_12x):
    opcode = 0xb1
class mul_int_2addr(format_12x):
    opcode = 0xb2
class div_int_2addr(format_12x):
    opcode = 0xb3
class rem_int_2addr(format_12x):
    opcode = 0xb4
class and_int_2addr(format_12x):
    opcode = 0xb5
class or_int_2addr(format_12x):
    opcode = 0xb6
class xor_int_2addr(format_12x):
    opcode = 0xb7
class shl_int_2addr(format_12x):
    opcode = 0xb8
class shr_int_2addr(format_12x):
    opcode = 0xb9
class ushr_int_2addr(format_12x):
    opcode = 0xba
class add_long_2addr(format_12x):
    opcode = 0xbb
class sub_long_2addr(format_12x):
    opcode = 0xbc
class mul_long_2addr(format_12x):
    opcode = 0xbd
class div_long_2addr(format_12x):
    opcode = 0xbe
class rem_long_2addr(format_12x):
    opcode = 0xbf
class and_long_2addr(format_12x):
    opcode = 0xc0
class or_long_2addr(format_12x):
    opcode = 0xc1
class xor_long_2addr(format_12x):
    opcode = 0xc2
class shl_long_2addr(format_12x):
    opcode = 0xc3
class shr_long_2addr(format_12x):
    opcode = 0xc4
class ushr_long_2addr(format_12x):
    opcode = 0xc5
class add_float_2addr(format_12x):
    opcode = 0xc6
class sub_float_2addr(format_12x):
    opcode = 0xc7
class mul_float_2addr(format_12x):
    opcode = 0xc8
class div_float_2addr(format_12x):
    opcode = 0xc9
class rem_float_2addr(format_12x):
    opcode = 0xca
class add_double_2addr(format_12x):
    opcode = 0xcb
class sub_double_2addr(format_12x):
    opcode = 0xcc
class mul_double_2addr(format_12x):
    opcode = 0xcd
class div_double_2addr(format_12x):
    opcode = 0xce
class rem_double_2addr(format_12x):
    opcode = 0xcf
class add_int_lit16(format_22s):
    opcode = 0xd0
class rsub_int_lit16(format_22s):
    opcode = 0xd1
class mul_int_lit16(format_22s):
    opcode = 0xd2
class div_int_lit16(format_22s):
    opcode = 0xd3
class rem_int_lit16(format_22s):
    opcode = 0xd4
class and_int_lit16(format_22s):
    opcode = 0xd5
class or_int_lit16(format_22s):
    opcode = 0xd6
class xor_int_lit16(format_22s):
    opcode = 0xd7
class add_int_lit8(format_22b):
    opcode = 0xd8
class rsub_int_lit8(format_22b):
    opcode = 0xd9
class mul_int_lit8(format_22b):
    opcode = 0xda
class div_int_lit8(format_22b):
    opcode = 0xdb
class rem_int_lit8(format_22b):
    opcode = 0xdc
class and_int_lit8(format_22b):
    opcode = 0xdd
class or_int_lit8(format_22b):
    opcode = 0xde
class xor_int_lit8(format_22b):
    opcode = 0xdf
class shl_int_lit8(format_22b):
    opcode = 0xe0
class shr_int_lit8(format_22b):
    opcode = 0xe1
class ushr_int_lit8(format_22b):
    opcode = 0xe2

lookup = {
    nop.opcode: nop,
    move.opcode: move,
    move_from16.opcode: move_from16,
    move_16.opcode: move_16,
    move_wide.opcode: move_wide,
    move_wide_from16.opcode: move_wide_from16,
    move_wide_16.opcode: move_wide_16,
    move_object.opcode: move_object,
    move_object_from16.opcode: move_object_from16,
    move_object_16.opcode: move_object_16,
    move_result.opcode: move_result,
    move_result_wide.opcode: move_result_wide,
    move_result_object.opcode: move_result_object,
    move_exception.opcode: move_exception,
    return_void.opcode: return_void,
    return_.opcode: return_,
    return_wide.opcode: return_wide,
    return_object.opcode: return_object,
    const_4.opcode: const_4,
    const_16.opcode: const_16,
    const.opcode: const,
    const_high_16.opcode: const_high_16,
    const_wide_16.opcode: const_wide_16,
    const_wide_32.opcode: const_wide_32,
    const_wide.opcode: const_wide,
    const_wide_high16.opcode: const_wide_high16,
    const_string.opcode: const_string,
    const_string_jumbo.opcode: const_string_jumbo,
    const_class.opcode: const_class,
    monitor_enter.opcode: monitor_enter,
    monitor_exit.opcode: monitor_exit,
    check_cast.opcode: check_cast,
    instance_of.opcode: instance_of,
    array_length.opcode: array_length,
    new_instance.opcode: new_instance,
    new_array.opcode: new_array,
    filled_new_array.opcode: filled_new_array,
    filled_new_array_range.opcode: filled_new_array_range,
    fill_array_data.opcode: fill_array_data,
    throw.opcode: throw,
    goto.opcode: goto,
    goto_16.opcode: goto_16,
    goto_32.opcode: goto_32,
    packed_switch.opcode: packed_switch,
    sparse_switch.opcode: sparse_switch,
    cmpl_float.opcode: cmpl_float,
    cmpg_float.opcode: cmpg_float,
    cmpl_double.opcode: cmpl_double,
    cmpg_double.opcode: cmpg_double,
    cmp_long.opcode: cmp_long,
    if_eq.opcode: if_eq,
    if_ne.opcode: if_ne,
    if_lt.opcode: if_lt,
    if_ge.opcode: if_ge,
    if_gt.opcode: if_gt,
    if_le.opcode: if_le,
    if_eqz.opcode: if_eqz,
    if_nez.opcode: if_nez,
    if_ltz.opcode: if_ltz,
    if_gez.opcode: if_gez,
    if_gtz.opcode: if_gtz,
    if_lez.opcode: if_lez,
    # 0x3e...0x43 are unused
    aget.opcode: aget,
    aget_wide.opcode: aget_wide,
    aget_object.opcode: aget_object,
    aget_boolean.opcode: aget_boolean,
    aget_byte.opcode: aget_byte,
    aget_char.opcode: aget_char,
    aget_short.opcode: aget_short,
    aput.opcode: aput,
    aput_wide.opcode: aput_wide,
    aput_object.opcode: aput_object,
    aput_boolean.opcode: aput_boolean,
    aput_byte.opcode: aput_byte,
    aput_char.opcode: aput_char,
    aput_short.opcode: aput_short,
    iget.opcode: iget,
    iget_wide.opcode: iget_wide,
    iget_object.opcode: iget_object,
    iget_boolean.opcode: iget_boolean,
    iget_byte.opcode: iget_byte,
    iget_char.opcode: iget_char,
    iget_short.opcode: iget_short,
    iput.opcode: iput,
    iput_wide.opcode: iput_wide,
    iput_object.opcode: iput_object,
    iput_boolean.opcode: iput_boolean,
    iput_byte.opcode: iput_byte,
    iput_char.opcode: iput_char,
    iput_short.opcode: iput_short,
    sget.opcode: sget,
    sget_wide.opcode: sget_wide,
    sget_object.opcode: sget_object,
    sget_boolean.opcode: sget_boolean,
    sget_byte.opcode: sget_byte,
    sget_char.opcode: sget_char,
    sget_short.opcode: sget_short,
    sput.opcode: sput,
    sput_wide.opcode: sput_wide,
    sput_object.opcode: sput_object,
    sput_boolean.opcode: sput_boolean,
    sput_byte.opcode: sput_byte,
    sput_char.opcode: sput_char,
    sput_short.opcode: sput_short,
    invoke_virtual.opcode: invoke_virtual,
    invoke_super.opcode: invoke_super,
    invoke_direct.opcode: invoke_direct,
    invoke_static.opcode: invoke_static,
    invoke_interface.opcode: invoke_interface,
    # 0x73 is unused
    invoke_virtual_range.opcode: invoke_virtual_range,
    invoke_super_range.opcode: invoke_super_range,
    invoke_direct_range.opcode: invoke_direct_range,
    invoke_static_range.opcode: invoke_static_range,
    invoke_interface_range.opcode: invoke_interface_range,
    # 0x79...0x7a are unused
    neg_int.opcode: neg_int,
    not_int.opcode: not_int,
    neg_long.opcode: neg_long,
    not_long.opcode: not_long,
    neg_float.opcode: neg_float,
    neg_double.opcode: neg_double,
    int_to_long.opcode: int_to_long,
    int_to_float.opcode: int_to_float,
    int_to_double.opcode: int_to_double,
    long_to_int.opcode: long_to_int,
    long_to_float.opcode: long_to_float,
    long_to_double.opcode: long_to_double,
    float_to_int.opcode: float_to_int,
    float_to_long.opcode: float_to_long,
    float_to_double.opcode: float_to_double,
    double_to_int.opcode: double_to_int,
    double_to_long.opcode: double_to_long,
    double_to_float.opcode: double_to_float,
    int_to_byte.opcode: int_to_byte,
    int_to_char.opcode: int_to_char,
    int_to_short.opcode: int_to_short,
    add_int.opcode: add_int,
    sub_int.opcode: sub_int,
    mul_int.opcode: mul_int,
    div_int.opcode: div_int,
    rem_int.opcode: rem_int,
    and_int.opcode: and_int,
    or_int.opcode: or_int,
    xor_int.opcode: xor_int,
    shl_int.opcode: shl_int,
    shr_int.opcode: shr_int,
    ushr_int.opcode: ushr_int,
    add_long.opcode: add_long,
    sub_long.opcode: sub_long,
    mul_long.opcode: mul_long,
    div_long.opcode: div_long,
    rem_long.opcode: rem_long,
    and_long.opcode: and_long,
    or_long.opcode: or_long,
    xor_long.opcode: xor_long,
    shl_long.opcode: shl_long,
    shr_long.opcode: shr_long,
    ushr_long.opcode: ushr_long,
    add_float.opcode: add_float,
    sub_float.opcode: sub_float,
    mul_float.opcode: mul_float,
    div_float.opcode: div_float,
    rem_float.opcode: rem_float,
    add_double.opcode: add_double,
    sub_double.opcode: sub_double,
    mul_double.opcode: mul_double,
    div_double.opcode: div_double,
    rem_double.opcode: rem_double,
    add_int_2addr.opcode: add_int_2addr,
    sub_int_2addr.opcode: sub_int_2addr,
    mul_int_2addr.opcode: mul_int_2addr,
    div_int_2addr.opcode: div_int_2addr,
    rem_int_2addr.opcode: rem_int_2addr,
    and_int_2addr.opcode: and_int_2addr,
    or_int_2addr.opcode: or_int_2addr,
    xor_int_2addr.opcode: xor_int_2addr,
    shl_int_2addr.opcode: shl_int_2addr,
    shr_int_2addr.opcode: shr_int_2addr,
    ushr_int_2addr.opcode: ushr_int_2addr,
    add_long_2addr.opcode: add_long_2addr,
    sub_long_2addr.opcode: sub_long_2addr,
    mul_long_2addr.opcode: mul_long_2addr,
    div_long_2addr.opcode: div_long_2addr,
    rem_long_2addr.opcode: rem_long_2addr,
    and_long_2addr.opcode: and_long_2addr,
    or_long_2addr.opcode: or_long_2addr,
    xor_long_2addr.opcode: xor_long_2addr,
    shl_long_2addr.opcode: shl_long_2addr,
    shr_long_2addr.opcode: shr_long_2addr,
    ushr_long_2addr.opcode: ushr_long_2addr,
    add_float_2addr.opcode: add_float_2addr,
    sub_float_2addr.opcode: sub_float_2addr,
    mul_float_2addr.opcode: mul_float_2addr,
    div_float_2addr.opcode: div_float_2addr,
    rem_float_2addr.opcode: rem_float_2addr,
    add_double_2addr.opcode: add_double_2addr,
    sub_double_2addr.opcode: sub_double_2addr,
    mul_double_2addr.opcode: mul_double_2addr,
    div_double_2addr.opcode: div_double_2addr,
    rem_double_2addr.opcode: rem_double_2addr,
    add_int_lit16.opcode: add_int_lit16,
    rsub_int_lit16.opcode: rsub_int_lit16,
    mul_int_lit16.opcode: mul_int_lit16,
    div_int_lit16.opcode: div_int_lit16,
    rem_int_lit16.opcode: rem_int_lit16,
    and_int_lit16.opcode: and_int_lit16,
    or_int_lit16.opcode: or_int_lit16,
    xor_int_lit16.opcode: xor_int_lit16,
    add_int_lit8.opcode: add_int_lit8,
    rsub_int_lit8.opcode: rsub_int_lit8,
    mul_int_lit8.opcode: mul_int_lit8,
    div_int_lit8.opcode: div_int_lit8,
    rem_int_lit8.opcode: rem_int_lit8,
    and_int_lit8.opcode: and_int_lit8,
    or_int_lit8.opcode: or_int_lit8,
    xor_int_lit8.opcode: xor_int_lit8,
    shl_int_lit8.opcode: shl_int_lit8,
    shr_int_lit8.opcode: shr_int_lit8,
    ushr_int_lit8.opcode: ushr_int_lit8
    # 0xe3...0xff are unused
    }

def disassemble(bytecodes, begin = 0, end = None, return_offsets = False):

    if end is None:
        end = len(bytecodes)
    
    instructions = []
    offset = begin
    
    while offset < end:
    
        if return_offsets:
            o = offset
        
        b = bytecodes[offset] & 0xff
        op = lookup[b]
        instruction = op()
        offset = instruction.decode(bytecodes, offset)
        
        if return_offsets:
            instructions.append((o, instruction))
        else:
            instructions.append(instruction)
    
    return instructions

def assemble(instructions, begin = 0, end = None, return_offsets = False):

    if end is None:
        end = len(instructions)
    
    bytecodes = []
    offset = begin
    
    while offset < end:
    
        if return_offsets:
            o = offset
        
        instruction = instructions[offset]
        words = instruction.encode()
        
        if return_offsets:
            bytecodes.append((o, words))
        else:
            bytecodes += words
        
        offset += 1
    
    return bytecodes
