# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.location"

from java.lang import Iterable, Object, String
from java.util import List
from android.app import PendingIntent
from android.os import Bundle, Looper, Parcelable

class Criteria(Object):

    __interfaces__ = [Parcelable]
    __fields__ = {
        "CREATOR": Parcelable.Creator(Criteria)
        }
    
    ACCURACY_COARSE = 2
    ACCURACY_FINE = 1
    ACCURACY_HIGH = 3
    ACCURACY_LOW = 1
    ACCURACY_MEDIUM = 2
    NO_REQUIREMENT = 0
    POWER_HIGH = 3
    POWER_LOW = 1
    POWER_MEDIUM = 2
    
    def __init__(self):
        pass
    
    @args(void, [Criteria])
    def __init__(self):
        pass
    
    @args(int, [])
    def getAccuracy(self):
        pass
    
    @args(int, [])
    def getBearingAccuracy(self):
        pass
    
    @args(int, [])
    def getHorizontalAccuracy(self):
        pass
    
    @args(int, [])
    def getPowerRequirement(self):
        pass
    
    @args(int, [])
    def getSpeedAccuracy(self):
        pass
    
    @args(int, [])
    def getVerticalAccuracy(self):
        pass
    
    @args(bool, [])
    def isAltitudeRequired(self):
        pass
    
    @args(bool, [])
    def isBearingRequired(self):
        pass
    
    @args(bool, [])
    def isCostAllowed(self):
        pass
    
    @args(bool, [])
    def isSpeedRequired(self):
        pass
    
    @args(void, [int])
    def setAccuracy(self, accuracy):
        pass
    
    @args(void, [bool])
    def setAltitudeRequired(self, altitudeRequired):
        pass
    
    @args(void, [int])
    def setBearingAccuracy(self, accuracy):
        pass
    
    @args(void, [bool])
    def setBearingRequired(self, bearingRequired):
        pass
    
    @args(void, [bool])
    def setCostAllowed(self, costAllowed):
        pass
    
    @args(void, [int])
    def setHorizontalAccuracy(self, accuracy):
        pass
    
    @args(void, [int])
    def setPowerRequirement(self, level):
        pass
    
    @args(void, [int])
    def setSpeedAccuracy(self, accuracy):
        pass
    
    @args(void, [bool])
    def setSpeedRequired(self, speedRequired):
        pass
    
    @args(void, [int])
    def setVerticalAccuracy(self, accuracy):
        pass


class GpsSatellite(Object):

    @args(float, [])
    def getAzimuth(self):
        pass
    
    @args(float, [])
    def getElevation(self):
        pass
    
    @args(int, [])
    def getPrn(self):
        pass
    
    @args(float, [])
    def getSnr(self):
        pass
    
    @args(bool, [])
    def hasAlmanac(self):
        pass
    
    @args(bool, [])
    def hasEphemeris(self):
        pass
    
    @args(bool, [])
    def usedInFix(self):
        pass


class GpsStatus(Object):

    GPS_EVENT_FIRST_FIX = 3
    GPS_EVENT_SATELLITE_STATUS = 4
    GPS_EVENT_STARTED = 1
    GPS_EVENT_STOPPED = 2
    
    class Listener:
    
        @args(void, [int])
        def onGpsStatusChanged(self, event):
            pass
    
    class NmeaListener:
    
        @args(void, [long, String])
        def onNmeaReceived(self, timestamp, nmea):
            pass
    
    @args(int, [])
    def getMaxSatellites(self):
        pass
    
    @args(Iterable(GpsSatellite), [])
    def getSatellites(self):
        pass
    
    @args(int, [])
    def getTimeToFirstFix(self):
        pass


class Location(Object):

    FORMAT_DEGREES = 0
    FORMAT_MINUTES = 1
    FORMAT_SECONDS = 2
    
    __interfaces__ = [Parcelable]
    __fields__ = {
        "CREATOR": Parcelable.Creator(Location)
        }
    
    @args(void, [String])
    def __init__(self, provider):
        pass
    
    @args(void, [Location])
    def __init__(self, location):
        pass
    
    @args(float, [Location])
    def bearingTo(self, dest):
        pass
    
    @static
    @args(double, [String])
    def convert(coordinate):
        pass
    
    @static
    @args(String, [double, int])
    def convert(coordinate, outputType):
        pass
    
    @static
    @args(void, [double, double, double, double, [float]])
    def distanceBetween(startLatitude, startLongitude, endLatitude,
                        endLongitude, results):
        pass
    
    @args(float, [Location])
    def distanceTo(self, dest):
        pass
    
    #@args(void, [Printer, String])
    #def dump(self, pw, prefix):
    #    pass
    
    @args(float, [])
    def getAccuracy(self):
        pass
    
    @args(double, [])
    def getAltitude(self):
        pass
    
    @args(float, [])
    def getBearing(self):
        pass
    
    @args(long, [])
    def getElapsedRealtimeNanos(self):
        pass
    
    @args(Bundle, [])
    def getExtras(self):
        pass
    
    @args(double, [])
    def getLatitude(self):
        pass
    
    @args(double, [])
    def getLongitude(self):
        pass
    
    @args(String, [])
    def getProvider(self):
        pass
    
    @args(float, [])
    def getSpeed(self):
        pass
    
    @args(long, [])
    def getTime(self):
        pass
    
    @args(bool, [])
    def hasAccuracy(self):
        pass
    
    @args(bool, [])
    def hasAltitude(self):
        pass
    
    @args(bool, [])
    def hasBearing(self):
        pass
    
    @args(bool, [])
    def hasSpeed(self):
        pass
    
    @args(bool, [])
    def isFromMockProvider(self):
        pass
    
    def removeAccuracy(self):
        pass
    
    def removeAltitude(self):
        pass
    
    def removeBearing(self):
        pass
    
    def removeSpeed(self):
        pass
    
    def reset(self):
        pass
    
    @args(void, [Location])
    def set(self, location):
        pass
    
    @args(void, [float])
    def setAccuracy(self, accuracy):
        pass
    
    @args(void, [double])
    def setAltitude(self, altitude):
        pass
    
    @args(void, [float])
    def setBearing(self, bearing):
        pass
    
    @args(void, [long])
    def setElapsedRealtimeNanos(self, time):
        pass
    
    @args(void, [Bundle])
    def setExtras(self, extras):
        pass
    
    @args(void, [double])
    def setLatitude(self, latitude):
        pass
    
    @args(void, [double])
    def setLongitude(self, longitude):
        pass
    
    @args(void, [String])
    def setProvider(self, provider):
        pass
    
    @args(void, [float])
    def setSpeed(self, speed):
        pass
    
    @args(void, [long])
    def setTime(self, time):
        pass


class LocationListener:

    @args(void, [Location])
    def onLocationChanged(self, location):
        pass
    
    @args(void, [String])
    def onProviderDisabled(self, provider):
        pass
    
    @args(void, [String])
    def onProviderEnabled(self, provider):
        pass
    
    @args(void, [String, int, Bundle])
    def onStatusChanged(self, provider, status, extras):
        pass


class LocationManager(Object):

    GPS_PROVIDER = "gps"
    KEY_LOCATION_CHANGED = "location"
    KEY_PROVIDER_CHANGED = "providerEnabled"
    KEY_PROXIMITY_ENTERING = "entering"
    KEY_STATUS_CHANGED = "status"
    MODE_CHANGED_ACTION = "android.location.MODE_CHANGED"
    NETWORK_PROVIDER = "network"
    PASSIVE_PROVIDER = "passive"
    PROVIDERS_CHANGED_ACTION = "android.location.PROVIDERS_CHANGED"
    
    @args(bool, [GpsStatus.Listener])
    def addGpsStatusListener(self, listener):
        pass
    
    @args(bool, [GpsStatus.NmeaListener])
    def addNmeaStatusListener(self, listener):
        pass
    
    @args(void, [double, double, float, long, PendingIntent])
    def addProximityAlert(self, latitude, longitude, radius, expiration, intent):
        pass
    
    @args(void, [String, bool, bool, bool, bool, bool, bool, bool, int, int])
    def addTestProvider(self, name, requiresNetwork, requiresSatellite,
        requiresCell, hasMonetaryCost, supportsAltitude, supportsSpeed,
        supportsBearing, powerRequirement, accuracy):
        pass
    
    @args(void, [String])
    def clearTestProviderEnabled(self, provider):
        pass
    
    @args(void, [String])
    def clearTestProviderLocation(self, provider):
        pass
    
    @args(void, [String])
    def clearTestProviderStatus(self, provider):
        pass
    
    @args(List(String), [])
    def getAllProviders(self):
        pass
    
    @args(String, [Criteria, bool])
    def getBestProvider(self, criteria, enabledOnly):
        pass
    
    @args(GpsStatus, [GpsStatus])
    def getGpsStatus(self, status):
        pass
    
    @args(Location, [String])
    def getLastKnownLocation(self, provider):
        pass
    
    @args(LocationProvider, [String])
    def getProvider(self, name):
        pass
    
    @args(List(String), [bool])
    def getProviders(self, enabledOnly):
        pass
    
    @args(List(String), [Criteria, bool])
    def getProviders(self, criteria, enabledOnly):
        pass
    
    @args(bool, [String])
    def isProviderEnabled(self, provider):
        pass
    
    @args(void, [GpsStatus.Listener])
    def removeGpsStatusListener(self, listener):
        pass
    
    @args(void, [GpsStatus.Listener])
    def removeNmeaStatusListener(self, listener):
        pass
    
    @args(void, [PendingIntent])
    def removeProximityAlert(self, listener):
        pass
    
    @args(void, [String])
    def removeTestProvider(self, provider):
        pass
    
    @args(void, [PendingIntent])
    def removeUpdates(self, intent):
        pass
    
    @args(void, [LocationListener])
    def removeUpdates(self, listener):
        pass
    
    @args(void, [long, float, Criteria, PendingIntent])
    def requestLocationUpdates(self, minTime, minDistance, criteria, intent):
        pass
    
    @args(void, [long, float, Criteria, LocationListener, Looper])
    def requestLocationUpdates(self, minTime, minDistance, criteria, listener,
                               looper):
        pass
    
    @args(void, [String, long, float, LocationListener])
    def requestLocationUpdates(self, provider, minTime, minDistance, listener):
        pass
    
    @args(void, [String, long, float, LocationListener, Looper])
    def requestLocationUpdates(self, provider, minTime, minDistance, listener,
                               looper):
        pass
    
    @args(void, [String, long, float, PendingIntent])
    def requestLocationUpdates(self, provider, minTime, minDistance, intent):
        pass
    
    @args(void, [String, LocationListener, Looper])
    def requestSingleUpdate(self, provider, listener, looper):
        pass
    
    @args(void, [Criteria, LocationListener, Looper])
    def requestSingleUpdate(self, criteria, listener, looper):
        pass
    
    @args(void, [String, PendingIntent])
    def requestSingleUpdate(self, provider, intent):
        pass
    
    @args(void, [Criteria, PendingIntent])
    def requestSingleUpdate(self, criteria, intent):
        pass
    
    @args(bool, [String, String, Bundle])
    def sendExtraCommand(self, provider, command, extras):
        pass
    
    @args(void, [String, bool])
    def setTestProviderEnabled(self, provider, enabled):
        pass
    
    @args(void, [String, Location])
    def setTestProviderLocation(self, provider, location):
        pass
    
    @args(void, [String, int, Bundle, long])
    def setTestProviderStatus(self, provider, status, extras, updateTime):
        pass


class LocationProvider(Object):

    AVAILABLE = 2
    OUT_OF_SERVICE = 0
    TEMPORARILY_UNAVAILABLE = 1
    
    @args(int, [])
    def getAccuracy(self):
        pass
    
    @args(String, [])
    def getName(self):
        pass
    
    @args(int, [])
    def getPowerRequirement(self):
        pass
    
    @args(bool, [])
    def hasMonetaryCost(self):
        pass
    
    @args(bool, [Criteria])
    def meetsCriteria(self, criteria):
        pass
    
    @args(bool, [])
    def requiresCell(self):
        pass
    
    @args(bool, [])
    def requiresNetwork(self):
        pass
    
    @args(bool, [])
    def requiresSatellite(self):
        pass
    
    @args(bool, [])
    def requiresAltitude(self):
        pass
    
    @args(bool, [])
    def requiresBearing(self):
        pass
    
    @args(bool, [])
    def requiresSpeed(self):
        pass
