__package__ = "com.example.comparisons8"

from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class ComparisonsActivity(Activity):

    __interfaces__ = []
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        
        s = ""
        
        s += "(1 == 1.0)\n"
        a = 1 == 1.0
        
        if 1 == 1.0:
            s += "Test passed!\n"
        else:
            s += "Test failed!\n"
        
        view.setText(s)
        
        self.setContentView(view)
