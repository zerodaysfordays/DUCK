__package__ = "com.example.arrays3"

from java.lang import Integer, String
from java.nio import ByteBuffer
from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.util import Log
from android.widget import TextView

# Application classes

class ArraysActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        self.view = TextView(self)
        self.setContentView(self.view)
        
        i = 0
        while i < 5:
        
            # Create an array of integers.
            a = array(int, i)
            
            j = 0
            while j < i:
                a[j] = j + 1
                j += 1
            
            self.showOutput(a)
            i += 1
    
    @args(void, [[int]])
    def showOutput(self, a):
    
        text = str(self.view.getText())
        
        l = len(a)
        text += str(l) + " items: "
        
        i = 0
        while i < l:
            text += str(a[i])
            i += 1
            if i != l:
                text += ","
        
        text += "\n"
        self.view.setText(text)
