__package__ = "com.example.for21"

from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        for i in ["0", "1", "2"]:
            s += i + " "
        
        expected = "0 1 2 "
        self.showResult(expected, s)
