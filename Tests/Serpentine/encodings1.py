# -*- coding: utf8 -*-

__package__ = "com.example.encodings1"

import android.os

from Common.helper import TestActivity

class EncodingsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = u"Hello world åøæ!"
        self.showResult(u"Hello world åøæ!", a, True)
