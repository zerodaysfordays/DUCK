__package__ = "com.example.lists15"

from java.lang import Object
from java.util import List
from Common.helper import TestActivity

class ListsActivity(TestActivity):

    __fields__ = {"l": List(Object)}
    
    def __init__(self):
    
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l = [1, 2, 3]
        m = [l, [4, 5, 6]]
        
        n = [1, 2, 3]
        o = [4, 5, 6]
        p = [-1, 0, 1]
        
        actual = str(0 in l) + " " + \
                 str(1 in l) + " " + \
                 str(4 in m) + " " + \
                 str(l in m) + " " + \
                 str(n in m) + " " + \
                 str(o in m) + " " + \
                 str(p in m)
        
        expected = "false true false true true true false"
        
        self.showResult(expected, actual)


class Sub(Object):

    def __init__(self):
        Object.__init__(self)
