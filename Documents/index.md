Documentation Index
===================

DUCK
----

 * [DUCK - Dalvik Unpythonic Compiler Kit](../README.html) - the main `README`
   file for this package
 * [Getting Started](Getting_Started.html)
 * [Creating Keys and Certificates](Keys_and_Certificates.html)

Serpentine
----------

 * [Introduction to Serpentine](Introduction_to_Serpentine.html)
 * [A Serpentine Compiler](../DUCK/Serpentine/README.html) - notes about
   the compiler for the Serpentine language
 * [Examples](../Examples/Serpentine/README.html) - how to build the examples
 * [Demonstrations](../Demos/Serpentine/README.html) - how to build the
   demonstration applications
 * [Tests](../Tests/Serpentine/README.html) - how to build and run the tests
