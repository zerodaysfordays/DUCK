__package__ = "com.example.assignments5"

from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class AssignmentsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        s = ""
        
        a = 1
        b = float(2)
        c = double(3)
        d = long(4)
        
        # Some of these assignments reduce the precision of the values.
        x = a
        y = b
        
        a = d
        b = c
        c = y
        d = x
        s1 = str(a) + " " + str(b) + " " + str(c) + " " + str(d)
        
        s += s1 + "\n"
        if s1 != "4 3.0 2.0 1":
            s += "Test failed"
        else:
            s += "Test passed!"
        
        view.setText(s)
        self.setContentView(view)
