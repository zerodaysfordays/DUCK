# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.view.inputmethod"

from java.lang import CharSequence, Object, String
from android.os import Bundle, Parcelable
from android.text import InputType

class EditorInfo(Object):

    __interfaces__ = [InputType, Parcelable]
    
    __static_fields__ = {
        "CREATOR": Parcelable.Creator(EditorInfo)
        }
    
    __fields__ = {
        "actionId": int,
        "actionLabel": CharSequence,
        "extras": Bundle,
        "fieldId": int,
        "fieldName": String,
        "hintText": CharSequence,
        "imeOptions": int,
        "initialCapsMode": int,
        "initialSelEnd": int,
        "initialSelStart": int,
        "inputType": int,
        "label": CharSequence,
        "packageName": String,
        "privateImeOptions": String
        }
    
    IME_ACTION_DONE = 6
    IME_ACTION_GO = 2
    IME_ACTION_NEXT = 5
    IME_ACTION_NONE = 1
    IME_ACTION_PREVIOUS = 7
    IME_ACTION_SEARCH = 3
    IME_ACTION_SEND = 4
    IME_ACTION_UNSPECIFIED = 0
    IME_FLAG_FORCE_ASCII         = 0x80000000
    IME_FLAG_NAVIGATE_NEXT       = 0x08000000
    IME_FLAG_NAVIGATE_PREVIOUS   = 0x04000000
    IME_FLAG_NO_ACCESSORY_ACTION = 0x20000000
    IME_FLAG_NO_ENTER_ACTION     = 0x40000000
    IME_FLAG_NO_EXTRACT_UI       = 0x10000000
    IME_FLAG_NO_FULLSCREEN       = 0x02000000
    IME_MASK_ACTION = 0xff
    IME_NULL        = 0
    
    def __init__(self):
        pass
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @args(void, [Printer, String])
    def dump(self, pw, prefix):
        pass
    
    @final
    @args(void, [int])
    def makeCompatible(self, targetSdkVersion):
        pass
    
    @args(void, [Parcel, int])
    def writeToParcel(self, dest, flags):
        pass
