__package__ = "com.example.comparisons20"

import android.os

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        expected = ""
        s = ""
        
        s += str(False and False or False) + " "
        s += str(False and False or True) + " "
        s += str(False and True or False) + " "
        s += str(False and True or True) + " "
        s += str(True and False or False) + " "
        s += str(True and False or True) + " "
        s += str(True and True or False) + " "
        s += str(True and True or True) + "\n"
        
        expected += "false true false true false true true true\n"
        
        s += str(False or False and False) + " "
        s += str(False or False and True) + " "
        s += str(False or True and False) + " "
        s += str(False or True and True) + " "
        s += str(True or False and False) + " "
        s += str(True or False and True) + " "
        s += str(True or True and False) + " "
        s += str(True or True and True) + "\n"
        
        expected += "false false false true true true true true\n"
        
        s += str(False or False or False) + " "
        s += str(False or False or True) + " "
        s += str(False or True or False) + " "
        s += str(False or True or True) + " "
        s += str(True or False or False) + " "
        s += str(True or False or True) + " "
        s += str(True or True or False) + " "
        s += str(True or True or True) + "\n"
        
        expected += "false true true true true true true true\n"
        
        actual = s
        self.showResult(expected, actual)
