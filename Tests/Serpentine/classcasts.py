__package__ = "com.example.classcasts"

from java.lang import Object
from android.app import Activity, Context
from android.hardware import Sensor, SensorManager
import android.os
from android.widget import TextView

# Application classes

class ClassCastsActivity(Activity):

    __interfaces__ = []
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        service = self.getSystemService(Context.SENSOR_SERVICE)
        sensorManager = self.sensorManager(service)
        
        view = TextView(self)
        view.setText(str(sensorManager))
        self.setContentView(view)
    
    @args(SensorManager, [Object])
    def sensorManager(self, obj):
    
        return CAST(obj, SensorManager)
