__package__ = "com.example.comparisons13"

import android.os

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        
        if True and False:
            s = "True and False -> True"
        else:
            s = "True and False -> False"
        
        s += "\n"
        
        if True or False:
            s += "True or False -> True"
        else:
            s += "True or False -> False"
        
        expected = "True and False -> False\nTrue or False -> True"
        actual = s
        self.showResult(expected, actual)
