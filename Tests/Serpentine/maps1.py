__package__ = "com.example.maps1"

from java.lang import Integer, String
from java.util import Collection, HashMap, HashSet
from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class StringStringMap(HashMap):

    __item_types__ = [String, String]
    
    def __init__(self):
        HashMap.__init__(self)


class MapsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        strings = StringStringMap()
        strings.put("Hello", "Test passed!")
        
        text = strings.get("Hello")
        
        view = TextView(self)
        view.setText(text)
        
        self.setContentView(view)
