#!/usr/bin/env python

"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, sys

options = ("all", "open", "done")

def number_sort(x, y):

    return cmp(int(os.path.splitext(x)[0]), int(os.path.splitext(y)[0]))


if __name__ == "__main__":

    if len(sys.argv) != 2 or sys.argv[1] not in options:
        sys.stderr.write("Usage: %s [all|open|done]\n" % sys.argv[0])
        sys.exit(1)
    
    option = sys.argv[1]
    
    this_dir = os.path.abspath(os.curdir)
    tasks_dir = os.path.join(this_dir, "Tasks")
    if os.path.isdir(tasks_dir):
    
        for subdir in os.listdir(tasks_dir):
        
            title = subdir + " (" + option + ")"
            print len(title) * "-"
            print title
            print len(title) * "-"
            files = os.listdir(os.path.join(tasks_dir, subdir))
            files.sort(cmp=number_sort)
            
            for file_name in files:
                f = open(os.path.join(tasks_dir, subdir, file_name))
                title = f.readline().rstrip()
                
                if not title and option != "open":
                    # Show done tasks if all/done tasks are specified.
                    title = f.readline().rstrip()
                    print file_name, title
                elif title and option != "done":
                    # Show open tasks if all/open tasks are specified.
                    print file_name, title
            
            print
