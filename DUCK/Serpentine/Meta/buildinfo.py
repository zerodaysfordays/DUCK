from java.lang import Object, String
import java.lang.annotation

# Meta-data

class TargetApi(Object):

    __package__ = "android.annotation"
    __interfaces__ = [java.lang.annotation.Annotation]
    
    __annotations__ = [
        ('java/lang/annotation/Retention', [
            ('value', Field('java/lang/annotation/RetentionPolicy', 'CLASS'))
            ]),
        ('java/lang/annotation/Target', [
            ('value', [
                Field('java/lang/annotation/ElementType', 'TYPE'),
                Field('java/lang/annotation/ElementType', 'METHOD'),
                Field('java/lang/annotation/ElementType', 'CONSTRUCTOR')
                ])
            ])
        ]
    
    @virtual
    @prototype(int, [])
    def value(self):
        pass


class SuppressLint(Object):

    __package__ = "android.annotation"
    __interfaces__ = [java.lang.annotation.Annotation]
    __annotations__ = [
        ('java/lang/annotation/Retention', [
            ('value', Field('java/lang/annotation/RetentionPolicy', 'CLASS'))
            ]),
        ('java/lang/annotation/Target', [
            ('value', [
                Field('java/lang/annotation/ElementType', 'TYPE'),
                Field('java/lang/annotation/ElementType', 'FIELD'),
                Field('java/lang/annotation/ElementType', 'METHOD'),
                Field('java/lang/annotation/ElementType', 'PARAMETER'),
                Field('java/lang/annotation/ElementType', 'CONSTRUCTOR'),
                Field('java/lang/annotation/ElementType', 'LOCAL_VARIABLE')
                ])
            ])
        ]
    
    @virtual
    @prototype([String], [])
    def value(self):
        pass


# Build classes

class BuildConfig(Object):

    __static_fields__ = {
        "DEBUG": False
        }
    
    def __init__(self):
        Object.__init__(self)
