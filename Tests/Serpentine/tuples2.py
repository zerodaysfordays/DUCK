__package__ = "com.example.tuples2"

from Common.helper import TestActivity

class TuplesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a, b = 1, 2
        s = str(a) + " " + str(b)
        
        expected = "1 2"
        self.showResult(expected, s)
