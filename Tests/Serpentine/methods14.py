__package__ = "com.example.methods14"

from java.lang import Object, String
from android.os import Bundle

from Common.helper import TestActivity

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        m = Base()
        s = m.fn(123)
        
        self.showResult("123123", s)


class Base(Object):

    def __init__(self):
        Object.__init__(self)
    
    @args(String, [int])
    def fn(self, i):
        return self.other(i) + \
               Base.other(self, i)
    
    @args(String, [int])
    def other(self, i):
        return str(i)
