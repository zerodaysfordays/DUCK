__package__ = "com.example.templates12"

from android.os import Bundle

from Common.helper import TestActivity
from serpentine.strings import Strings

class TemplatesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = Strings.str("Hello world!")
        b = Strings.str(123)
        c = Strings.str(123.456)
        d = Strings.str(long(123456))
        e = Strings.str(float(12.345))
        f = Strings.str(self)
        g = Strings.str(True)
        h = Strings.str(False)
        i = Strings.str(char(127))
        
        f = f.substring(0, f.indexOf("@"))
        
        s = a + "\n" + b + "\n" + c + "\n" + d + "\n" + e + "\n" + \
            f + "\n" + g + "\n" + h
        
        expected = "Hello world!\n123\n123.456\n123456\n12.345\n" + \
                   "com.example.templates12.TemplatesActivity\ntrue\nfalse"
        
        self.showResult(expected, s)
